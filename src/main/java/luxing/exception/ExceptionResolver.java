package luxing.exception;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import luxing.util.JsonUtil;
import luxing.web.model.Result;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * spring mvc 全局处理异常捕获 根据请求区分ajax和普通请求，分别进行响应. 第一、异常信息输出到日志中。
 * 第二、截取异常详细信息的前50个字符，写入日志表中t_s_log。
 */
@Component
public class ExceptionResolver implements HandlerExceptionResolver {

	@Autowired
	private LogService logService;
	// 记录日志信息
	private static final Logger logger = Logger.getLogger(ExceptionResolver.class);
	// 记录数据库最大字符长度
	private static final int WIRTE_DB_MAX_LENGTH = 1500;

	/**
	 * 对异常信息进行统一处理，区分异步和同步请求，分别处理
	 */
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		logger.error("全局处理异常:", ex);
		boolean isAjax = isAjax(request, response);
		Throwable deepestException = null;
		if (!ex.getClass().getSimpleName().equals("BusinessException")) {
			deepestException = deepestException(ex);
		} else {
			deepestException = ex;
		}
		if (isAjax) {
			return processAjax(response, deepestException);
		} else {
			return processNotAjax(response, deepestException);
		}
	}

	/**
	 * 判断当前请求是否为异步请求.
	 */
	private boolean isAjax(HttpServletRequest request, HttpServletResponse response) {
		return StringUtils.isNotBlank(request.getHeader("X-Requested-With"));
	}

	/**
	 * 获取最原始的异常出处，即最初抛出异常的地方
	 */
	private Throwable deepestException(Throwable e) {
		Throwable tmp = e;
		int breakPoint = 0;
		while (tmp.getCause() != null) {
			if (tmp.equals(tmp.getCause())) {
				break;
			}
			tmp = tmp.getCause();
			breakPoint++;
			if (breakPoint > 1000) {
				break;
			}
		}
		return tmp;
	}

	/**
	 * 异常信息记录截取前50字符写入数据库中
	 * 
	 * @param ex
	 */
	private void addLogError(Throwable ex) {
		// String exceptionMessage = getThrowableMessage(ex);
		String exceptionMessage = "异常：" + ex.getMessage() + ",类型:" + ex.getClass().getSimpleName();
		if (StringUtils.isNotBlank(exceptionMessage)) {
			if (exceptionMessage.length() > WIRTE_DB_MAX_LENGTH) {
				exceptionMessage = exceptionMessage.substring(0, WIRTE_DB_MAX_LENGTH);
			}
		}
		logService.addLogError(exceptionMessage, Log.OPERATE_EXCEPTION);
	}

	/**
	 * ajax异常处理并返回.
	 * 
	 * @param request
	 * @param response
	 * @param handler
	 * @param deepestException
	 * @return
	 */
	private ModelAndView processAjax(HttpServletResponse response, Throwable ex) {
		addLogError(ex);
		ModelAndView mv = new ModelAndView();
		response.setHeader("Cache-Control", "no-store");
		Result result = new Result();
		result.setSuccess(false);
		result.setMsg(ex.getMessage());
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
			pw.write(JsonUtil.toJson(result));
			pw.flush();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			pw.close();
		}
		mv.clear();
		return mv;
	}

	/**
	 * 普通页面异常处理并返回.
	 * 
	 * @param request
	 * @param response
	 * @param handler
	 * @param deepestException
	 * @return
	 */
	private ModelAndView processNotAjax(HttpServletResponse response, Throwable ex) {
		addLogError(ex);
		String exceptionMessage = getThrowableMessage(ex);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("exceptionMessage", exceptionMessage);
		model.put("ex", ex);
		return new ModelAndView("common/error", model);
	}

	/**
	 * 返回错误信息字符串
	 * 
	 * @param ex
	 *            Exception
	 * @return 错误信息字符串
	 */
	public String getThrowableMessage(Throwable ex) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		return sw.toString();
	}
}
