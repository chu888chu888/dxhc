package luxing.common.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 云存储
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 14:58
 */
public interface CloudStorageService {

	void upload(byte[] data, String bucket, String path);

	void uploadCompress(MultipartFile file, String bucket, String suffix, String path, String thumbPath)
			throws Exception;

	void delete(String bucket, String path);

	String generatePresignedUrl(String bucket, String key);

}
