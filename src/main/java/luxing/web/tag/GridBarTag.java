package luxing.web.tag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * @author zzc 类描述：列表工具条标签
 */
public class GridBarTag extends TagSupport {
	private static final long serialVersionUID = 5877135885808980692L;
	private String url;
	private String title;
	private String funname;// 自定义函数名称
	private String icon;// 图标
	private String width;
	private String height;

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		Tag t = findAncestorWithClass(this, GridTag.class);
		GridTag parent = (GridTag) t;
		parent.setBar(url, title, funname, icon, width, height);
		return EVAL_PAGE;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFunname(String funname) {
		this.funname = funname;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public void setHeight(String height) {
		this.height = height;
	}

}
