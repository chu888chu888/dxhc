package luxing.web.tag;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import manage.sys.entity.Menu;

/**
 * 
 * 类描述：菜单标签
 * 
 * zzc
 * 
 * @date： 日期：2012-12-7 时间：上午10:17:45
 * 
 * @version 1.0
 */
public class MenuTag extends TagSupport {

	private static final long serialVersionUID = -1709730728370216260L;

	protected Map<Integer, List<Menu>> menuMap;// 菜单Map

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		JspWriter out = null;
		try {
			out = this.pageContext.getOut();
			out.print(end().toString());
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.clearBuffer();
			} catch (Exception e2) {
			}
		}
		return EVAL_PAGE;
	}

	public StringBuffer end() {
		StringBuffer sb = new StringBuffer();
		sb.append(getMultistageTree(menuMap));
		return sb;
	}

	private String getMultistageTree(Map<Integer, List<Menu>> menuMap) {
		if (menuMap == null || menuMap.size() == 0 || !menuMap.containsKey(0)) {
			return "没有权限,请联系管理员.";
		}
		StringBuffer menuString = new StringBuffer();
		List<Menu> list = menuMap.get(0);
		for (Menu menu : list) {
			menuString.append("<li>");
			menuString.append("<a href=\"#\" class=\"\" ><i class=\"fa " + menu.getIcon() + "\"></i>");
			menuString.append("<span class=\"menu-text\">");
			menuString.append(menu.getName());
			menuString.append("</span>");
			menuString.append("<span class=\"fa arrow\">");
			menuString.append("</span>");
			if (!menu.hasSubMenu(menuMap)) {
				menuString.append("</a></li>");
			} else {
				menuString.append("</a><ul  class=\"nav nav-second-level\" >");
				menuString.append(getSubMenu(menu, 1, menuMap));
				menuString.append("</ul></li>");
			}
		}
		return menuString.toString();
	}

	private String getSubMenu(Menu parent, int level, Map<Integer, List<Menu>> menuMap) {
		StringBuffer menuString = new StringBuffer();
		List<Menu> list = menuMap.get(level);
		for (Menu menu : list) {
			if (menu.getParentMenu().getId().equals(parent.getId())) {
				menuString.append(getLeafOfTree(menu, menuMap));
			}
		}
		return menuString.toString();
	}

	private String getLeafOfTree(Menu menu, Map<Integer, List<Menu>> menuMap) {
		StringBuffer menuString = new StringBuffer();
		String name = menu.getName();
		menuString.append("<li> <a class=\"J_menuItem\" href=\"").append(menu.getUrl())
				.append("\">");
		if (!menu.hasSubMenu(menuMap)) {
			menuString.append("<span class=\"menu-text\">");
			menuString.append(name);
			menuString.append("</span>");
			menuString.append("</a>");
			menuString.append("</li>");
		} else {
			menuString.append("<span class=\"menu-text\">");
			menuString.append(name);
			menuString.append("</span>");
			menuString.append("<span class=\"fa arrow\">");
			menuString.append("</span>");
			menuString.append("</a>");
			menuString.append("<ul class=\"nav nav-third-level\" >");
			menuString.append(getSubMenu(menu, 2, menuMap));
			menuString.append("</ul></li>");
		}
		return menuString.toString();
	}

	public void setMenuMap(Map<Integer, List<Menu>> menuMap) {
		this.menuMap = menuMap;
	}

}
