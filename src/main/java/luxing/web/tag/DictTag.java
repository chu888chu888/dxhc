package luxing.web.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import luxing.util.ApplicationContextUtil;
import luxing.util.LoginUserUtil;
import modules.sys.entity.Dict;
import modules.sys.service.DictService;

/**
 * 
 * 选择下拉框
 * 
 * @author: lianglaiyang @date： 日期：2013-04-18
 * @version 1.0
 */
public class DictTag extends TagSupport {

	private static final long serialVersionUID = -131294310756619995L;

	private static final Logger logger = Logger.getLogger(DictTag.class);

	private static DictService dictService = ApplicationContextUtil.getBean(DictService.class);

	private String groupCode; // 数据字典组编码
	private String id;
	private String name;
	private String defaultVal = ""; // 默认值
	private String type;// 控件类型select|radio|checkbox
	private String extend; // 扩展
	private String disabled;// 只读属性
	private String datatype;
	private String onchange;// onchange事件

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		JspWriter out = null;
		try {
			out = this.pageContext.getOut();
			out.print(end().toString());
			out.flush();
		} catch (IOException e) {
			logger.error(e);
		}
		return EVAL_PAGE;
	}

	public StringBuffer end() {
		StringBuffer sb = new StringBuffer();
		List<Dict> dictList = new ArrayList<Dict>();
		List<Dict> dictDbList = dictService.listByGroupCode(groupCode,
				LoginUserUtil.getLoginUser().getTenantId());
		dictList.addAll(dictDbList);
		if (StringUtils.isNotBlank(extend)) {
			String[] temp = extend.split(",");
			for (String string : temp) {
				Dict dict = new Dict();
				dict.setName(string.split("_")[0]);
				dict.setId(string.split("_")[1]);
				dictList.add(dict);
			}
		}
		if ("radio".equals(type)) {
			for (Dict dict : dictList) {
				radio(dict.getName(), dict.getId(), sb);
			}
		} else if ("checkbox".equals(type)) {
			for (Dict dict : dictList) {
				checkbox(dict.getName(), dict.getId(), sb);
			}
		} else {
			sb.append("<select name=\"" + name + "\"" + " id=\"" + id + "\"");
			if (StringUtils.isNotBlank(disabled) && disabled.equals("disabled")) {
				disabled(sb);
			}
			if (StringUtils.isNotBlank(datatype)) {
				datatype(sb);
			}
			if (StringUtils.isNotBlank(onchange)) {
				sb.append(" onchange=\"" + onchange + "\"");
			}
			sb.append(">");
			if (!(StringUtils.isNotBlank(datatype) && datatype.equals("*"))) {
				select("请选择", "", sb);
			}
			for (Dict dict : dictList) {
				select(dict.getName(), dict.getId(), sb);
			}
			sb.append("</select>");
		}
		return sb;
	}

	/**
	 * 单选框方法
	 * 
	 * @作者：Alexander
	 * 
	 * @param name
	 * @param id
	 * @param sb
	 */
	private void radio(String name, String id, StringBuffer sb) {
		if (id.equals(defaultVal)) {
			sb.append("<input type=\"radio\" name=\"" + name + "\" checked=\"checked\" value=\"" + id + "\"" + " id=\""
					+ id + "\"");
			if (StringUtils.isNotBlank(disabled) && disabled.equals("disabled")) {
				disabled(sb);
			}
			if (StringUtils.isNotBlank(datatype)) {
				datatype(sb);
			}
			if (StringUtils.isNotBlank(onchange)) {
				sb.append(" onchange=\"" + onchange + "\"");
			}
			sb.append(" />");
		} else {
			sb.append("<input type=\"radio\" name=\"" + name + "\" value=\"" + id + "\"" + " id=\"" + id + "\"");
			if (StringUtils.isNotBlank(disabled) && disabled.equals("disabled")) {
				disabled(sb);
			}
			if (StringUtils.isNotBlank(datatype)) {
				datatype(sb);
			}
			if (StringUtils.isNotBlank(onchange)) {
				sb.append(" onchange=\"" + onchange + "\"");
			}
			sb.append(" />");
		}
		sb.append(name);
	}

	/**
	 * 复选框方法
	 * 
	 * @作者：Alexander
	 * 
	 * @param name
	 * @param id
	 * @param sb
	 */
	private void checkbox(String name, String id, StringBuffer sb) {
		String[] values = this.defaultVal.split(",");
		Boolean checked = false;
		for (int i = 0; i < values.length; i++) {
			String value = values[i];
			if (id.equals(value)) {
				checked = true;
				break;
			}
			checked = false;
		}
		if (checked) {
			sb.append("<input type=\"checkbox\" name=\"" + name + "\" checked=\"checked\" value=\"" + id + "\"");
			if (StringUtils.isNotBlank(disabled) && disabled.equals("disabled")) {
				disabled(sb);
			}
			if (StringUtils.isNotBlank(datatype)) {
				datatype(sb);
			}
			if (StringUtils.isNotBlank(onchange)) {
				sb.append(" onchange=\"" + onchange + "\"");
			}
			sb.append(" />");
		} else {
			sb.append("<input type=\"checkbox\" name=\"" + name + "\" value=\"" + id + "\"" + " id=\"" + id + "\"");
			if (StringUtils.isNotBlank(disabled) && disabled.equals("disabled")) {
				disabled(sb);
			}
			if (StringUtils.isNotBlank(datatype)) {
				datatype(sb);
			}
			if (StringUtils.isNotBlank(onchange)) {
				sb.append(" onchange=\"" + onchange + "\"");
			}
			sb.append(" />");
		}
		sb.append(name);
	}

	/**
	 * 选择框方法
	 * 
	 * @作者：Alexander
	 * 
	 * @param name
	 * @param id
	 * @param sb
	 */
	private void select(String name, String id, StringBuffer sb) {
		if (id.equals(this.defaultVal)) {
			sb.append(" <option value=\"" + id + "\" selected=\"selected\">");
		} else {
			sb.append(" <option value=\"" + id + "\">");
		}
		sb.append(name);
		sb.append(" </option>");
	}

	/**
	 * 加入datatype属性,并加入非空验证作为默认值
	 * 
	 * @param sb
	 * @return
	 */
	private StringBuffer datatype(StringBuffer sb) {
		return sb.append(" datatype=\"" + datatype + "\"");
	}

	/**
	 * 加入readonly 属性,当此属性值为 readonly时，设置控件只读
	 * 
	 * @author jg_xugj
	 * @param sb
	 * @return sb
	 */
	private StringBuffer disabled(StringBuffer sb) {
		sb.append(" disabled= \"disabled\" ");
		return sb;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setExtend(String extend) {
		this.extend = extend;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public void setOnchange(String onchange) {
		this.onchange = onchange;
	}

}
