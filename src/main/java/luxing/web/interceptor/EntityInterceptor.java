package luxing.web.interceptor;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

import luxing.constant.EntityConstant;
import luxing.util.LoginUserUtil;
import luxing.util.ManageLoginUserUtil;
import luxing.util.ObjectUtil;
import manage.sys.entity.ManageUser;
import modules.sys.entity.User;

/**
 * Hiberate拦截器：实现创建人，创建时间，创建人名称自动注入; 修改人,修改时间,修改人名自动注入;
 * 
 * @author zzc
 */
@Component
public class EntityInterceptor extends EmptyInterceptor {

	private static final long serialVersionUID = 3335289972777892192L;

	@Override
	public String onPrepareStatement(String sql) {
		// TODO 分表
		return super.onPrepareStatement(sql);
	}

	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		User user = LoginUserUtil.getUser();
		ManageUser manageUser = ManageLoginUserUtil.getUser();
		// 添加数据
		for (int index = 0; index < propertyNames.length; index++) {
			/* 找到名为"创建时间"的属性 */
			if (EntityConstant.CREATE_DATE.equals(propertyNames[index])) {
				/* 使用拦截器将对象的"创建时间"属性赋上值 */
				if (ObjectUtil.isEmpty(state[index])) {
					state[index] = new Date();
				}
				continue;
			}
			/* 找到名为"创建人"的属性 */
			else if (EntityConstant.CREATE_BY.equals(propertyNames[index])) {
				/* 使用拦截器将对象的"创建人"属性赋上值 */
				if (ObjectUtil.isEmpty(state[index])) {
					if (user != null) {
						state[index] = user.getName();
					} else if (manageUser != null) {
						state[index] = manageUser.getName();
					}
				}
				continue;
			}
			/* 找到名为"租户"的属性 */
			else if (EntityConstant.TENANT_ID.equals(propertyNames[index])) {
				/* 使用拦截器将对象的"租户"属性赋上值 */
				if (ObjectUtil.isEmpty(state[index])) {
					if (user != null) {
						state[index] = LoginUserUtil.getLoginUser().getTenantId();
					}
				}
				continue;
			}
		}
		return true;
	}

	public boolean onFlushDirty(Object entity, Serializable id, Object[] state, Object[] previousState,
			String[] propertyNames, Type[] types) {
		User user = LoginUserUtil.getUser();
		ManageUser manageUser = ManageLoginUserUtil.getUser();
		// 添加数据
		for (int index = 0; index < propertyNames.length; index++) {
			/* 找到名为"修改时间"的属性 */
			if (EntityConstant.UPDATE_DATE.equals(propertyNames[index])) {
				/* 使用拦截器将对象的"修改时间"属性赋上值 */
				state[index] = new Date();
				continue;
			}
			/* 找到名为"修改人"的属性 */
			else if (EntityConstant.UPDATE_BY.equals(propertyNames[index])) {
				/* 使用拦截器将对象的"修改人"属性赋上值 */
				if (user != null) {
					state[index] = user.getName();
				} else if (manageUser != null) {
					state[index] = manageUser.getName();
				}
				continue;
			}

		}
		return true;
	}
}
