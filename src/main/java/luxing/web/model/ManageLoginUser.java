package luxing.web.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import manage.sys.entity.ManageUser;
import manage.sys.entity.Menu;

/**
 * 在线用户对象
 * 
 * @author JueYue
 * @date 2013-9-28
 * @version 1.0
 */
public class ManageLoginUser implements Serializable {

	private static final long serialVersionUID = -9071491677290821063L;

	private ManageUser user;
	private String ip;
	private Date loginTime;
	private Map<Integer, List<Menu>> userMenu;

	public ManageUser getUser() {
		return user;
	}

	public void setUser(ManageUser user) {
		this.user = user;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Map<Integer, List<Menu>> getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(Map<Integer, List<Menu>> userMenu) {
		this.userMenu = userMenu;
	}

}
