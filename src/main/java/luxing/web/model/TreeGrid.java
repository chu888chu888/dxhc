package luxing.web.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

public class TreeGrid implements Serializable {

	private static final long serialVersionUID = -2464844271201405912L;

	private String id;
	private String text;
	private String parentId;
	private String parentText;
	private String state = "open";// 是否展开(open,closed)
	private Map<String, Object> fieldMap; // 存储实体字段信息容器： key-字段名称，value-字段值

	public String getParentText() {
		return parentText;
	}

	public void setParentText(String parentText) {
		this.parentText = parentText;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Map<String, Object> getFieldMap() {
		return fieldMap;
	}

	public void setFieldMap(Map<String, Object> fieldMap) {
		this.fieldMap = fieldMap;
	}

	public String toJson() {
		return "{" + "'id':'" + id + '\'' + ", 'text':'" + text + '\'' + ", 'parentId':'" + parentId + '\''
				+ ", 'parentText':'" + parentText + '\'' + ", 'state':'" + state + '\'' + assembleFieldsJson() + '}';
	}

	private String assembleFieldsJson() {
		String fieldsJson = ", 'fieldMap':" + fieldMap;
		if (fieldMap != null && fieldMap.size() > 0) {
			Map<String, Object> resultMap = new HashMap<String, Object>();
			for (Map.Entry<String, Object> entry : fieldMap.entrySet()) {
				resultMap.put("fieldMap." + entry.getKey(), entry.getValue());
			}
			fieldsJson = ", " + JSON.toJSON(resultMap).toString().replace("{", "").replace("}", "");
		}
		return fieldsJson;
	}

}
