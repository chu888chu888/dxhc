package luxing.web.model;

/**
 * 
 * @author zzc
 *
 */
public class GridOpt {

	private String title;// 按钮名称
	private String funname;// 自定义函数名称
	private String exp;// 判断链接是否显示的表达式
	private String style;// 样式

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public String getFunname() {
		return funname;
	}

	public void setFunname(String funname) {
		this.funname = funname;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

}
