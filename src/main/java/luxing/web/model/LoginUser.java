package luxing.web.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import manage.sys.entity.Menu;
import modules.sys.entity.User;

/**
 * 在线用户对象
 * 
 * @author JueYue
 * @date 2013-9-28
 * @version 1.0
 */
public class LoginUser implements Serializable {

	private static final long serialVersionUID = -9071491677290821063L;

	private User user;
	private String tenantId;
	private String ip;
	private Date loginTime;
	private Map<Integer, List<Menu>> userMenu;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Map<Integer, List<Menu>> getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(Map<Integer, List<Menu>> userMenu) {
		this.userMenu = userMenu;
	}
}
