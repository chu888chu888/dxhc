package luxing.constant;

/**
 * 云存储配置信息
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 16:12
 */
public interface StorageConfig {

	// 阿里云EndPoint
	public static final String endPoint = "oss-cn-beijing.aliyuncs.com";
	// 阿里云AccessKeyId
	public static final String accessKeyId = "accessKeyId";
	// 阿里云AccessKeySecret
	public static final String accessKeySecret = "accessKeySecret";
	// 阿里云BucketName-车辆图片
	public static final String bucketVehicle = "dxhc-vehicle";
	// 阿里云BucketName-umeditor
	public static final String bucketUmeditor = "dxhc-umeditor";
	// 阿里云BucketName-cover
	public static final String bucketCover = "dxhc-safe";

}
