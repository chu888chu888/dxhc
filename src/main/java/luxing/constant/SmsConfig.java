package luxing.constant;

/**
 * 短信配置信息
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 16:12
 */
public interface SmsConfig {

	// 短信API产品名称（短信产品名固定，无需修改）
	public static final String product = "Dysmsapi";
	// 短信API产品域名（接口地址固定，无需修改）
	public static final String domain = "dysmsapi.aliyuncs.com";
	// 阿里云AccessKeyId
	public static final String accessKeyId = "accessKeyId";
	// 阿里云AccessKeySecret
	public static final String accessKeySecret = "accessKeySecret";
	// regionId
	public static final String regionId = "cn-hangzhou";
	// endpointName
	public static final String endpointName = "cn-hangzhou";
	// signName
	public static final String signName = "大象货车";
	// 车辆到期提醒
	public static final String template_vehicle_warn = "template_vehicle_warn";
	// 责任人借款提醒
	public static final String template_owner_warn = "template_owner_warn";
	// 密码重置
	public static final String template_password_reset = "template_password_reset";
	// 密码重置key,session存放读取
	public static final String key_password_reset = "passwordResetCode";

}
