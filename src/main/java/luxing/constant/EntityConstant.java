package luxing.constant;

/**
 * 数据库上下文常量
 * 
 * @author JueYue
 * @date 2014年8月30日 下午10:10:36
 */
public interface EntityConstant {

	/**
	 * 创建者登录名称
	 */
	public static final String TENANT_ID = "tenantId";

	/**
	 * 创建者登录名称
	 */
	public static final String CREATE_BY = "createBy";
	/**
	 * 创建日期
	 */
	public static final String CREATE_DATE = "createDate";
	/**
	 * 更新用户登录名称
	 */
	public static final String UPDATE_BY = "updateBy";
	/**
	 * 更新日期
	 */
	public static final String UPDATE_DATE = "updateDate";
}
