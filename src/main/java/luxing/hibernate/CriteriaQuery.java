package luxing.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;

import luxing.web.model.DataGrid;
import luxing.web.model.SortDirection;

/**
 * 
 * 类描述：CriteriaQuery类是对hibernate QBC查询方法的封装，需要的参数是当前操作的实体类 zzc
 *
 * @date： 日期：2012-12-7 时间：上午10:22:15
 * 
 * @version 1.0
 */
@SuppressWarnings({ "rawtypes", "static-access" })
public class CriteriaQuery implements Serializable {

	private static final long serialVersionUID = -8234774534960267175L;
	private int curPage = 1;// 当前页
	private int pageSize = 15;// 默认一页条数
	private CriteriaList criteriaList = new CriteriaList();// 自定义查询条件集合

	private DetachedCriteria detachedCriteria;
	private static Map<String, Object> map;
	private static Map<String, Object> orderMap;// 排序字段
	private boolean flag = true;// 对同一字段进行第二次重命名查询时值设置FASLE不保存重命名查询条件
	private String field = "";// 查询需要显示的字段
	private Class entityClass;// POJO
	private List results;// 结果集
	private int total;
	private List<String> alias = new ArrayList<String>();// 保存创建的aliasName

	public CriteriaQuery() {
	}

	public CriteriaQuery(Class c) {
		this.detachedCriteria = DetachedCriteria.forClass(c);
		this.map = new HashMap<String, Object>();
		this.orderMap = new HashMap<String, Object>();
	}

	public CriteriaQuery(Class entityClass, DataGrid dg) {
		this.curPage = dg.getPage();
		this.detachedCriteria = DetachedCriteria.forClass(entityClass);
		this.field = dg.getField();
		this.entityClass = entityClass;
		this.dataGrid = dg;
		this.pageSize = dg.getRows();
		this.map = new HashMap<String, Object>();
		this.orderMap = new HashMap<String, Object>();
	}

	/**
	 * 加载条件(条件之间有关联) hql((this_.0 like ? and this_.1 like ?) or this_.2 like ?)
	 * 表示法cq.add(cq.or(cq.and(cq, 0, 1), cq, 2))----- hql2:(this_.0 like ? or
	 * this_.1 like ?) 表示法:cq.add(cq.or(cq, 0, 1)); 例子：cq.in("TBPrjstatus.code",
	 * status); cq.eq("attn", user.getUserName()); cq.isNull("attn");
	 * cq.add(cq.and(cq.or(cq, 1, 2), cq, 0));
	 */
	public void add(Criterion c) {
		detachedCriteria.add(c);
	}

	/**
	 * 加载条件
	 */
	public void add() {
		for (int i = 0; i < getCriteriaList().size(); i++) {
			add(getCriteriaList().getParas(i));
		}
		getCriteriaList().removeAll(getCriteriaList());
	}

	/**
	 * 创建外键表关联对象
	 * 
	 * @param name外键表实体名
	 * @param value引用名
	 */
	public void createAlias(String name, String value) {
		if (!alias.contains(name)) {
			detachedCriteria.createAlias(name, value);
			alias.add(name);
		}
	}

	/**
	 * 设置order（排序）查询条件
	 * 
	 * @param ordername
	 *            ：排序字段名
	 * @param ordervalue
	 *            ：排序字段值（"asc","desc"）
	 */
	public void addOrder(String ordername, SortDirection ordervalue) {
		orderMap.put(ordername, ordervalue);

	}

	/**
	 * 设置order（排序）查询条件
	 * 
	 * @param ordername
	 *            ：排序字段名
	 * @param ordervalue
	 *            ：排序字段值（"asc","desc"）
	 */
	public void setOrder(Map<String, Object> map) {
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			judgeCreateAlias(entry.getKey());
			if (SortDirection.asc.equals(entry.getValue())) {
				detachedCriteria.addOrder(Order.asc(entry.getKey()));
			} else {
				detachedCriteria.addOrder(Order.desc(entry.getKey()));
			}
		}
	}

	/**
	 * 创建 alias
	 * 
	 * @param entitys
	 *            规则 entitys 为a.b.c.d
	 */
	public void judgeCreateAlias(String entitys) {
		String[] aliass = entitys.split("\\.");
		for (int i = 0; i < aliass.length - 1; i++) {
			createAlias(aliass[i], aliass[i]);
		}
	}

	public Map<String, Object> getOrderMap() {
		return orderMap;
	}

	/**
	 * 设置eq(相等)查询条件
	 * 
	 * @param keyname
	 *            :字段名
	 * @param keyvalue
	 *            ：字段值
	 */
	public void eq(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			criteriaList.addPara(Restrictions.eq(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置notEq(不等)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void notEq(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			criteriaList.addPara(Restrictions.ne(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置like(模糊)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void like(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			// criterionList.addPara(Restrictions.like(keyname, "%" + keyvalue+
			// "%"));
			criteriaList.addPara(Restrictions.like(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置gt(>)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void gt(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			criteriaList.addPara(Restrictions.gt(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置lt(<)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void lt(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			criteriaList.addPara(Restrictions.lt(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置le(<=)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void le(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			criteriaList.addPara(Restrictions.le(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置ge(>=)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void ge(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			criteriaList.addPara(Restrictions.ge(keyname, keyvalue));
			if (flag) {
				this.put(keyname, keyvalue);
			}
			flag = true;
		}
	}

	/**
	 * 设置in(包含)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void in(String keyname, Object[] keyvalue) {
		if (keyvalue != null && keyvalue.length > 0 && keyvalue[0] != "") {
			criteriaList.addPara(Restrictions.in(keyname, keyvalue));
		}
	}

	/**
	 * 设置notIn(不包含)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void notIn(String keyname, Object[] keyvalue) {
		if (keyvalue != null && keyvalue.length > 0 && keyvalue[0] != "") {
			criteriaList.addPara(Restrictions.not(Restrictions.in(keyname, keyvalue)));
		}
	}

	/**
	 * 设置isNull查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void isNull(String keyname) {
		criteriaList.addPara(Restrictions.isNull(keyname));
	}

	/**
	 * 设置isNull查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void isNotNull(String keyname) {
		criteriaList.addPara(Restrictions.isNotNull(keyname));
	}

	/**
	 * 保存查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void put(String keyname, Object keyvalue) {
		if (keyvalue != null && keyvalue != "") {
			map.put(keyname, keyvalue);
		}
	}

	/**
	 * 设置between(之间)查询条件
	 * 
	 * @param keyname
	 * @param keyvalue1
	 * @param keyvalue2
	 */
	public void between(String keyname, Object keyvalue1, Object keyvalue2) {
		Criterion c = null;// 写入between查询条件

		if (!keyvalue1.equals(null) && !keyvalue2.equals(null)) {
			c = Restrictions.between(keyname, keyvalue1, keyvalue2);
		} else if (!keyvalue1.equals(null)) {
			c = Restrictions.ge(keyname, keyvalue1);
		} else if (!keyvalue2.equals(null)) {
			c = Restrictions.le(keyname, keyvalue2);
		}
		criteriaList.add(c);
	}

	public void sql(String sql) {
		Restrictions.sqlRestriction(sql);
	}

	public void sql(String sql, Object[] objects, Type[] type) {
		Restrictions.sqlRestriction(sql, objects, type);
	}

	public void sql(String sql, Object objects, Type type) {
		Restrictions.sqlRestriction(sql, objects, type);
	}

	public Integer getCurPage() {
		return curPage;
	}

	public void setCurPage(Integer curPage) {
		this.curPage = curPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public DetachedCriteria getDetachedCriteria() {
		return detachedCriteria;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public void setDetachedCriteria(DetachedCriteria detachedCriteria) {
		this.detachedCriteria = detachedCriteria;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public List getResults() {
		return results;
	}

	public void setResults(List results) {
		this.results = results;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	private DataGrid dataGrid;

	public DataGrid getDataGrid() {
		return dataGrid;
	}

	public void setDataGrid(DataGrid dataGrid) {
		this.dataGrid = dataGrid;
	}

	public Class getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class entityClass) {
		this.entityClass = entityClass;
	}

	public CriteriaList getCriteriaList() {
		return criteriaList;
	}

	public void setCriteriaList(CriteriaList criteriaList) {
		this.criteriaList = criteriaList;
	}

}
