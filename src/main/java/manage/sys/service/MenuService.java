package manage.sys.service;

import java.util.List;
import java.util.Map;

import luxing.common.service.CommonService;
import manage.sys.entity.Menu;

/**
 * 
 * @author zzc
 *
 */
public interface MenuService extends CommonService {

	void initMenu();

	Menu getByUrl(String url);

	void updateMenu(Menu menu);

	void deleteMenu(Menu menu);

	List<Menu> listByType(String type);

	List<Menu> listByUser(String userId);

	List<Menu> listByManageUser(String userId);

	List<String> listMenuIdsByUser(String userId);

	List<String> listMenuIdsByManageUser(String userId);

	Menu getByUserAndUrl(String userId, String url);

	Menu getByManageUserAndUrl(String userId, String url);

	Map<Integer, List<Menu>> mapLevelMenuByUser(String userId);

	Map<Integer, List<Menu>> mapLevelMenuByManageUser(String userId);

}
