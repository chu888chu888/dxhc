package manage.sys.service;

import luxing.common.service.CommonService;

/**
 * 
 * @author zzc
 *
 */
public interface JobExecuteService extends CommonService {

	/**
	 * 租户扣费
	 */
	void deductionFee();

	/**
	 * 保险到期无效
	 */
	void insExpire();

	/**
	 * 清除被禁用户记录
	 */
	void deleteUserLogin();

	/**
	 * 清除被禁管理员记录
	 */
	void deleteManageUserLogin();

	/**
	 * 计算首页车辆动态数据
	 */
	void calIndexDynamicData();

	/**
	 * 计算首页车辆状态数据
	 */
	void calIndexStatusData();

	/**
	 * 计算首页到期数据
	 */
	void calIndexWarnData();

	/**
	 * 注销责任人
	 */
	void disableOwner();

	/**
	 * 注销司机
	 */
	void disableDriver();

	/**
	 * 计算车辆动态报表数据
	 */
	void calRptVehicleDynamicData();

	/**
	 * 计算车辆状态报表数据
	 */
	void calRptVehicleStatusData();

}
