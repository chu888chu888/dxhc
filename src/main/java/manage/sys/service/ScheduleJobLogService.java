package manage.sys.service;

import luxing.common.service.CommonService;
import manage.sys.entity.ScheduleJobLog;

/**
 * 
 * @author zzc
 *
 */
public interface ScheduleJobLogService extends CommonService {
	void save(ScheduleJobLog scheduleJobLog);
}
