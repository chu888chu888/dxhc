package manage.sys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import manage.sys.entity.ManageUserLogin;
import manage.sys.service.ManageUserLoginService;

/**
 * 
 * @author zzc
 *
 */
@Service("manageUserLoginService")
@Transactional
public class ManageUserLoginServiceImpl extends CommonServiceImpl implements ManageUserLoginService {

	public ManageUserLogin getByUserName(String userName) {
		String hql = "from ManageUserLogin where userName=?";
		return getUniqueByHql(hql, userName);
	}

	public List<ManageUserLogin> listByIP(String ip) {
		String hql = "from ManageUserLogin where ip=? and loginNum>=10";
		return listByHql(hql, ip);
	}

}
