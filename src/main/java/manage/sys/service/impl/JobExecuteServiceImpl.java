package manage.sys.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.DateUtil;
import manage.sys.service.JobExecuteService;
import manage.tenant.entity.Tenant;
import modules.arc.entity.Driver;
import modules.arc.entity.Owner;
import modules.arc.entity.Vehicle;
import modules.arc.entity.VehicleWarn;
import modules.fin.entity.Income;
import modules.ins.entity.Insurance;
import modules.loan.entity.LoanContract;
import modules.loan.entity.Loan;
import modules.rpt.entity.RptVehicle;
import modules.sys.entity.Bill;
import modules.sys.entity.Dict;
import modules.sys.entity.IndexData;
import modules.warn.entity.WarnType;

/**
 * 
 * @author zzc
 *
 */
@Service("jobExecuteService")
@Transactional
public class JobExecuteServiceImpl extends CommonServiceImpl implements JobExecuteService {

	/**
	 * 租户扣费
	 */
	public void deductionFee() {
		List<Tenant> tenantList = listByProperty(Tenant.class, "status", Tenant.STATUS_ENABLE);
		List<Bill> billList = new ArrayList<Bill>();
		for (Tenant tenant : tenantList) {
			// 如果余额小于1,设置余额为0
			if (tenant.getBalance().compareTo(new BigDecimal(1)) == -1) {
				tenant.setBalance(new BigDecimal(0));
			} else {
				BigDecimal balance = tenant.getBalance().subtract(new BigDecimal(tenant.getUserList().size()));
				tenant.setBalance(balance);
			}
			// 如果账户等于0,设置租户状态禁用
			if (tenant.getBalance().compareTo(new BigDecimal(0)) == 0) {
				tenant.setStatus(Tenant.STATUS_DISABLE);
			}
			// 更新租户信息
			saveOrUpdate(tenant);
			// 账户明细
			Bill bill = new Bill(tenant, "使用费", Bill.TYPE_EXPENSE, new BigDecimal(1), new Date(), "系统", null);
			billList.add(bill);
		}
		batchSave(billList);
	}

	/**
	 * 保险到期无效
	 */
	public void insExpire() {
		String hql = "from Insurance where status=? and endDate<=?";
		List<Insurance> insuranceList = listByHql(hql, Insurance.STATUS_ENABLE, new Date());
		for (Insurance insurance : insuranceList) {
			insurance.setStatus(Insurance.STATUS_DISABLE);
			saveOrUpdate(insurance);
		}
	}

	/**
	 * 清除被禁用户记录
	 */
	public void deleteUserLogin() {
		String hql = "delete from UserLogin where loginNum >= 10";
		executeHql(hql);
	}

	/**
	 * 清除被禁用户记录
	 */
	public void deleteManageUserLogin() {
		String hql = "delete from ManageUserLogin where loginNum >= 10";
		executeHql(hql);
	}

	/**
	 * 计算首页动态数据
	 */
	public void calIndexDynamicData() {
		// 车辆动态数据列表
		List<IndexData> indexDataList = new ArrayList<IndexData>();
		List<Tenant> tenantList = listByHql("from Tenant where status=?", Tenant.STATUS_ENABLE);
		for (Tenant tenant : tenantList) {
			// 删除历史数据
			executeHql("delete from IndexData where type=? and tenantId=?", IndexData.TYPE_CLDT, tenant.getId());
			// 查询车辆
			List<Vehicle> allList = listByHql("from Vehicle where tenantId=?", tenant.getId());
			List<Vehicle> vehicleList = new ArrayList<Vehicle>();
			// 有效车辆 :牵引车，正常，单挂车
			for (Vehicle vehicle : allList) {
				if (vehicle.getType().equals(Vehicle.TYPE_TRACTOR) || vehicle.getType().equals(Vehicle.TYPE_WHOLE)
						|| (vehicle.getType().equals(Vehicle.TYPE_TRAILER) && vehicle.getRelationVehicleId() == null)) {
					vehicleList.add(vehicle);
				}
			}
			// 当前日期,当月第一天，当年第一天
			Date currentDate = new Date();
			Date monthFirstDay = DateUtil.minDateOfMonth(currentDate);
			Date yearFirstDay = DateUtil.minDateOfYear(currentDate);
			Date yearLastDay = DateUtil.maxDateOfYear(currentDate);
			// 计算当月新增，当月注销，当年新增，当年注销
			int currentNum = 0, monthAdd = 0, yearAdd = 0, monthReduce = 0, yearReduce = 0;
			for (Vehicle vehicle : vehicleList) {
				// 当前车辆
				if (!vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					currentNum = currentNum + 1;
				}
				// 当月新增
				if (vehicle.getCreateDate().getTime() >= monthFirstDay.getTime()
						&& vehicle.getCreateDate().getTime() <= currentDate.getTime()
						&& !vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					monthAdd = monthAdd + 1;
				}
				// 当年新增
				if (vehicle.getCreateDate().getTime() >= yearFirstDay.getTime()
						&& vehicle.getCreateDate().getTime() <= yearLastDay.getTime()
						&& !vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					yearAdd = yearAdd + 1;
				}
				// 当月注销
				if (vehicle.getVoidDate() != null && vehicle.getVoidDate().getTime() >= monthFirstDay.getTime()
						&& vehicle.getVoidDate().getTime() <= currentDate.getTime()
						&& vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					monthReduce = monthReduce + 1;
				}
				// 当年注销
				if (vehicle.getVoidDate() != null && vehicle.getVoidDate().getTime() >= yearFirstDay.getTime()
						&& vehicle.getVoidDate().getTime() <= yearLastDay.getTime()
						&& vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					yearReduce = yearReduce + 1;
				}
			}
			int yearDynamic = yearAdd - yearReduce;
			// 当前车辆数
			IndexData currentData = new IndexData();
			currentData.setName(IndexData.NAME_CURRENTAMOUNT);
			currentData.setValue(currentNum);
			currentData.setType(IndexData.TYPE_CLDT);
			currentData.setTenantId(tenant.getId());
			indexDataList.add(currentData);
			// 当月新增
			IndexData monthAddData = new IndexData();
			monthAddData.setName(IndexData.NAME_MONTHADD);
			monthAddData.setValue(monthAdd);
			monthAddData.setType(IndexData.TYPE_CLDT);
			monthAddData.setTenantId(tenant.getId());
			indexDataList.add(monthAddData);
			// 当月注销
			IndexData monthRduceData = new IndexData();
			monthRduceData.setName(IndexData.NAME_MONTHREDUCE);
			monthRduceData.setValue(monthReduce);
			monthRduceData.setType(IndexData.TYPE_CLDT);
			monthRduceData.setTenantId(tenant.getId());
			indexDataList.add(monthRduceData);
			// 当年动态
			IndexData yearDynamicData = new IndexData();
			yearDynamicData.setName(IndexData.NAME_YEARDYNAMIC);
			yearDynamicData.setValue(yearDynamic);
			yearDynamicData.setType(IndexData.TYPE_CLDT);
			yearDynamicData.setTenantId(tenant.getId());
			indexDataList.add(yearDynamicData);
		}
		batchSave(indexDataList);
	}

	/**
	 * 计算首页车辆状态数据
	 */
	public void calIndexStatusData() {
		List<IndexData> indexDataList = new ArrayList<IndexData>();
		List<Tenant> tenantList = listByHql("from Tenant where status=?", Tenant.STATUS_ENABLE);
		for (Tenant tenant : tenantList) {
			// 删除历史数据
			executeHql("delete from IndexData where type=? and tenantId=?", IndexData.TYPE_CLZT, tenant.getId());
			// 查询车辆
			List<Vehicle> allList = listByHql("from Vehicle where status!=-1 and tenantId=?", tenant.getId());
			List<Vehicle> vehicleList = new ArrayList<Vehicle>();
			// 有效车辆 :牵引车，正常，单挂车
			for (Vehicle vehicle : allList) {
				if (vehicle.getType().equals(Vehicle.TYPE_TRACTOR) || vehicle.getType().equals(Vehicle.TYPE_WHOLE)
						|| (vehicle.getType().equals(Vehicle.TYPE_TRAILER) && vehicle.getRelationVehicleId() == null)) {
					vehicleList.add(vehicle);
				}
			}
			List<Dict> dictList = listByHql("from Dict where dictGroup.code=? and tenantId=?", "vehicleStatus",
					tenant.getId());
			for (Dict dict : dictList) {
				int amount = 0;
				for (Vehicle vehicle : vehicleList) {
					if (dict.getId().equals(vehicle.getStatus())) {
						amount = amount + 1;
					}
				}
				IndexData indexData = new IndexData();
				indexData.setName(dict.getName());
				indexData.setValue(amount);
				indexData.setType(IndexData.TYPE_CLZT);
				indexData.setTenantId(tenant.getId());
				indexDataList.add(indexData);
			}
		}
		batchSave(indexDataList);
	}

	/**
	 * 计算首页到期数据
	 */
	public void calIndexWarnData() {
		// 到期列表
		List<IndexData> indexDataList = new ArrayList<IndexData>();
		// 10天，30天，60天
		Date after10Day = DateUtil.dateAdd(null, 10, true);
		Date after30Day = DateUtil.dateAdd(null, 30, true);
		Date after60Day = DateUtil.dateAdd(null, 60, true);
		List<Tenant> tenantList = listByHql("from Tenant where status=?", Tenant.STATUS_ENABLE);
		for (Tenant tenant : tenantList) {
			// 删除历史数据
			executeHql("delete from IndexData where type=? or type=? or type=? and tenantId=?", IndexData.TYPE_TX10,
					IndexData.TYPE_TX30, IndexData.TYPE_TX60, tenant.getId());
			/************ 车辆到期 ************/
			List<WarnType> warnTypeList = listByHql("from WarnType where status=? and tenantId=?",
					WarnType.STATUS_ENABLE, tenant.getId());
			List<VehicleWarn> vehicleWarnList = listByHql("from VehicleWarn where status=? and tenantId=?",
					VehicleWarn.STATUS_NORMAL, tenant.getId());
			for (WarnType warnType : warnTypeList) {
				int warnNumAfter10 = 0, warnNumAfter30 = 0, warnNumAfter60 = 0;
				for (VehicleWarn vehicleWarn : vehicleWarnList) {
					if (warnType.getId().equals(vehicleWarn.getWarnType().getId())) {
						if (vehicleWarn.getWarnDate().getTime() <= after10Day.getTime()) {
							// 10天内到期
							warnNumAfter10 = warnNumAfter10 + 1;
						}
						if (vehicleWarn.getWarnDate().getTime() <= after30Day.getTime()) {
							// 30天内到期
							warnNumAfter30 = warnNumAfter30 + 1;
						}
						if (vehicleWarn.getWarnDate().getTime() <= after60Day.getTime()) {
							// 60天内到期
							warnNumAfter60 = warnNumAfter60 + 1;
						}
					}
				}
				// 10天内到期
				IndexData indexData10 = new IndexData();
				indexData10.setName(warnType.getName());
				indexData10.setValue(warnNumAfter10);
				indexData10.setTenantId(tenant.getId());
				indexData10.setType(IndexData.TYPE_TX10);
				indexDataList.add(indexData10);
				// 30天内到期
				IndexData indexData30 = new IndexData();
				indexData30.setName(warnType.getName());
				indexData30.setValue(warnNumAfter30);
				indexData30.setTenantId(tenant.getId());
				indexData30.setType(IndexData.TYPE_TX30);
				indexDataList.add(indexData30);
				// 60天内到期
				IndexData indexData60 = new IndexData();
				indexData60.setName(warnType.getName());
				indexData60.setValue(warnNumAfter60);
				indexData60.setTenantId(tenant.getId());
				indexData60.setType(IndexData.TYPE_TX60);
				indexDataList.add(indexData60);
			}
			/************ 保险到期 ************/
			List<Insurance> insuranceList = listByHql("from Insurance where warn=? and tenantId=?",
					Insurance.WARN_NORMAL, tenant.getId());
			int insWarnNumAfter10 = 0, insWarnNumAfter30 = 0, insWarnNumAfter60 = 0;
			for (Insurance insurance : insuranceList) {
				if (insurance.getEndDate().getTime() <= after10Day.getTime()) {
					// 10天内到期
					insWarnNumAfter10 = insWarnNumAfter10 + 1;
				}
				if (insurance.getEndDate().getTime() <= after30Day.getTime()) {
					// 30天内到期
					insWarnNumAfter30 = insWarnNumAfter30 + 1;
				}
				if (insurance.getEndDate().getTime() <= after60Day.getTime()) {
					// 60天内到期
					insWarnNumAfter60 = insWarnNumAfter60 + 1;
				}
			}
			// 10天内到期
			IndexData insIndexData10 = new IndexData();
			insIndexData10.setName("保险到期");
			insIndexData10.setValue(insWarnNumAfter10);
			insIndexData10.setTenantId(tenant.getId());
			insIndexData10.setType(IndexData.TYPE_TX10);
			indexDataList.add(insIndexData10);
			// 30天内到期
			IndexData insIndexData30 = new IndexData();
			insIndexData30.setName("保险到期");
			insIndexData30.setValue(insWarnNumAfter30);
			insIndexData30.setTenantId(tenant.getId());
			insIndexData30.setType(IndexData.TYPE_TX30);
			indexDataList.add(insIndexData30);
			// 60天内到期
			IndexData insIndexData60 = new IndexData();
			insIndexData60.setName("保险到期");
			insIndexData60.setValue(insWarnNumAfter60);
			insIndexData60.setTenantId(tenant.getId());
			insIndexData60.setType(IndexData.TYPE_TX60);
			indexDataList.add(insIndexData60);
			/************ 财务到期 ************/
			List<Income> incomeList = listByHql("from Income where endDate is not null and status=? and tenantId=?",
					Income.STATUS_NO, tenant.getId());
			int incomeWarnNumAfter10 = 0, incomeWarnNumAfter30 = 0, incomeWarnNumAfter60 = 0;
			for (Income income : incomeList) {
				if (income.getEndDate().getTime() <= after10Day.getTime()) {
					// 10天内到期
					incomeWarnNumAfter10 = incomeWarnNumAfter10 + 1;
				}
				if (income.getEndDate().getTime() <= after30Day.getTime()) {
					// 30天内到期
					incomeWarnNumAfter30 = incomeWarnNumAfter30 + 1;
				}
				if (income.getEndDate().getTime() <= after60Day.getTime()) {
					// 60天内到期
					incomeWarnNumAfter60 = incomeWarnNumAfter60 + 1;
				}
			}
			// 10天内到期
			IndexData incomeIndexData10 = new IndexData();
			incomeIndexData10.setName("财务到期");
			incomeIndexData10.setValue(incomeWarnNumAfter10);
			incomeIndexData10.setTenantId(tenant.getId());
			incomeIndexData10.setType(IndexData.TYPE_TX10);
			indexDataList.add(incomeIndexData10);
			// 30天内到期
			IndexData incomeIndexData30 = new IndexData();
			incomeIndexData30.setName("财务到期");
			incomeIndexData30.setValue(incomeWarnNumAfter30);
			incomeIndexData30.setTenantId(tenant.getId());
			incomeIndexData30.setType(IndexData.TYPE_TX30);
			indexDataList.add(incomeIndexData30);
			// 60天内到期
			IndexData incomeIndexData60 = new IndexData();
			incomeIndexData60.setName("财务到期");
			incomeIndexData60.setValue(incomeWarnNumAfter60);
			incomeIndexData60.setTenantId(tenant.getId());
			incomeIndexData60.setType(IndexData.TYPE_TX60);
			indexDataList.add(incomeIndexData60);
			/************ 贷款到期 ************/
			List<Loan> loanList = listByHql("from Loan where status!=? and tenantId=?", Loan.STATUS_YES,
					tenant.getId());
			int loanWarnNumAfter10 = 0, loanWarnNumAfter30 = 0, loanWarnNumAfter60 = 0;
			for (Loan loan : loanList) {
				if (loan.getEndDate().getTime() <= after10Day.getTime()) {
					// 10天内到期
					loanWarnNumAfter10 = loanWarnNumAfter10 + 1;
				}
				if (loan.getEndDate().getTime() <= after30Day.getTime()) {
					// 30天内到期
					loanWarnNumAfter30 = loanWarnNumAfter30 + 1;
				}
				if (loan.getEndDate().getTime() <= after60Day.getTime()) {
					// 60天内到期
					loanWarnNumAfter60 = loanWarnNumAfter60 + 1;
				}
			}
			// 10天内到期
			IndexData loanIndexData10 = new IndexData();
			loanIndexData10.setName("贷款到期");
			loanIndexData10.setValue(loanWarnNumAfter10);
			loanIndexData10.setTenantId(tenant.getId());
			loanIndexData10.setType(IndexData.TYPE_TX10);
			indexDataList.add(loanIndexData10);
			// 30天内到期
			IndexData loanIndexData30 = new IndexData();
			loanIndexData30.setName("贷款到期");
			loanIndexData30.setValue(loanWarnNumAfter30);
			loanIndexData30.setTenantId(tenant.getId());
			loanIndexData30.setType(IndexData.TYPE_TX30);
			indexDataList.add(loanIndexData30);
			// 60天内到期
			IndexData loanIndexData60 = new IndexData();
			loanIndexData60.setName("贷款到期");
			loanIndexData60.setValue(loanWarnNumAfter60);
			loanIndexData60.setTenantId(tenant.getId());
			loanIndexData60.setType(IndexData.TYPE_TX60);
			indexDataList.add(loanIndexData60);
		}
		batchSave(indexDataList);
	}

	/**
	 * 注销责任人
	 */
	public void disableOwner() {
		List<Owner> ownerList = listAll(Owner.class);
		for (Owner owner : ownerList) {
			String vehicleHql = "from Vehicle where owner.id=? and status!=? and tenantId=?";
			List<Vehicle> vehicleList = listByHql(vehicleHql, owner.getId(), Vehicle.STATUS_DISABLE,
					owner.getTenantId());
			if (CollectionUtils.isEmpty(vehicleList)) {
				// 责任人名下车辆全部为注销状态
				String loanHql = "from LoanContract where owner.id=? and status!=? and tenantId=?";
				List<LoanContract> loanContractList = listByHql(loanHql, owner.getId(), LoanContract.STATUS_YES,
						owner.getTenantId());
				if (CollectionUtils.isEmpty(loanContractList)) {
					// 没有借款,注销责任人
					String hql = "update Owner set name=?,status=? where id=? and tenantId=?";
					executeHql(hql, owner.getName().concat("(注销)"), Owner.STATUS_DISABLE, owner.getId(),
							owner.getTenantId());
				}
			}
		}
	}

	/**
	 * 注销司机
	 */
	public void disableDriver() {
		List<Driver> driverList = listAll(Driver.class);
		for (Driver driver : driverList) {
			String vehicleHql = "select v from Driver d,Vehicle v where d in elements(v.driverList) and d.id=? and v.status!=? and d.tenantId=?";
			List<Vehicle> vehicleList = listByHql(vehicleHql, driver.getId(), Vehicle.STATUS_DISABLE,
					driver.getTenantId());
			if (CollectionUtils.isEmpty(vehicleList)) {
				// 司机名下车辆全部为注销状态,注销司机
				String hql = "update Driver set name=?,status=? where id=? and tenantId=?";
				executeHql(hql, driver.getName().concat("(注销)"), Driver.STATUS_DISABLE, driver.getId(),
						driver.getTenantId());
			}
		}
	}

	/**
	 * 计算车辆动态报表数据
	 */
	public void calRptVehicleDynamicData() {
		List<RptVehicle> rptVehicleList = new ArrayList<RptVehicle>();
		List<Tenant> tenantList = listByHql("from Tenant where status=?", Tenant.STATUS_ENABLE);
		for (Tenant tenant : tenantList) {
			// 查询车辆
			List<Vehicle> allList = listByHql("from Vehicle where tenantId=?", tenant.getId());
			List<Vehicle> vehicleList = new ArrayList<Vehicle>();
			// 有效车辆 :牵引车，正常，单挂车
			for (Vehicle vehicle : allList) {
				if (vehicle.getType().equals(Vehicle.TYPE_TRACTOR) || vehicle.getType().equals(Vehicle.TYPE_WHOLE)
						|| (vehicle.getType().equals(Vehicle.TYPE_TRAILER) && vehicle.getRelationVehicleId() == null)) {
					vehicleList.add(vehicle);
				}
			}
			// 计算当月新增，当月注销
			int currentNum = 0, monthAdd = 0, monthVoid = 0;
			// 当前日期,当月第一天
			Date currentDate = new Date();
			Date monthFirstDay = DateUtil.minDateOfMonth(currentDate);
			// 当前年份,月份
			String year = DateUtil.dateToStr(currentDate, "yyyy");
			int month = DateUtil.getMonth(currentDate);
			for (Vehicle vehicle : vehicleList) {
				// 当月车辆
				if (!vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					currentNum = currentNum + 1;
				}
				// 当月新增
				if (vehicle.getCreateDate().getTime() >= monthFirstDay.getTime()
						&& vehicle.getCreateDate().getTime() <= currentDate.getTime()) {
					monthAdd = monthAdd + 1;
				}
				// 当月注销
				if (vehicle.getVoidDate() != null && vehicle.getVoidDate().getTime() >= monthFirstDay.getTime()
						&& vehicle.getVoidDate().getTime() <= currentDate.getTime()
						&& vehicle.getStatus().equals(Vehicle.STATUS_DISABLE)) {
					monthVoid = monthVoid + 1;
				}
			}
			// 新增vehicleRptModel
			String hqlAdd = "from RptVehicle where name='新增' and year=? and tenantId=?";
			RptVehicle rptVehicleAdd = getUniqueByHql(hqlAdd, year, tenant.getId());
			if (rptVehicleAdd == null) {
				rptVehicleAdd = new RptVehicle();
				rptVehicleAdd.setTenantId(tenant.getId());
				rptVehicleAdd.setType(RptVehicle.TYPE_DYNAMIC);
				rptVehicleAdd.setName("新增");
				rptVehicleAdd.setYear(year);
				rptVehicleAdd.setSort(1);
			}
			// 注销vehicleRptModel
			String hqlVoid = "from RptVehicle where name='注销' and year=? and tenantId=?";
			RptVehicle rptVehicleVoid = getUniqueByHql(hqlVoid, year, tenant.getId());
			if (rptVehicleVoid == null) {
				rptVehicleVoid = new RptVehicle();
				rptVehicleVoid.setTenantId(tenant.getId());
				rptVehicleVoid.setType(RptVehicle.TYPE_DYNAMIC);
				rptVehicleVoid.setName("注销");
				rptVehicleVoid.setYear(year);
				rptVehicleVoid.setSort(2);
			}
			// 当月RptVehicle
			String hqlCurrent = "from RptVehicle where name='结余' and year=? and tenantId=?";
			RptVehicle rptVehicleCurrent = getUniqueByHql(hqlCurrent, year, tenant.getId());
			if (rptVehicleCurrent == null) {
				rptVehicleCurrent = new RptVehicle();
				rptVehicleCurrent.setTenantId(tenant.getId());
				rptVehicleCurrent.setType(RptVehicle.TYPE_DYNAMIC);
				rptVehicleCurrent.setName("结余");
				rptVehicleCurrent.setYear(year);
				rptVehicleCurrent.setSort(3);
			}
			// 数据
			switch (month) {
			case 1:
				rptVehicleAdd.setM1(monthAdd);
				rptVehicleVoid.setM1(monthVoid);
				rptVehicleCurrent.setM1(currentNum);
				break;
			case 2:
				rptVehicleAdd.setM2(monthAdd);
				rptVehicleVoid.setM2(monthVoid);
				rptVehicleCurrent.setM2(currentNum);
				break;
			case 3:
				rptVehicleAdd.setM3(monthAdd);
				rptVehicleVoid.setM3(monthVoid);
				rptVehicleCurrent.setM3(currentNum);
				break;
			case 4:
				rptVehicleAdd.setM4(monthAdd);
				rptVehicleVoid.setM4(monthVoid);
				rptVehicleCurrent.setM4(currentNum);
				break;
			case 5:
				rptVehicleAdd.setM5(monthAdd);
				rptVehicleVoid.setM5(monthVoid);
				rptVehicleCurrent.setM5(currentNum);
				break;
			case 6:
				rptVehicleAdd.setM6(monthAdd);
				rptVehicleVoid.setM6(monthVoid);
				rptVehicleCurrent.setM6(currentNum);
				break;
			case 7:
				rptVehicleAdd.setM7(monthAdd);
				rptVehicleVoid.setM7(monthVoid);
				rptVehicleCurrent.setM7(currentNum);
				break;
			case 8:
				rptVehicleAdd.setM8(monthAdd);
				rptVehicleVoid.setM8(monthVoid);
				rptVehicleCurrent.setM8(currentNum);
				break;
			case 9:
				rptVehicleAdd.setM9(monthAdd);
				rptVehicleVoid.setM9(monthVoid);
				rptVehicleCurrent.setM9(currentNum);
				break;
			case 10:
				rptVehicleAdd.setM10(monthAdd);
				rptVehicleVoid.setM10(monthVoid);
				rptVehicleCurrent.setM10(currentNum);
				break;
			case 11:
				rptVehicleAdd.setM11(monthAdd);
				rptVehicleVoid.setM11(monthVoid);
				rptVehicleCurrent.setM12(currentNum);
				break;
			case 12:
				rptVehicleAdd.setM12(monthAdd);
				rptVehicleVoid.setM12(monthVoid);
				rptVehicleCurrent.setM12(currentNum);
				break;
			default:
				break;
			}

			// 保存
			rptVehicleList.add(rptVehicleAdd);
			rptVehicleList.add(rptVehicleVoid);
			rptVehicleList.add(rptVehicleCurrent);
		}
		for (RptVehicle entity : rptVehicleList) {
			super.saveOrUpdate(entity);
		}
	}

	/**
	 * 计算车辆状态报表数据
	 */
	public void calRptVehicleStatusData() {
		List<RptVehicle> rptVehicleList = new ArrayList<RptVehicle>();
		List<Tenant> tenantList = listByHql("from Tenant where status=?", Tenant.STATUS_ENABLE);
		for (Tenant tenant : tenantList) {
			// 车辆状态
			List<Dict> statusList = listByHql("from Dict where tenantId=? and dictGroup.code=?", tenant.getId(),
					"vehicleStatus");
			Map<String, Dict> statusMap = new HashMap<String, Dict>();
			Map<String, List<Vehicle>> vehicleMap = new HashMap<String, List<Vehicle>>();
			for (Dict dict : statusList) {
				statusMap.put(dict.getId(), dict);
				vehicleMap.put(dict.getId(), new ArrayList<Vehicle>());
			}
			// 查询车辆
			List<Vehicle> allList = listByHql("from Vehicle where tenantId=?", tenant.getId());
			List<Vehicle> vehicleList = new ArrayList<Vehicle>();
			// 有效车辆 :牵引车，正常，单挂车
			for (Vehicle vehicle : allList) {
				if (vehicle.getType().equals(Vehicle.TYPE_TRACTOR) || vehicle.getType().equals(Vehicle.TYPE_WHOLE)
						|| (vehicle.getType().equals(Vehicle.TYPE_TRAILER) && vehicle.getRelationVehicleId() == null)) {
					vehicleList.add(vehicle);
				}
			}
			// 当前日期,当月第一天
			Date currentDate = new Date();
			// 当前年份,月份
			String year = DateUtil.dateToStr(currentDate, "yyyy");
			int month = DateUtil.getMonth(currentDate);
			for (Vehicle vehicle : vehicleList) {
				vehicleMap.get(vehicle.getStatus()).add(vehicle);
			}
			for (String status : vehicleMap.keySet()) {
				String hql = "from RptVehicle where name=? and year=? and tenantId=?";
				RptVehicle rptVehicle = getUniqueByHql(hql, statusMap.get(status).getName(), year, tenant.getId());
				if (rptVehicle == null) {
					rptVehicle = new RptVehicle();
					rptVehicle.setTenantId(tenant.getId());
					rptVehicle.setType(RptVehicle.TYPE_STATUS);
					rptVehicle.setName(statusMap.get(status).getName());
					rptVehicle.setYear(year);
					rptVehicle.setSort(statusMap.get(status).getOrder());
				}
				// 数据
				switch (month) {
				case 1:
					rptVehicle.setM1(vehicleMap.get(status).size());
					break;
				case 2:
					rptVehicle.setM2(vehicleMap.get(status).size());
					break;
				case 3:
					rptVehicle.setM3(vehicleMap.get(status).size());
					break;
				case 4:
					rptVehicle.setM4(vehicleMap.get(status).size());
					break;
				case 5:
					rptVehicle.setM5(vehicleMap.get(status).size());
					break;
				case 6:
					rptVehicle.setM6(vehicleMap.get(status).size());
					break;
				case 7:
					rptVehicle.setM7(vehicleMap.get(status).size());
					break;
				case 8:
					rptVehicle.setM8(vehicleMap.get(status).size());
					break;
				case 9:
					rptVehicle.setM9(vehicleMap.get(status).size());
					break;
				case 10:
					rptVehicle.setM10(vehicleMap.get(status).size());
					break;
				case 11:
					rptVehicle.setM11(vehicleMap.get(status).size());
					break;
				case 12:
					rptVehicle.setM12(vehicleMap.get(status).size());
					break;
				default:
					break;
				}
				rptVehicleList.add(rptVehicle);
			}
		}
		// 保存
		for (RptVehicle model : rptVehicleList) {
			super.saveOrUpdate(model);
		}
	}

}
