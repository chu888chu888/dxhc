package manage.sys.service;

import luxing.common.service.CommonService;
import manage.sys.entity.UserSession;

public interface UserSessionService extends CommonService {

	public UserSession getByUser(String userId);

	public void update(UserSession userSession);

	public void save(UserSession userSession);

}
