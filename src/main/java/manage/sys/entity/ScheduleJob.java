package manage.sys.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 定时任务
 * 
 * @author zzc
 */

@Entity
@Table(name = "manage_schedule_job")
@DynamicInsert
@DynamicUpdate
public class ScheduleJob implements Serializable {

	private static final long serialVersionUID = -1839988078064609474L;

	public static final String JOB_PARAM_KEY = "JOB_PARAM_KEY";
	public static final String STATUS_NORMAL = "0";
	public static final String STATUS_PAUSE = "1";

	private String id;
	/** 任务名称 */
	private String jobName;
	/** spring bean名称 */
	private String beanName;
	/** 方法名称 */
	private String methodName;
	/** 参数 */
	private String params;
	/** cron表达式 */
	private String cronExpression;
	/** 任务状态 */
	private String status;
	/** 备注 */
	private String remarks;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "job_name", nullable = false, length = 50)
	@NotBlank(message = "任务名称不能为空")
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Column(name = "bean_name", nullable = false, length = 50)
	@NotBlank(message = "bean名称不能为空")
	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	@Column(name = "method_name", nullable = false, length = 50)
	@NotBlank(message = "方法名称不能为空")
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Column(name = "params", length = 50)
	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	@Column(name = "cron_expression", nullable = false, length = 50)
	@NotBlank(message = "cron表达式不能为空")
	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
