package manage.tenant.service;

import java.util.List;

import luxing.common.service.CommonService;
import manage.tenant.entity.Recharge;
import manage.tenant.entity.Tenant;
import modules.sys.entity.User;

/**
 * 
 * @author zzc
 *
 */
public interface TenantService extends CommonService {

	void save(Tenant tenant, User user, Recharge recharge);

	void delete(Tenant tenant);

	List<Tenant> listByUser(String userId);

	String getMaxNumber(String cityId);

}
