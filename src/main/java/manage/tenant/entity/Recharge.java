package manage.tenant.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 租户充值
 * 
 * @author zzc
 */
@Entity
@Table(name = "manage_recharge")
@DynamicInsert
@DynamicUpdate
public class Recharge implements Serializable {

	private static final long serialVersionUID = -1251384596544999088L;

	public static final String WAY_WEIXIN = "1"; // 微信
	public static final String WAY_ALIPAY = "2"; // 支付宝
	public static final String WAY_BANK = "3"; // 银行转账
	public static final String WAY_CASH = "4"; // 现金

	public static final String STATUS_ENABLE = "0"; // 正常
	public static final String STATUS_DISABLE = "1"; // 作废

	private String id;
	/** 租户 */
	private Tenant tenant;
	/** 金额 */
	private BigDecimal amount;
	/** 充值方式 */
	private String way;
	/** 充值日期 */
	private Date rechargeDate;
	/** 状态 */
	private String status;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tenant_id")
	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Column(name = "amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "way", nullable = false, length = 10)
	@NotBlank(message = "充值方式不能为空")
	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "recharge_date")
	public Date getRechargeDate() {
		return rechargeDate;
	}

	public void setRechargeDate(Date rechargeDate) {
		this.rechargeDate = rechargeDate;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}