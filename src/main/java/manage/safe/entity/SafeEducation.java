package manage.safe.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 安全教育
 * 
 * @author zzc
 */
@Entity
@Table(name = "manage_education")
@DynamicInsert
@DynamicUpdate
public class SafeEducation implements Serializable {

	private static final long serialVersionUID = -7894242042131851362L;

	public static final String STATUS_OPEN = "0";// 开放
	public static final String STATUS_CLOSE = "1";// 关闭

	private String id;
	/** 课程名称 */
	private String name;
	/** 分类: 货运_1,危险品_2 */
	private String type;
	/** 课程时长 */
	private Integer duration;
	/** 状态: 开放_0,关闭_1 */
	private String status;
	/** 内容 */
	private String content;
	/** 封面图片cover */
	private String cover;
	/** 创建时间 */
	private Date createDate;
	/** 图片url */
	private String url;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 50)
	@NotBlank(message = "名称不能为空")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "分类不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "duration")
	@Min(0)
	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "content", nullable = false)
	@NotBlank
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "cover", nullable = false, length = 255)
	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Transient
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}