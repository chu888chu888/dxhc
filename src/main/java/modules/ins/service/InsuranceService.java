package modules.ins.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.ins.entity.Insurance;

public interface InsuranceService extends CommonService {

	void save(Insurance insurance);

	void delete(Insurance insurance);

	List<Insurance> listByVehicleAndType(String vehicleId, String type);

	List<Insurance> listByVehicle(String vehicleId);

	void updateWarn(String vehicleId, String warn);

}
