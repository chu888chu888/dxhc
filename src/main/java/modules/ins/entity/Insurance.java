package modules.ins.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import modules.arc.entity.Vehicle;

/**
 * 车辆保险
 * 
 * @author zzc
 */
@Entity
@Table(name = "ins_insurance")
@DynamicInsert
@DynamicUpdate
public class Insurance implements Serializable {

	private static final long serialVersionUID = -3397777138013419154L;

	public static final String TYPE_SALI = "1";// 交强险
	public static final String TYPE_CI = "2";// 商业险
	public static final String STATUS_ENABLE = "0";// 有效
	public static final String STATUS_DISABLE = "1";// 无效
	public static final String WARN_NORMAL = "0";// 正常
	public static final String WARN_PAUSE = "1";// 暂停
	public static final String WARN_ALL = "-1";// 全部

	private String id;
	/** 租户 */
	private String tenantId;
	/** 车辆 */
	private Vehicle vehicle;
	/** 保险类型 */
	private String type;
	/** 保险公司 */
	private String insCompany;
	/** 生效日期 */
	private Date startDate;
	/** 到期日期 */
	private Date endDate;
	/** 保费 */
	private BigDecimal premium;
	/** 车船税vehicle and vessel tax */
	private BigDecimal vavt;
	/** 状态 */
	private String status;
	/** 提醒状态 1、null 2、正常 3、暂停 */
	private String warn;
	/** 备注 */
	private String remarks;
	/** 创建时间 */
	private Date createDate;
	/** 创建人ID */
	private String createBy;
	/** 修改时间 */
	private Date updateDate;
	/** 修改人 */
	private String updateBy;
	/** 投保险种 */
	private List<InsuranceKind> insuranceKindList = new ArrayList<InsuranceKind>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@Column(name = "type", nullable = false, length = 10)
	@NotBlank(message = "类型不能为空")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "ins_company", nullable = false, length = 32)
	public String getInsCompany() {
		return insCompany;
	}

	public void setInsCompany(String insCompany) {
		this.insCompany = insCompany;
	}

	@Column(name = "start_date")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "premium", nullable = false, scale = 1, length = 10)
	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	@Column(name = "vavt", nullable = false, scale = 1, length = 10)
	public BigDecimal getVavt() {
		return vavt;
	}

	public void setVavt(BigDecimal vavt) {
		this.vavt = vavt;
	}

	@Column(name = "status", nullable = false, length = 10)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "warn", length = 10)
	public String getWarn() {
		return warn;
	}

	public void setWarn(String warn) {
		this.warn = warn;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "insurance")
	@Fetch(FetchMode.SUBSELECT)
	public List<InsuranceKind> getInsuranceKindList() {
		return insuranceKindList;
	}

	public void setInsuranceKindList(List<InsuranceKind> insuranceKindList) {
		this.insuranceKindList = insuranceKindList;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "update_date", nullable = true)
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "update_by", nullable = true, length = 32)
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}