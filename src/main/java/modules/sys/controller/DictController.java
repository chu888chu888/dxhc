package modules.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.LoginUserUtil;
import luxing.util.ValidatorUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import luxing.web.model.SortDirection;
import modules.sys.entity.Dict;
import modules.sys.entity.DictGroup;
import modules.sys.entity.Log;
import modules.sys.service.DictGroupService;
import modules.sys.service.DictService;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/dictController")
public class DictController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(DictController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private DictGroupService dictGroupService;
	@Autowired
	private DictService dictService;

	/**
	 * 分类列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "dictGroupIndex")
	public ModelAndView dictGroupIndex(HttpServletRequest request) {
		return new ModelAndView("modules/sys/dictGroup-list");
	}

	/**
	 * 分类列表数据
	 * 
	 * @param dictGroup
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "dictGroupList")
	public void dictGroupList(DictGroup dictGroup, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(DictGroup.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, dictGroup, request.getParameterMap());
			cq.add();
			dictGroupService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("分类组查询失败", e);
			throw new BusinessException("分类组询失败", e);
		}
	}

	/**
	 * 分类列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		String dictGroupId = request.getParameter("dictGroupId");
		request.setAttribute("dictGroupId", dictGroupId);
		return new ModelAndView("modules/sys/dict-list");
	}

	/**
	 * 分类列表数据
	 * 
	 * @param dict
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Dict dict, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Dict.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, dict, request.getParameterMap());
			cq.eq("dictGroup.id", request.getParameter("dictGroupId"));
			cq.addOrder("order", SortDirection.asc);
			cq.add();
			dictService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("分类查询失败", e);
			throw new BusinessException("分类查询失败", e);
		}
	}

	/**
	 * 增加页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(HttpServletRequest req) {
		String dictGroupId = req.getParameter("dictGroupId");
		DictGroup dictGroup = dictGroupService.get(DictGroup.class, dictGroupId);
		req.setAttribute("dictGroupPage", dictGroup);
		return new ModelAndView("modules/sys/dict-add");
	}

	/**
	 * 增加页面-自定义
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAddCustom")
	public ModelAndView goAddCustom(HttpServletRequest req) {
		String groupCode = req.getParameter("groupCode");
		DictGroup dictGroup = dictGroupService.getByCode(groupCode);
		req.setAttribute("dictGroupPage", dictGroup);
		return new ModelAndView("modules/sys/dictCustom-add");
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(Dict dict, HttpServletRequest req) {
		if (StringUtils.isNotBlank(dict.getId())) {
			dict = dictService.get(Dict.class, dict.getId());
			req.setAttribute("dictPage", dict);
		}
		return new ModelAndView("modules/sys/dict-update");
	}

	/**
	 * 增加
	 * 
	 * @param dict
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public Result doAdd(Dict dict, HttpServletRequest request) {
		String msg = null;
		try {
			// bean校验
			msg = ValidatorUtil.validate(dict);
			if (StringUtils.isNotBlank(msg)) {
				return Result.error(msg);
			}
			dictService.save(dict, LoginUserUtil.getLoginUser().getTenantId());
			msg = "分类\"" + dict.getName() + "\"增加成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_ADD);
		} catch (Exception e) {
			msg = "分类\"" + dict.getName() + "\"增加失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}


	/**
	 * 修改
	 * 
	 * @param dict
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public Result doUpdate(Dict dict, HttpServletRequest request) {
		String msg = null;
		try {
			dictService.update(dict);
			msg = "分类\"" + dict.getName() + "\"修改成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "分类\"" + dict.getName() + "\"修改失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public Result doDel(Dict dict, HttpServletRequest request) {
		String msg = null;
		try {
			dict = dictService.get(Dict.class, dict.getId());
			dictService.delete(dict);
			msg = "分类\"" + dict.getName() + "\"删除成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_DEL);
		} catch (Exception e) {
			msg = "分类\"" + dict.getName() + "\"删除失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
