package modules.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.LoginUserUtil;
import luxing.web.model.DataGrid;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 日志处理类
 * 
 * @author zzc
 * 
 */
@Controller
@RequestMapping("/logController")
public class LogController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(LogController.class);

	@Autowired
	private LogService logService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index() {
		return new ModelAndView("modules/sys/log-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param log
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Log log, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Log.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, log, request.getParameterMap());
			cq.eq("tenant.id", LoginUserUtil.getLoginUser().getTenantId());
			cq.add();
			logService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("日志查询失败", e);
			throw new BusinessException("日志查询失败", e);
		}
	}

	/**
	 * 查看页面
	 * 
	 * @param log
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "goDetail")
	public ModelAndView goDetail(Log log, HttpServletRequest request) {
		if (StringUtils.isNotBlank(log.getId())) {
			log = logService.get(Log.class, log.getId());
			request.setAttribute("logPage", log);
		}
		return new ModelAndView("modules/sys/log-detail");
	}
}
