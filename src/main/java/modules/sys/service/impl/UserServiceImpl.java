package modules.sys.service.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.SecretUtil;
import modules.sys.entity.User;
import modules.sys.service.UserService;

/**
 * 
 * @author zzc
 *
 */
@Service("userService")
@Transactional
public class UserServiceImpl extends CommonServiceImpl implements UserService {

	public User loginCheck(User user) {
		String password = SecretUtil.encrypt(user.getPhone(), user.getPassword(), SecretUtil.getStaticSalt());
		String hql = "from User where phone=? and password=?";
		return getUniqueByHql(hql, user.getPhone(), password);
	}

	public User getByTenantAndPhone(String tenantId, String phone) {
		String hql = "select u from User u,Tenant t where u in elements(t.userList) and t.id=? and u.phone=?";
		return getUniqueByHql(hql, tenantId, phone);
	}

	public User getByPhone(String phone) {
		String hql = "from User where phone=?";
		return getUniqueByHql(hql, phone);
	}

	public void updatePassword(User user) {
		saveOrUpdate(user);
		// 删除登录记录
		String hql = "delete from UserLogin where phone=?";
		executeHql(hql, user.getPhone());
	}

	@CacheEvict(value = { "menuCache", "operationCache" }, allEntries = true)
	public void updateAuth(User user) {
		saveOrUpdate(user);
	}

}
