package modules.sys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.sys.entity.UserLogin;
import modules.sys.service.UserLoginService;

/**
 * 
 * @author zzc
 *
 */
@Service("userLoginService")
@Transactional
public class UserLoginServiceImpl extends CommonServiceImpl implements UserLoginService {

	public UserLogin getByPhone(String phone) {
		String hql = "from UserLogin where phone=?";
		return getUniqueByHql(hql, phone);
	}

	public List<UserLogin> listByIP(String ip) {
		String hql = "from UserLogin where ip=? and loginNum>=10";
		return listByHql(hql, ip);
	}

}
