package modules.sys.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import luxing.util.ManageLoginUserUtil;
import manage.sys.entity.ManageLog;
import manage.tenant.entity.Tenant;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 日志Service接口实现类
 * 
 * @author 方文荣
 *
 */
@Service("logService")
public class LogServiceImpl extends CommonServiceImpl implements LogService {

	/**
	 * 添加日志
	 */
	public void addLogInfo(String content, String operateType) {
		if (LoginUserUtil.getUser() != null) {
			Log log = new Log();
			Tenant tenant = new Tenant();
			tenant.setId(LoginUserUtil.getLoginUser().getTenantId());
			log.setTenant(tenant);
			log.setPhone(LoginUserUtil.getUser().getPhone());
			log.setUserName(LoginUserUtil.getUser().getName());
			log.setContent(content);
			log.setOperateType(operateType);
			log.setIp(LoginUserUtil.getLoginUser().getIp());
			log.setOperateTime(new Date());
			save(log);
		} else if (ManageLoginUserUtil.getUser() != null) {
			ManageLog manageLog = new ManageLog();
			manageLog.setUserName(ManageLoginUserUtil.getUser().getName());
			manageLog.setContent(content);
			manageLog.setOperateType(operateType);
			manageLog.setIp(ManageLoginUserUtil.getLoginUser().getIp());
			manageLog.setOperateTime(new Date());
			save(manageLog);
		}
	}

	/**
	 * 添加日志
	 */
	public void addLogError(String content, String operateType) {
		if (LoginUserUtil.getUser() != null) {
			Log log = new Log();
			Tenant tenant = new Tenant();
			tenant.setId(LoginUserUtil.getLoginUser().getTenantId());
			log.setTenant(tenant);
			log.setPhone(LoginUserUtil.getUser().getPhone());
			log.setUserName(LoginUserUtil.getUser().getName());
			log.setContent(content);
			log.setOperateType(operateType);
			log.setIp(LoginUserUtil.getLoginUser().getIp());
			log.setOperateTime(new Date());
			save(log);
		} else if (ManageLoginUserUtil.getUser() != null) {
			ManageLog manageLog = new ManageLog();
			manageLog.setUserName(ManageLoginUserUtil.getUser().getName());
			manageLog.setContent(content);
			manageLog.setOperateType(operateType);
			manageLog.setIp(ManageLoginUserUtil.getLoginUser().getIp());
			manageLog.setOperateTime(new Date());
			save(manageLog);
		}

	}

}
