package modules.sys.service.impl;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.sys.entity.Dict;
import modules.sys.service.DictService;

/**
 * 
 * @author zzc
 *
 */
@Service("dcitService")
@Transactional
public class DictServiceImpl extends CommonServiceImpl implements DictService {

	@Cacheable(value = "dictCache", key = "#groupCode.concat(#tenantId)")
	public List<Dict> listByGroupCode(String groupCode, String tenantId) {
		String hql = "from Dict where dictGroup.code=? and tenantId=? order by order asc";
		return listByHql(hql, groupCode, tenantId);
	}

	@CacheEvict(value = "dictCache", key = "#dict.dictGroup.code.concat(#tenantId)")
	public void save(Dict dict, String tenantId) {
		super.save(dict);
	}

	@CacheEvict(value = "dictCache", key = "#dict.dictGroup.code.concat(#dict.tenantId)")
	public void update(Dict dict) {
		super.saveOrUpdate(dict);
	}

	@CacheEvict(value = "dictCache", key = "#dict.dictGroup.code.concat(#dict.tenantId)")
	public void delete(Dict dict) {
		super.delete(dict);
	}
}
