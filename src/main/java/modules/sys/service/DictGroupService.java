package modules.sys.service;

import luxing.common.service.CommonService;
import modules.sys.entity.DictGroup;

public interface DictGroupService extends CommonService {

	DictGroup getByName(String name);

	DictGroup getByCode(String code);

}
