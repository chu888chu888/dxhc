package modules.sys.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.sys.entity.Dict;

public interface DictService extends CommonService {

	List<Dict> listByGroupCode(String groupCode, String tenantId);

	void save(Dict dict, String tenantId);

	void update(Dict dict);

	void delete(Dict dict);
}
