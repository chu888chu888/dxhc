package modules.sys.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.sys.entity.UserLogin;

/**
 * 
 * @author zzc
 *
 */
public interface UserLoginService extends CommonService {

	UserLogin getByPhone(String phone);

	List<UserLogin> listByIP(String ip);

}
