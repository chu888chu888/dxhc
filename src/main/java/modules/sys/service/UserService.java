package modules.sys.service;

import luxing.common.service.CommonService;
import modules.sys.entity.User;

/**
 * 
 * @author zzc
 *
 */
public interface UserService extends CommonService {

	User loginCheck(User user);

	User getByTenantAndPhone(String tenantId, String phone);

	User getByPhone(String phone);

	void updatePassword(User user);

	void updateAuth(User user);

}
