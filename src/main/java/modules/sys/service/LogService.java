/**
 * 
 */
package modules.sys.service;

import luxing.common.service.CommonService;

/**
 * 日志Service接口
 * 
 * @author 方文荣
 *
 */
public interface LogService extends CommonService {

	/**
	 * 日志添加
	 * 
	 * @param content
	 *            内容
	 * @param operateType
	 *            类型
	 */
	void addLogInfo(String content, String operateType);

	/**
	 * 日志添加
	 * 
	 * @param content
	 *            内容
	 * @param operateType
	 *            类型
	 */
	void addLogError(String content, String operateType);

}
