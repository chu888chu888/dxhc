package modules.rpt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.web.model.DataGrid;
import modules.rpt.entity.RptVehicle;
import modules.rpt.service.RptVehicleService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/rptVehicleController")
public class RptVehicleController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(RptVehicleController.class);

	@Autowired
	private RptVehicleService rptVehicleService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/rpt/rptVehicle-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param rptVehicle
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(RptVehicle rptVehicle, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			String rptType = request.getParameter("rptType");
			CriteriaQuery cq = new CriteriaQuery(RptVehicle.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, rptVehicle, request.getParameterMap());
			cq.eq("type", rptType);
			cq.add();
			rptVehicleService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("车辆报表查询失败", e);
			throw new BusinessException("车辆报表查询失败", e);
		}
	}

}
