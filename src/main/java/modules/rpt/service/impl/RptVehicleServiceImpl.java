package modules.rpt.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.rpt.service.RptVehicleService;

/**
 * 
 * @author zzc
 *
 */
@Service("rptVehicleService")
@Transactional
public class RptVehicleServiceImpl extends CommonServiceImpl implements RptVehicleService {

}
