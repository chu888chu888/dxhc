package modules.arc.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import luxing.common.service.CommonService;
import modules.arc.entity.ArcExt;
import modules.arc.entity.Vehicle;
import modules.arc.entity.VehicleExt;
import modules.warn.entity.WarnType;

public interface VehicleService extends CommonService {

	void save(Vehicle vehicle, List<ArcExt> arcExtList, List<WarnType> warnTypeList, HttpServletRequest request);

	void update(Vehicle vehicle, List<VehicleExt> vehicleExtList);

	void delete(Vehicle vehicle);

	Vehicle getByPlateNumber(String plateNumber);

	List<Vehicle> listEnableBySn(String sn);

	void disable(Vehicle vehicle);

}
