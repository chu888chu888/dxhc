package modules.arc.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import luxing.common.service.CommonService;
import modules.arc.entity.ArcExt;
import modules.arc.entity.Driver;
import modules.arc.entity.DriverExt;

public interface DriverService extends CommonService {

	void save(Driver driver, List<ArcExt> driverArcExtList, HttpServletRequest requests);

	void update(Driver driver, List<DriverExt> driverExtList);

	void delete(Driver driver);

	Driver getByName(String name);

	Driver getByPhone(String phone);

	Driver getByIdCard(String idCard);

}
