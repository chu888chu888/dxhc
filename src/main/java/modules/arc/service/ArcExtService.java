package modules.arc.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.arc.entity.ArcExt;

public interface ArcExtService extends CommonService {

	List<ArcExt> listByType(String type);

	void delete(ArcExt arcExt);
}
