package modules.arc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.VehicleExt;
import modules.arc.service.VehicleExtService;

/**
 * 
 * @author zzc
 *
 */
@Service("vehicleExtService")
@Transactional
public class VehicleExtServiceImpl extends CommonServiceImpl implements VehicleExtService {

	public Map<String, String> mapByVehicle(String vehicleId) {
		Map<String, String> valueMap = new HashMap<>();
		String hql = "from VehicleExt where  vehicleId=? and tenantId=? ";
		List<VehicleExt> list = listByHql(hql, vehicleId, LoginUserUtil.getLoginUser().getTenantId());
		for (VehicleExt vehicleExt : list) {
			valueMap.put(vehicleExt.getArcExtId(), vehicleExt.getValue());
		}
		return valueMap;
	}

}
