package modules.arc.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.OwnerExt;
import modules.arc.service.OwnerExtService;

/**
 * 
 * @author zzc
 *
 */
@Service("ownerExtService")
@Transactional
public class OwnerExtServiceImpl extends CommonServiceImpl implements OwnerExtService {

	public Map<String, String> mapByOwner(String ownerId) {
		Map<String, String> valueMap = new HashMap<>();
		String hql = "from OwnerExt where ownerId=? and tenantId=?";
		List<OwnerExt> list = listByHql(hql, ownerId, LoginUserUtil.getLoginUser().getTenantId());
		for (OwnerExt ownerExt : list) {
			valueMap.put(ownerExt.getArcExtId(), ownerExt.getValue());
		}
		return valueMap;
	}

}
