package modules.arc.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.ArcExt;
import modules.arc.service.ArcExtService;

/**
 * 
 * @author zzc
 *
 */
@Service("arcExtService")
@Transactional
public class ArcExtServiceImpl extends CommonServiceImpl implements ArcExtService {

	public List<ArcExt> listByType(String type) {
		String hql = "from ArcExt where type=? and tenantId=?";
		return listByHql(hql, type, LoginUserUtil.getLoginUser().getTenantId());
	}

	public void delete(ArcExt arcExt) {
		if (arcExt.getType().equals(ArcExt.TYPE_VEHICLE)) {
			// 删除车辆扩展信息
			String hql = "delete from VehicleExt where arcExtId=?";
			executeHql(hql, arcExt.getId());
		} else if (arcExt.getType().equals(ArcExt.TYPE_OWNER)) {
			// 删除责任人扩展信息
			String hql = "delete from OwnerExt where arcExtId=?";
			executeHql(hql, arcExt.getId());

		} else if (arcExt.getType().equals(ArcExt.TYPE_DRIVER)) {
			// 删除司机扩展信息
			String hql = "delete from DriverExt where arcExtId=?";
			executeHql(hql, arcExt.getId());
		}
		// 删除扩展档案
		super.delete(arcExt);
	}

}
