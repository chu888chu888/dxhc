package modules.arc.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.ArcExt;
import modules.arc.entity.Driver;
import modules.arc.entity.DriverExt;
import modules.arc.entity.Owner;
import modules.arc.entity.OwnerExt;
import modules.arc.service.OwnerService;

/**
 * 
 * @author zzc
 *
 */
@Service("ownerService")
@Transactional
public class OwnerServiceImpl extends CommonServiceImpl implements OwnerService {

	public void save(Owner owner, List<ArcExt> ownerArcExtList, Driver driver, List<ArcExt> driverArcExtList,
			HttpServletRequest request) {
		// 保存责任人
		save(owner);
		// 保存责任人扩展信息
		for (ArcExt arcExt : ownerArcExtList) {
			String value = request.getParameter(arcExt.getId());
			if (StringUtils.isNotBlank(value)) {
				OwnerExt ownerExt = new OwnerExt();
				ownerExt.setOwnerId(owner.getId());
				ownerExt.setArcExtId(arcExt.getId());
				ownerExt.setValue(value);
				save(ownerExt);
			}
		}
		// 保存司机
		save(driver);
		// 保存司机扩展信息
		for (ArcExt arcExt : driverArcExtList) {
			String value = request.getParameter(arcExt.getId());
			if (StringUtils.isNotBlank(value)) {
				DriverExt driverExt = new DriverExt();
				driverExt.setDriverId(driver.getId());
				driverExt.setArcExtId(arcExt.getId());
				driverExt.setValue(value);
				save(driverExt);
			}
		}
	}

	public void save(Owner owner, List<ArcExt> ownerArcExtList, HttpServletRequest request) {
		// 保存责任人
		save(owner);
		// 保存责任人扩展信息
		for (ArcExt arcExt : ownerArcExtList) {
			String value = request.getParameter(arcExt.getId());
			if (StringUtils.isNotBlank(value)) {
				OwnerExt ownerExt = new OwnerExt();
				ownerExt.setOwnerId(owner.getId());
				ownerExt.setArcExtId(arcExt.getId());
				ownerExt.setValue(value);
				save(ownerExt);
			}
		}
	}

	public void update(Owner owner, List<OwnerExt> ownerExtList) {
		// 责任人
		saveOrUpdate(owner);
		// 扩展属性,先删除再重新增加
		String hql = "delete from OwnerExt where ownerId=?";
		executeHql(hql, owner.getId());
		for (OwnerExt ownerExt : ownerExtList) {
			save(ownerExt);
		}
	}

	public void delete(Owner owner) {
		// 删除扩展属性
		String hql = "delete from OwnerExt where ownerId=?";
		executeHql(hql, owner.getId());
		// 删除图片
		super.delete(owner);
	}

	public Owner getByName(String name) {
		String hql = "from Owner where name=? and tenantId=?";
		List<Owner> list = listByHql(hql, name, LoginUserUtil.getLoginUser().getTenantId());
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	public Owner getByPhone(String phone) {
		String hql = "from Owner where phone=? and tenantId=?";
		List<Owner> list = listByHql(hql, phone, LoginUserUtil.getLoginUser().getTenantId());
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	public Owner getByIdCard(String idCard) {
		String hql = "from Owner where idCard=? and tenantId=?";
		List<Owner> list = listByHql(hql, idCard, LoginUserUtil.getLoginUser().getTenantId());
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

}
