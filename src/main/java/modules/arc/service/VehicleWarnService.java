package modules.arc.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import luxing.common.service.CommonService;
import modules.arc.entity.VehicleWarn;
import modules.warn.entity.VehicleWarnHis;

public interface VehicleWarnService extends CommonService {

	Map<String, Date> mapByVehicle(String vehicleId);

	List<VehicleWarn> listByWarnTypeAndStatus(String warnType, String status);

	void save(List<VehicleWarn> list);

	void handle(VehicleWarn vehicleWarn, VehicleWarnHis vehicleWarnHis);

}
