package modules.arc.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;

import modules.warn.entity.WarnType;

/**
 * 车辆提醒
 * 
 * @author zzc
 */
@Entity
@Table(name = "arc_vehicle_warn")
@DynamicInsert
@DynamicUpdate
public class VehicleWarn implements Serializable {

	private static final long serialVersionUID = 8233672706684160587L;
	public static final String STATUS_ALL = "-1";// 全部
	public static final String STATUS_NORMAL = "0";// 正常
	public static final String STATUS_PAUSE = "1";// 暂停

	private String id;
	/** 租户 */
	private String tenantId;
	/** 车辆 */
	@ExcelEntity(name = "车辆")
	private Vehicle vehicle;
	/** 提醒 */
	@ExcelEntity(name = "提醒类型")
	private WarnType warnType;
	/** 提醒日期 */
	@Excel(name = "到期日期", format = "yyyy-MM-dd")
	private Date warnDate;
	/** 状态 */
	@Excel(name = "提醒状态", replace = { "正常_0", "暂停_1" })
	private String status;
	/** 下次到期 */
	private Date nextDate;

	private List<VehicleWarn> warnList;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "warn_type_id")
	public WarnType getWarnType() {
		return warnType;
	}

	public void setWarnType(WarnType warnType) {
		this.warnType = warnType;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "warn_date", nullable = false)
	public Date getWarnDate() {
		return warnDate;
	}

	public void setWarnDate(Date warnDate) {
		this.warnDate = warnDate;
	}

	@Transient
	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	@Transient
	public List<VehicleWarn> getWarnList() {
		return warnList;
	}

	public void setWarnList(List<VehicleWarn> warnList) {
		this.warnList = warnList;
	}

}