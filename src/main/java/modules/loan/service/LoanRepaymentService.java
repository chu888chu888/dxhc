package modules.loan.service;

import luxing.common.service.CommonService;
import modules.loan.entity.LoanRepayment;

public interface LoanRepaymentService extends CommonService {

	void doVoid(LoanRepayment loanRepayment);

}
