package modules.loan.service.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import modules.loan.entity.LoanContract;
import modules.loan.entity.Loan;
import modules.loan.entity.LoanRepayment;
import modules.loan.service.LoanService;

/**
 * 
 * @author zzc
 *
 */
@Service("loanService")
@Transactional
public class LoanServiceImpl extends CommonServiceImpl implements LoanService {

	public void update(Loan loan) {
		// 修改贷款
		super.saveOrUpdate(loan);
		// 贷款合同
		LoanContract loanContract = get(LoanContract.class, loan.getLoanContract().getId());
		BigDecimal interest = new BigDecimal(0);
		BigDecimal totalAmount = new BigDecimal(0);
		BigDecimal oweAmount = new BigDecimal(0);
		String status = "";
		for (Loan entity : loanContract.getLoanList()) {
			interest = interest.add(entity.getInterest());
			totalAmount = totalAmount.add(entity.getTotalAmount());
			oweAmount = oweAmount.add(entity.getOweAmount());
			status += entity.getStatus();
		}
		loanContract.setInterest(interest);
		loanContract.setTotalAmount(totalAmount);
		loanContract.setOweAmount(oweAmount);
		if (!status.contains("0") && !status.contains("1")) {
			loanContract.setStatus(LoanContract.STATUS_YES);
		} else if (status.contains("1")) {
			loanContract.setStatus(LoanContract.STATUS_ING);
		} else if (!status.contains("1") && !status.contains("2")) {
			loanContract.setStatus(LoanContract.STATUS_NO);
		}
		super.saveOrUpdate(loanContract);
	}

	public void repayment(Loan loan, LoanRepayment loanRepayment) {
		// 保存贷款
		super.saveOrUpdate(loan);
		// 贷款合同
		LoanContract loanContract = get(LoanContract.class, loan.getLoanContract().getId());
		BigDecimal realPrincipal = new BigDecimal(0);
		BigDecimal realInterest = new BigDecimal(0);
		BigDecimal realTotalAmount = new BigDecimal(0);
		BigDecimal oweAmount = new BigDecimal(0);
		String status = "";
		for (Loan entity : loanContract.getLoanList()) {
			realPrincipal = realPrincipal.add(entity.getRealPrincipal());
			realInterest = realInterest.add(entity.getRealInterest());
			realTotalAmount = realTotalAmount.add(entity.getRealTotalAmount());
			oweAmount = oweAmount.add(entity.getOweAmount());
			status += entity.getStatus();
		}
		loanContract.setRealPrincipal(realPrincipal);
		loanContract.setRealInterest(realInterest);
		loanContract.setRealTotalAmount(realTotalAmount);
		loanContract.setOweAmount(oweAmount);
		if (!status.contains(LoanContract.STATUS_NO) && !status.contains(LoanContract.STATUS_ING)) {
			loanContract.setStatus(LoanContract.STATUS_YES);
		} else if (status.contains(LoanContract.STATUS_ING)
				|| (status.contains(LoanContract.STATUS_NO) && status.contains(LoanContract.STATUS_YES))) {
			loanContract.setStatus(LoanContract.STATUS_ING);
		} else if (!status.contains(LoanContract.STATUS_ING) && !status.contains(LoanContract.STATUS_YES)) {
			loanContract.setStatus(LoanContract.STATUS_NO);
		}
		super.saveOrUpdate(loanContract);
		// 还款记录
		loanRepayment.setLoan(loan);
		super.save(loanRepayment);
	}

}
