package modules.warn.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.arc.entity.VehicleWarn;
import modules.warn.entity.WarnType;
import modules.warn.service.WarnTypeService;

/**
 * 
 * @author zzc
 *
 */
@Service("warnTypeService")
@Transactional
public class WarnTypeServiceImpl extends CommonServiceImpl implements WarnTypeService {

	public List<WarnType> listByStatus(String status) {
		String hql = "from WarnType where status=? and tenantId=?";
		return listByHql(hql, status, LoginUserUtil.getLoginUser().getTenantId());
	}

	public void delete(WarnType warnType) {
		// 删除车辆提醒
		String hql = "delete from VehicleWarn where warnType.id=? and status=?";
		executeHql(hql, warnType.getId(), VehicleWarn.STATUS_PAUSE);
		super.delete(warnType);
	}
}
