package modules.warn.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.warn.entity.WarnType;

public interface WarnTypeService extends CommonService {

	List<WarnType> listByStatus(String status);

	void delete(WarnType warnType);
}
