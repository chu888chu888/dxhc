package modules.warn.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import modules.arc.entity.Vehicle;

/**
 * 车辆提醒
 * 
 * @author zzc
 */
@Entity
@Table(name = "warn_vehicle_his")
@DynamicInsert
@DynamicUpdate
public class VehicleWarnHis implements Serializable {

	private static final long serialVersionUID = -8803087310126387837L;

	private String id;
	/** 租户 */
	private String tenantId;
	/** 车辆 */
	private Vehicle vehicle;
	/** 提醒 */
	private WarnType warnType;
	/** 办理前日期 */
	private Date preWarnDate;
	/** 办理后日期 */
	private Date afterWarnDate;
	/** 创建时间 */
	private Date createDate;
	/** 创建人ID */
	private String createBy;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "warn_type_id")
	public WarnType getWarnType() {
		return warnType;
	}

	public void setWarnType(WarnType warnType) {
		this.warnType = warnType;
	}

	@Column(name = "pre_warn_date", nullable = false)
	public Date getPreWarnDate() {
		return preWarnDate;
	}

	public void setPreWarnDate(Date preWarnDate) {
		this.preWarnDate = preWarnDate;
	}

	@Column(name = "after_warn_date", nullable = false)
	public Date getAfterWarnDate() {
		return afterWarnDate;
	}

	public void setAfterWarnDate(Date afterWarnDate) {
		this.afterWarnDate = afterWarnDate;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}