package modules.warn.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.common.service.SmsService;
import luxing.constant.SmsConfig;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.DateUtil;
import luxing.util.JsonUtil;
import luxing.util.LoginUserUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import manage.tenant.entity.Tenant;
import manage.tenant.service.TenantService;
import modules.fin.entity.Expenses;
import modules.fin.entity.Income;
import modules.fin.service.ExpensesService;
import modules.fin.service.IncomeService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/incomeWarnController")
public class IncomeWarnController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IncomeWarnController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private IncomeService incomeService;
	@Autowired
	private ExpensesService expensesService;
	@Autowired
	private TenantService tenantService;
	@Autowired
	private SmsService smsService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		List<Expenses> expensesList = expensesService.listByTypeAndStatus(Expenses.TYPE_INCOME,
				Expenses.STATUS_ENABLE);
		request.setAttribute("expensesList", expensesList);
		return new ModelAndView("modules/warn/incomeWarn-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param income
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Income income, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Income.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, income, request.getParameterMap());
			// 费用类型
			String exs = request.getParameter("exs");
			if (StringUtils.isNotBlank(exs)) {
				cq.eq("expenses.id", exs);
			}
			// 提醒类型
			cq.isNotNull("endDate");
			String warnDateType = request.getParameter("warnDateType");
			String endDateBegin = request.getParameter("endDate_begin");
			String endDateEnd = request.getParameter("endDate_end");
			if (StringUtils.isNotBlank(endDateBegin) || StringUtils.isNotBlank(endDateEnd)
					|| (warnDateType != null && warnDateType.equals("2"))) {
				// 按时间范围
				if (StringUtils.isNotBlank(endDateBegin)) {
					cq.ge("endDate", DateUtil.strToDate(endDateBegin, "yyyy-MM-dd"));
				}
				if (StringUtils.isNotBlank(endDateEnd)) {
					cq.le("endDate", DateUtil.strToDate(endDateEnd, "yyyy-MM-dd"));
				}
			} else if ((warnDateType == null || warnDateType.equals("1"))
					&& (StringUtils.isBlank(endDateBegin) && StringUtils.isBlank(endDateEnd))) {
				// 下月到期
				Calendar calBegin = Calendar.getInstance();
				calBegin.add(Calendar.YEAR, -100);
				cq.ge("endDate", calBegin.getTime());
				Calendar calEnd = Calendar.getInstance();
				calEnd.add(Calendar.MONTH, 2);
				calEnd.set(Calendar.DATE, 0);
				cq.le("endDate", calEnd.getTime());
			}
			cq.add();
			incomeService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("财务到期查询失败", e);
			throw new BusinessException("财务到期查询失败", e);
		}
	}

	/**
	 * 发送短信页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goSendSms")
	public ModelAndView goSendSms(Income income, HttpServletRequest req) {
		if (StringUtils.isNotBlank(income.getId())) {
			income = incomeService.get(Income.class, income.getId());
			req.setAttribute("incomePage", income);
		}
		return new ModelAndView("modules/warn/incomeWarn-sms");
	}

	/**
	 * 发送短信
	 * 
	 * @param income
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doSendSms")
	@ResponseBody
	public Result doSendSms(Income income, HttpServletRequest request) {
		String msg = null;
		String phone = request.getParameter("ownerPhone");
		try {
			Tenant tenant = tenantService.get(Tenant.class, LoginUserUtil.getLoginUser().getTenantId());
			// 判断用户余额
			if (tenant.getBalance().compareTo(Tenant.BALANCE_MIN) < 0) {
				msg = "账户余额不足,发送失败.";
				return Result.error(msg);
			}
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("plateNumber", income.getVehicle().getPlateNumber());
			paramMap.put("warnTypeName", income.getExpenses().getName());
			paramMap.put("warnDate", DateUtil.dateToStr(income.getEndDate(), "yyyy-MM-dd"));
			String templateParam = JsonUtil.toJson(paramMap);
			smsService.sendSms(phone, SmsConfig.template_vehicle_warn, templateParam, tenant);
			msg = "号码\"" + phone + "\"短信发送成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_SMS);
		} catch (Exception e) {
			msg = "号码\"" + phone + "\"短信发送失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
