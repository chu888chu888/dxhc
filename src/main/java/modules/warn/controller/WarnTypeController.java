package modules.warn.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.ValidatorUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.arc.entity.VehicleWarn;
import modules.arc.service.VehicleWarnService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;
import modules.warn.entity.WarnType;
import modules.warn.service.WarnTypeService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/warnTypeController")
public class WarnTypeController extends BaseController {

	private static final Logger logger = Logger.getLogger(WarnTypeController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private WarnTypeService warnTypeService;
	@Autowired
	private VehicleWarnService vehicleWarnService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/warn/warnType-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param warnType
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(WarnType warnType, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(WarnType.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, warnType, request.getParameterMap());
			cq.add();
			warnTypeService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("提醒类型查询失败", e);
			throw new BusinessException("提醒类型查询失败", e);
		}
	}

	/**
	 * 增加页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(HttpServletRequest req) {
		return new ModelAndView("modules/warn/warnType-add");
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(WarnType warnType, HttpServletRequest req) {
		if (StringUtils.isNotBlank(warnType.getId())) {
			warnType = warnTypeService.get(WarnType.class, warnType.getId());
			req.setAttribute("warnTypePage", warnType);
		}
		return new ModelAndView("modules/warn/warnType-update");
	}

	/**
	 * 增加
	 * 
	 * @param warnType
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public Result doAdd(WarnType warnType, HttpServletRequest request) {
		String msg = null;
		try {
			warnType.setStatus(WarnType.STATUS_ENABLE);
			// bean校验
			msg = ValidatorUtil.validate(warnType);
			if (StringUtils.isNotBlank(msg)) {
				return Result.error(msg);
			}
			warnTypeService.save(warnType);
			msg = "提醒\"" + warnType.getName() + "\"增加成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_ADD);
		} catch (Exception e) {
			msg = "提醒\"" + warnType.getName() + "\"增加失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 修改
	 * 
	 * @param warnType
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public Result doUpdate(WarnType warnType, HttpServletRequest request) {
		String msg = null;
		try {
			warnTypeService.saveOrUpdate(warnType);
			msg = "提醒\"" + warnType.getName() + "\"修改成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "提醒\"" + warnType.getName() + "\"修改失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public Result doDel(WarnType warnType, HttpServletRequest request) {
		String msg = null;
		try {
			warnType = warnTypeService.get(WarnType.class, warnType.getId());
			// 车辆提醒
			List<VehicleWarn> list = vehicleWarnService.listByWarnTypeAndStatus(warnType.getId(),
					VehicleWarn.STATUS_NORMAL);
			if (CollectionUtils.isNotEmpty(list)) {
				msg = "提醒\"" + warnType.getName() + "\"已经使用,无法删除";
				return Result.error(msg);
			}
			warnTypeService.delete(warnType);
			msg = "提醒\"" + warnType.getName() + "\"删除成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_DEL);
		} catch (Exception e) {
			msg = "提醒\"" + warnType.getName() + "\"删除失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
