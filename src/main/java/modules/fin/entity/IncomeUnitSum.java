package modules.fin.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 收单位款记录
 * 
 * @author zzc
 */
@Entity
@Table(name = "fin_income_unit_sum")
@DynamicInsert
@DynamicUpdate
public class IncomeUnitSum implements Serializable {

	private static final long serialVersionUID = 5063067375269798835L;

	public static final String WAY_CASH = "1"; // 现金
	public static final String WAY_BANK = "2"; // 银行转账
	public static final String WAY_WEIXIN = "3"; // 微信
	public static final String WAY_ALIPAY = "4"; // 支付宝
	public static final String WAY_POS = "5"; // pos

	public static final String STATUS_ENABLE = "0";// 正常
	public static final String STATUS_DISABLE = "1";// 作废

	private String id;
	/** 租户 */
	private String tenantId;
	/** 编号 SDWK20171202001 */
	private String sn;
	/** 来往单位 */
	private String unit;
	/** 费用 */
	private Expenses expenses;
	/** 收款金额 */
	private BigDecimal amount;
	/** 收款方式 */
	private String way;
	/** 状态 */
	private String status;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 作废时间 */
	private Date voidDate;
	/** 作废人 */
	private String voidBy;
	/** 收款集合 */
	private List<IncomeUnit> incomeUnitList = new ArrayList<IncomeUnit>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Column(name = "sn", nullable = false, length = 50)
	@NotBlank(message = "编号不能为空")
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "unit", nullable = false, length = 32)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expenses_id")
	public Expenses getExpenses() {
		return expenses;
	}

	public void setExpenses(Expenses expenses) {
		this.expenses = expenses;
	}

	@Column(name = "amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "way", nullable = false, length = 10)
	@NotBlank(message = "收款方式不能为空")
	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "incomeUnitSum")
	@Fetch(FetchMode.SUBSELECT)
	public List<IncomeUnit> getIncomeUnitList() {
		return incomeUnitList;
	}

	public void setIncomeUnitList(List<IncomeUnit> incomeUnitList) {
		this.incomeUnitList = incomeUnitList;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "void_date")
	public Date getVoidDate() {
		return voidDate;
	}

	public void setVoidDate(Date voidDate) {
		this.voidDate = voidDate;
	}

	@Column(name = "void_by", length = 50)
	public String getVoidBy() {
		return voidBy;
	}

	public void setVoidBy(String voidBy) {
		this.voidBy = voidBy;
	}

}