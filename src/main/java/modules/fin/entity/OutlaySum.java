package modules.fin.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import modules.arc.entity.Owner;

/**
 * 付款记录
 * 
 * @author zzc
 */
@Entity
@Table(name = "fin_outlay_sum")
@DynamicInsert
@DynamicUpdate
public class OutlaySum implements Serializable {

	private static final long serialVersionUID = 1721409086029352410L;

	public static final String WAY_CASH = "1"; // 现金
	public static final String WAY_WEIXIN = "2"; // 微信
	public static final String WAY_ALIPAY = "3"; // 支付宝
	public static final String WAY_BANK = "4"; // 银行转账
	public static final String WAY_DX = "5"; // 大象支付

	public static final String STATUS_ENABLE = "0";// 正常
	public static final String STATUS_DISABLE = "1";// 作废

	private String id;
	/** 租户 */
	private String tenantId;
	/** 责任人 */
	private Owner owner;
	/** 编号 FK20171202001 */
	private String sn;
	/** 付款金额 */
	private BigDecimal amount;
	/** 付款方式 */
	private String way;
	/** 状态 */
	private String status;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 修改时间 */
	private Date updateDate;
	/** 修改人 */
	private String updateBy;
	/** 付款集合 */
	private List<Outlay> outlayList = new ArrayList<Outlay>();

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id")
	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	@Column(name = "sn", nullable = false, length = 50)
	@NotBlank(message = "编号不能为空")
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "way", nullable = false, length = 10)
	@NotBlank(message = "付款方式不能为空")
	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "outlaySum")
	@Fetch(FetchMode.SUBSELECT)
	public List<Outlay> getOutlayList() {
		return outlayList;
	}

	public void setOutlayList(List<Outlay> outlayList) {
		this.outlayList = outlayList;
	}

	@Column(name = "create_date", nullable = true)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", nullable = true, length = 32)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "update_date")
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name = "update_by", length = 50)
	public String getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}