package modules.fin.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import modules.arc.entity.Vehicle;

/**
 * 收车辆款
 * 
 * @author zzc
 */
@Entity
@Table(name = "fin_income")
@DynamicInsert
@DynamicUpdate
public class Income implements Serializable {

	private static final long serialVersionUID = 2551349484232782872L;
	public static final String STATUS_NO = "0";// 未核销
	public static final String STATUS_YES = "1";// 已核销

	private String id;
	/** 租户 */
	private String tenantId;
	/** 收款记录 */
	private IncomeSum incomeSum;
	/** 车辆 */
	private Vehicle vehicle;
	/** 费用 */
	private Expenses expenses;
	/** 应收金额 */
	private BigDecimal incomeAmount;
	/** 优惠金额 */
	private BigDecimal discountAmount;
	/** 核销金额 */
	private BigDecimal settleAmount;
	/** 到期日期 */
	private Date endDate;
	/** 状态 */
	private String status;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 核销时间 */
	private Date settleDate;
	/** 核销人 */
	private String settleBy;

	private List<Income> incomeList;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "incomesum_id")
	public IncomeSum getIncomeSum() {
		return incomeSum;
	}

	public void setIncomeSum(IncomeSum incomeSum) {
		this.incomeSum = incomeSum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expenses_id")
	public Expenses getExpenses() {
		return expenses;
	}

	public void setExpenses(Expenses expenses) {
		this.expenses = expenses;
	}

	@Column(name = "income_amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getIncomeAmount() {
		return incomeAmount;
	}

	public void setIncomeAmount(BigDecimal incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	@Column(name = "discount_amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	@Column(name = "settle_amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getSettleAmount() {
		return settleAmount;
	}

	public void setSettleAmount(BigDecimal settleAmount) {
		this.settleAmount = settleAmount;
	}

	@Column(name = "end_date")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", length = 50)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "settle_date")
	public Date getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(Date settleDate) {
		this.settleDate = settleDate;
	}

	@Column(name = "settle_by", length = 50)
	public String getSettleBy() {
		return settleBy;
	}

	public void setSettleBy(String settleBy) {
		this.settleBy = settleBy;
	}

	@Transient
	public List<Income> getIncomeList() {
		return incomeList;
	}

	public void setIncomeList(List<Income> incomeList) {
		this.incomeList = incomeList;
	}

}