package modules.fin.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import modules.arc.entity.Vehicle;

/**
 * 收单位款
 * 
 * @author zzc
 */
@Entity
@Table(name = "fin_income_unit")
@DynamicInsert
@DynamicUpdate
public class IncomeUnit implements Serializable {

	private static final long serialVersionUID = 1070983655190529439L;

	public static final String STATUS_NO = "0";// 未核销
	public static final String STATUS_YES = "1";// 已核销

	private String id;
	/** 租户 */
	private String tenantId;
	/** 收款记录 */
	private IncomeUnitSum incomeUnitSum;
	/** 来往单位 */
	private String unit;
	/** 车辆 */
	private Vehicle vehicle;
	/** 费用 */
	private Expenses expenses;
	/** 金额 */
	private BigDecimal amount;
	/** 状态 */
	private String status;
	/** 备注 */
	private String remarks;
	/** 创建人ID */
	private String createBy;
	/** 创建时间 */
	private Date createDate;
	/** 核销时间 */
	private Date settleDate;
	/** 核销人 */
	private String settleBy;

	private List<IncomeUnit> incomeUnitList;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", nullable = false, length = 32)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "tenant_id", nullable = false, length = 32)
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "incomeUnitSum_id")
	public IncomeUnitSum getIncomeUnitSum() {
		return incomeUnitSum;
	}

	public void setIncomeUnitSum(IncomeUnitSum incomeUnitSum) {
		this.incomeUnitSum = incomeUnitSum;
	}

	@Column(name = "unit", nullable = false, length = 32)
	@NotBlank(message = "单位不能为空")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_id")
	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expenses_id")
	public Expenses getExpenses() {
		return expenses;
	}

	public void setExpenses(Expenses expenses) {
		this.expenses = expenses;
	}

	@Column(name = "amount", nullable = false, scale = 1, length = 10)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "status", nullable = false, length = 10)
	@NotBlank(message = "状态不能为空")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "remarks", length = 255)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "create_by", length = 50)
	public String getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Column(name = "settle_date")
	public Date getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(Date settleDate) {
		this.settleDate = settleDate;
	}

	@Column(name = "settle_by", length = 50)
	public String getSettleBy() {
		return settleBy;
	}

	public void setSettleBy(String settleBy) {
		this.settleBy = settleBy;
	}

	@Transient
	public List<IncomeUnit> getIncomeUnitList() {
		return incomeUnitList;
	}

	public void setIncomeUnitList(List<IncomeUnit> incomeUnitList) {
		this.incomeUnitList = incomeUnitList;
	}

}