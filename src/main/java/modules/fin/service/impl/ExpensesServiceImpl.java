package modules.fin.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.LoginUserUtil;
import modules.fin.entity.Expenses;
import modules.fin.service.ExpensesService;

/**
 * 
 * @author zzc
 *
 */
@Service("expensesService")
@Transactional
public class ExpensesServiceImpl extends CommonServiceImpl implements ExpensesService {

	public List<Expenses> listByTypeAndStatus(String type, String status) {
		String hql = "from Expenses where type=? and status=? and tenantId=? order by sort asc";
		return listByHql(hql, type, status, LoginUserUtil.getLoginUser().getTenantId());
	}

}
