package modules.fin.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.DateUtil;
import luxing.util.LoginUserUtil;
import modules.fin.entity.Outlay;
import modules.fin.entity.OutlaySum;
import modules.fin.service.OutlayService;

/**
 * 
 * @author zzc
 *
 */
@Service("outlayService")
@Transactional
public class OutlayServiceImpl extends CommonServiceImpl implements OutlayService {

	public void settle(OutlaySum outlaySum) {
		save(outlaySum);
		for (Outlay outlay : outlaySum.getOutlayList()) {
			// 核销时间 核销人
			outlay.setSettleBy(LoginUserUtil.getUser().getName());
			outlay.setSettleDate(new Date());
			outlay.setOutlaySum(outlaySum);
			saveOrUpdate(outlay);
		}
	}

	public void doVoid(OutlaySum outlaySum) {
		for (Outlay outlay : outlaySum.getOutlayList()) {
			outlay.setOutlaySum(null);
			outlay.setSettleBy(null);
			outlay.setSettleDate(null);
			outlay.setSettleAmount(new BigDecimal(0));
			outlay.setStatus(Outlay.STATUS_NO);
			super.saveOrUpdate(outlay);
		}
		super.saveOrUpdate(outlaySum);
	}

	public List<Outlay> listByVehicleAndStatus(String vehicleId, String status) {
		String hql = "from Outlay where vehicle.id=? and status=? and tenantId=?";
		return listByHql(hql, vehicleId, status, LoginUserUtil.getLoginUser().getTenantId());
	}

	public List<Outlay> listByVehicle(String vehicleId) {
		String hql = "from Outlay where vehicle.id=?";
		return listByHql(hql, vehicleId);
	}

	public List<Outlay> listByOwnerAndStatus(String ownerId, String status) {
		String hql = "from Outlay where vehicle.owner.id=? and status=? and tenantId=?";
		return listByHql(hql, ownerId, status, LoginUserUtil.getLoginUser().getTenantId());
	}

	public List<OutlaySum> listByOwner(String ownerId) {
		String hql = "from OutlaySum where owner.id=?";
		return listByHql(hql, ownerId);
	}

	public List<Outlay> listByExpenses(String expensesId) {
		String hql = "from Outlay where expenses.id=? and tenantId=?";
		return listByHql(hql, expensesId, LoginUserUtil.getLoginUser().getTenantId());
	}

	public String getSn() {
		String hql = "from OutlaySum where date(createDate)=? and tenantId=? order by sn desc";
		List<OutlaySum> outlaySumList = listByHql(hql, new Date(), LoginUserUtil.getLoginUser().getTenantId());
		String sn = "FK" + DateUtil.dateToStr(new Date(), "yyyyMMdd") + "001";
		if (CollectionUtils.isNotEmpty(outlaySumList)) {
			String endNum = outlaySumList.get(0).getSn().substring(10, 13);
			if (StringUtils.isNumeric(endNum)) {
				endNum = String.valueOf(Integer.valueOf(endNum).intValue() + 1);
				if (endNum.length() == 1) {
					endNum = "00" + endNum;
				} else if (endNum.length() == 2) {
					endNum = "0" + endNum;
				}
				sn = "FK" + DateUtil.dateToStr(new Date(), "yyyyMMdd") + endNum;
				return sn;
			}
		}
		return sn;
	}
}
