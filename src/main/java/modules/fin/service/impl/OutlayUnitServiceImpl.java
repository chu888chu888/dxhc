package modules.fin.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import luxing.common.service.impl.CommonServiceImpl;
import luxing.util.DateUtil;
import luxing.util.LoginUserUtil;
import modules.fin.entity.OutlayUnit;
import modules.fin.entity.OutlayUnitSum;
import modules.fin.service.OutlayUnitService;

/**
 * 
 * @author zzc
 *
 */
@Service("outlayUnitService")
@Transactional
public class OutlayUnitServiceImpl extends CommonServiceImpl implements OutlayUnitService {

	public void settle(OutlayUnitSum outlayUnitSum) {
		save(outlayUnitSum);
		for (OutlayUnit outlayUnit : outlayUnitSum.getOutlayUnitList()) {
			outlayUnit.setOutlayUnitSum(outlayUnitSum);
			saveOrUpdate(outlayUnit);
		}
	}

	public void doVoid(OutlayUnitSum outlayUnitSum) {
		for (OutlayUnit outlayUnit : outlayUnitSum.getOutlayUnitList()) {
			outlayUnit.setOutlayUnitSum(null);
			outlayUnit.setStatus(OutlayUnit.STATUS_NO);
			outlayUnit.setSettleBy(null);
			outlayUnit.setSettleDate(null);
			super.saveOrUpdate(outlayUnit);
		}
		super.saveOrUpdate(outlayUnitSum);
	}

	public List<OutlayUnit> listByUnitAndExpensesAndStatus(String unit, String expensesId, String status) {
		String hql = "from OutlayUnit where unit=? and expenses.id=? and status=? and tenantId=?";
		return listByHql(hql, unit, expensesId, status, LoginUserUtil.getLoginUser().getTenantId());
	}

	public List<OutlayUnit> listByExpenses(String expensesId) {
		String hql = "from OutlayUnit where expenses.id=?  and tenantId=?";
		return listByHql(hql, expensesId, LoginUserUtil.getLoginUser().getTenantId());
	}

	public String getSn() {
		String hql = "from OutlayUnitSum where date(createDate)=? and tenantId=? order by sn desc";
		List<OutlayUnitSum> outlayUnitSumList = listByHql(hql, new Date(), LoginUserUtil.getLoginUser().getTenantId());
		String sn = "FDWK" + DateUtil.dateToStr(new Date(), "yyyyMMdd") + "001";
		if (CollectionUtils.isNotEmpty(outlayUnitSumList)) {
			String endNum = outlayUnitSumList.get(0).getSn().substring(12, 15);
			if (StringUtils.isNumeric(endNum)) {
				endNum = String.valueOf(Integer.valueOf(endNum).intValue() + 1);
				if (endNum.length() == 1) {
					endNum = "00" + endNum;
				} else if (endNum.length() == 2) {
					endNum = "0" + endNum;
				}
				sn = "FDWK" + DateUtil.dateToStr(new Date(), "yyyyMMdd") + endNum;
				return sn;
			}
		}
		return sn;
	}

}
