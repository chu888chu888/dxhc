package modules.fin.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.fin.entity.Income;
import modules.fin.entity.IncomeSum;

public interface IncomeService extends CommonService {

	void settle(IncomeSum incomeSum);

	void doVoid(IncomeSum incomeSum);

	List<Income> listByVehicleAndStatus(String vehicleId, String status);

	List<Income> listByOwnerAndStatus(String ownerId, String status);

	List<IncomeSum> listByOwner(String ownerId);

	List<Income> listByExpenses(String expensesId);

	String getSn();

}
