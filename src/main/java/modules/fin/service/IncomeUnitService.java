package modules.fin.service;

import java.util.List;

import luxing.common.service.CommonService;
import modules.fin.entity.IncomeUnit;
import modules.fin.entity.IncomeUnitSum;

public interface IncomeUnitService extends CommonService {

	void settle(IncomeUnitSum incomeUnitSum);

	void doVoid(IncomeUnitSum incomeUnitSum);

	List<IncomeUnit> listByUnitAndExpensesAndStatus(String unit, String expensesId, String status);

	List<IncomeUnit> listByExpenses(String expensesId);

	String getSn();

}
