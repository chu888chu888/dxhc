package modules.fin.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.fin.entity.Outlay;
import modules.fin.entity.OutlaySum;
import modules.fin.service.OutlayService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/outlaySumController")
public class OutlaySumController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OutlaySumController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private OutlayService outlayService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/fin/outlaySum-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param outlaySum
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(OutlaySum outlaySum, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(OutlaySum.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, outlaySum, request.getParameterMap());
			cq.add();
			outlayService.listByPage(cq, true);
			dataGrid.setFooter("amount,sn:合计");
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("付款记录查询失败", e);
			throw new BusinessException("付款记录查询失败", e);
		}
	}

	/**
	 * 详情页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goDetail")
	public ModelAndView goDetail(OutlaySum outlaySum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(outlaySum.getId())) {
			outlaySum = outlayService.get(OutlaySum.class, outlaySum.getId());
			req.setAttribute("outlaySumPage", outlaySum);
		}
		return new ModelAndView("modules/fin/outlaySum-detail");
	}

	/**
	 * 打印页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goPrint")
	public ModelAndView goPrint(OutlaySum outlaySum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(outlaySum.getId())) {
			outlaySum = outlayService.get(OutlaySum.class, outlaySum.getId());
			req.setAttribute("outlaySumPage", outlaySum);
		}
		return new ModelAndView("modules/fin/outlaySum-print");
	}

	/**
	 * 核销明细
	 * 
	 * @return
	 */
	@RequestMapping(params = "settleTab")
	public ModelAndView settleTab(HttpServletRequest req) {
		String id = req.getParameter("id");
		OutlaySum outlaySum = outlayService.get(OutlaySum.class, id);
		BigDecimal totalOutlayAmount = new BigDecimal(0);
		BigDecimal totalSettleAmount = new BigDecimal(0);
		for (Outlay outlay : outlaySum.getOutlayList()) {
			totalOutlayAmount = totalOutlayAmount.add(outlay.getOutlayAmount());
			totalSettleAmount = totalSettleAmount.add(outlay.getSettleAmount());
		}
		req.setAttribute("totalOutlayAmount", totalOutlayAmount);
		req.setAttribute("totalSettleAmount", totalSettleAmount);
		req.setAttribute("outlayList", outlaySum.getOutlayList());
		return new ModelAndView("modules/fin/outlaySumDetail-tab");
	}

	/**
	 * 作废
	 * 
	 * @param outlaySum
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doVoid")
	@ResponseBody
	public Result doVoid(OutlaySum outlaySum, HttpServletRequest request) {
		String msg = null;
		try {
			outlaySum = outlayService.get(OutlaySum.class, outlaySum.getId());
			outlaySum.setStatus(OutlaySum.STATUS_DISABLE);
			outlayService.doVoid(outlaySum);
			msg = "编号\"" + outlaySum.getSn() + "\"付款作废成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "编号\"" + outlaySum.getSn() + "\"付款作废失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
