package modules.fin.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.fin.entity.Income;
import modules.fin.entity.IncomeSum;
import modules.fin.service.IncomeService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/incomeSumController")
public class IncomeSumController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(IncomeSumController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private IncomeService incomeService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/fin/incomeSum-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param incomeSum
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(IncomeSum incomeSum, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(IncomeSum.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, incomeSum, request.getParameterMap());
			cq.add();
			incomeService.listByPage(cq, true);
			dataGrid.setFooter("amount,sn:合计");
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("收款记录查询失败", e);
			throw new BusinessException("收款记录查询失败", e);
		}
	}

	/**
	 * 详情页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goDetail")
	public ModelAndView goDetail(IncomeSum incomeSum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(incomeSum.getId())) {
			incomeSum = incomeService.get(IncomeSum.class, incomeSum.getId());
			req.setAttribute("incomeSumPage", incomeSum);
		}
		return new ModelAndView("modules/fin/incomeSum-detail");
	}

	/**
	 * 打印页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goPrint")
	public ModelAndView goPrint(IncomeSum incomeSum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(incomeSum.getId())) {
			incomeSum = incomeService.get(IncomeSum.class, incomeSum.getId());
			req.setAttribute("incomeSumPage", incomeSum);
		}
		return new ModelAndView("modules/fin/incomeSum-print");
	}

	/**
	 * 核销明细
	 * 
	 * @return
	 */
	@RequestMapping(params = "settleTab")
	public ModelAndView settleTab(HttpServletRequest req) {
		String id = req.getParameter("id");
		IncomeSum incomeSum = incomeService.get(IncomeSum.class, id);
		BigDecimal totalIncomeAmount = new BigDecimal(0);
		BigDecimal totalDiscountAmount = new BigDecimal(0);
		BigDecimal totalSettleAmount = new BigDecimal(0);
		for (Income income : incomeSum.getIncomeList()) {
			totalIncomeAmount = totalIncomeAmount.add(income.getIncomeAmount());
			totalDiscountAmount = totalDiscountAmount.add(income.getDiscountAmount());
			totalSettleAmount = totalSettleAmount.add(income.getSettleAmount());
		}
		req.setAttribute("totalIncomeAmount", totalIncomeAmount);
		req.setAttribute("totalDiscountAmount", totalDiscountAmount);
		req.setAttribute("totalSettleAmount", totalSettleAmount);
		req.setAttribute("incomeList", incomeSum.getIncomeList());
		return new ModelAndView("modules/fin/incomeSumDetail-tab");
	}

	/**
	 * 作废
	 * 
	 * @param incomeSum
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doVoid")
	@ResponseBody
	public Result doVoid(IncomeSum incomeSum, HttpServletRequest request) {
		String msg = null;
		try {
			incomeSum = incomeService.get(IncomeSum.class, incomeSum.getId());
			incomeSum.setStatus(IncomeSum.STATUS_DISABLE);
			incomeService.doVoid(incomeSum);
			msg = "编号\"" + incomeSum.getSn() + "\"收款作废成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "编号\"" + incomeSum.getSn() + "\"收款作废失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
