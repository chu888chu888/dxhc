package modules.fin.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.LoginUserUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.fin.entity.OutlayUnitSum;
import modules.fin.service.OutlayUnitService;
import modules.sys.entity.Dict;
import modules.sys.entity.Log;
import modules.sys.service.DictService;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/outlayUnitSumController")
public class OutlayUnitSumController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(OutlayUnitSumController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private OutlayUnitService outlayUnitService;
	@Autowired
	private DictService dictService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/fin/outlayUnitSum-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param outlayUnitSum
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(OutlayUnitSum outlayUnitSum, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(OutlayUnitSum.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, outlayUnitSum, request.getParameterMap());
			cq.add();
			outlayUnitService.listByPage(cq, true);
			dataGrid.setFooter("amount,sn:合计");
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("收款记录查询失败", e);
			throw new BusinessException("收款记录查询失败", e);
		}
	}

	/**
	 * 详情页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goDetail")
	public ModelAndView goDetail(OutlayUnitSum outlayUnitSum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(outlayUnitSum.getId())) {
			outlayUnitSum = outlayUnitService.get(OutlayUnitSum.class, outlayUnitSum.getId());
			req.setAttribute("outlayUnitSumPage", outlayUnitSum);
		}
		return new ModelAndView("modules/fin/outlayUnitSum-detail");
	}

	/**
	 * 打印页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goPrint")
	public ModelAndView goPrint(OutlayUnitSum outlayUnitSum, HttpServletRequest req) {
		if (StringUtils.isNotBlank(outlayUnitSum.getId())) {
			outlayUnitSum = outlayUnitService.get(OutlayUnitSum.class, outlayUnitSum.getId());
			Dict unitDict = dictService.get(Dict.class, outlayUnitSum.getUnit());
			if (unitDict != null) {
				req.setAttribute("unit", unitDict.getName());
			}
			req.setAttribute("outlayUnitSumPage", outlayUnitSum);
		}
		return new ModelAndView("modules/fin/outlayUnitSum-print");
	}

	/**
	 * 核销明细
	 * 
	 * @return
	 */
	@RequestMapping(params = "settleTab")
	public ModelAndView settleTab(HttpServletRequest req) {
		String id = req.getParameter("id");
		OutlayUnitSum outlayUnitSum = outlayUnitService.get(OutlayUnitSum.class, id);
		req.setAttribute("totalAmount", outlayUnitSum.getAmount());
		req.setAttribute("outlayUnitList", outlayUnitSum.getOutlayUnitList());
		return new ModelAndView("modules/fin/outlayUnitSumDetail-tab");
	}

	/**
	 * 作废
	 * 
	 * @param outlayUnitSum
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doVoid")
	@ResponseBody
	public Result doVoid(OutlayUnitSum outlayUnitSum, HttpServletRequest request) {
		String msg = null;
		try {
			outlayUnitSum = outlayUnitService.get(OutlayUnitSum.class, outlayUnitSum.getId());
			outlayUnitSum.setStatus(OutlayUnitSum.STATUS_DISABLE);
			outlayUnitSum.setVoidBy(LoginUserUtil.getUser().getName());
			outlayUnitSum.setVoidDate(new Date());
			outlayUnitService.doVoid(outlayUnitSum);
			msg = "编号\"" + outlayUnitSum.getSn() + "\"收款作废成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "编号\"" + outlayUnitSum.getSn() + "\"收款作废失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

}
