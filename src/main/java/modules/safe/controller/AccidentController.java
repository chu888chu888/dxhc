package modules.safe.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import luxing.common.controller.BaseController;
import luxing.exception.BusinessException;
import luxing.hibernate.CriteriaQuery;
import luxing.util.CriteriaQueryUtil;
import luxing.util.LoginUserUtil;
import luxing.util.ValidatorUtil;
import luxing.web.model.DataGrid;
import luxing.web.model.Result;
import modules.arc.entity.Vehicle;
import modules.arc.service.VehicleService;
import modules.safe.entity.Accident;
import modules.safe.service.AccidentService;
import modules.sys.entity.Log;
import modules.sys.service.LogService;

/**
 * 
 * @author zzc
 *
 */
@Controller
@RequestMapping("/accidentController")
public class AccidentController extends BaseController {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AccidentController.class);

	@Autowired
	private LogService logService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private AccidentService accidentService;

	/**
	 * 列表页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("modules/safe/accident-list");
	}

	/**
	 * 列表数据
	 * 
	 * @param accident
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "list")
	public void list(Accident accident, HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		try {
			CriteriaQuery cq = new CriteriaQuery(Accident.class, dataGrid);
			CriteriaQueryUtil.assembling(cq, accident, request.getParameterMap());
			cq.add();
			accidentService.listByPage(cq, true);
			listView(response, dataGrid);
		} catch (Exception e) {
			logger.error("事故记录查询失败", e);
			throw new BusinessException("事故记录查询失败", e);
		}
	}

	/**
	 * 增加页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(HttpServletRequest req) {
		return new ModelAndView("modules/safe/accident-add");
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(Accident accident, HttpServletRequest req) {
		if (StringUtils.isNotBlank(accident.getId())) {
			accident = accidentService.get(Accident.class, accident.getId());
			req.setAttribute("accidentPage", accident);
		}
		return new ModelAndView("modules/safe/accident-update");
	}

	/**
	 * 处理页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goHandle")
	public ModelAndView goHandle(Accident accident, HttpServletRequest req) {
		if (StringUtils.isNotBlank(accident.getId())) {
			accident = accidentService.get(Accident.class, accident.getId());
			req.setAttribute("accidentPage", accident);
		}
		return new ModelAndView("modules/safe/accident-handle");
	}

	/**
	 * 查看页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "goDetail")
	public ModelAndView goDetail(Accident accident, HttpServletRequest req) {
		if (StringUtils.isNotBlank(accident.getId())) {
			accident = accidentService.get(Accident.class, accident.getId());
			req.setAttribute("accidentPage", accident);
		}
		return new ModelAndView("modules/safe/accident-detail");
	}

	/**
	 * 增加
	 * 
	 * @param accident
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public Result doAdd(Accident accident, HttpServletRequest request) {
		String msg = null;
		try {
			accident.setStatus(Accident.STATUS_NO);
			accident.setRegisterBy(LoginUserUtil.getUser().getName());
			accident.setRegisterDate(new Date());
			// bean校验
			msg = ValidatorUtil.validate(accident);
			if (StringUtils.isNotBlank(msg)) {
				return Result.error(msg);
			}
			accidentService.save(accident);
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故增加成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_ADD);
		} catch (Exception e) {
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故增加失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 修改
	 * 
	 * @param accident
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public Result doUpdate(Accident accident, HttpServletRequest request) {
		String msg = null;
		try {
			accidentService.saveOrUpdate(accident);
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故修改成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故修改失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 处理
	 * 
	 * @param accident
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "doHandle")
	@ResponseBody
	public Result doHandle(Accident accident, HttpServletRequest request) {
		String msg = null;
		try {
			// 设置状态为已处理
			accident.setStatus(Accident.STATUS_YES);
			accidentService.saveOrUpdate(accident);
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故处理成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_UPDATE);
		} catch (Exception e) {
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故处理失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public Result doDel(Accident accident, HttpServletRequest request) {
		String msg = null;
		try {
			accident = accidentService.get(Accident.class, accident.getId());
			accidentService.delete(accident);
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故删除成功";
			logger.info(msg);
			logService.addLogInfo(msg, Log.OPERATE_DEL);
		} catch (Exception e) {
			msg = "车辆\"" + accident.getVehicle().getPlateNumber() + "\"事故删除失败";
			logger.error(msg, e);
			throw new BusinessException(msg, e);
		}
		return Result.success(msg);
	}

	/**
	 * 车辆 change
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "vehicleChange")
	@ResponseBody
	public Result vehicleChange(HttpServletRequest req) {
		Result result = new Result();
		String driverName = "";
		String vehicleId = req.getParameter("vehicleId");
		Map<String, Object> attributes = new HashMap<String, Object>();
		Vehicle vehicle = vehicleService.get(Vehicle.class, vehicleId);
		if (CollectionUtils.isNotEmpty(vehicle.getDriverList())) {
			driverName = vehicle.getDriverList().get(0).getName();
		}
		attributes.put("driverName", driverName);
		result.setAttributes(attributes);
		return result;
	}
}
