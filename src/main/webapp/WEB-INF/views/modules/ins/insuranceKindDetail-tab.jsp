<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="center" bgcolor="#EEEEEE" width="20"><label class="Validform_label">序号</label></td>
			<td align="center" bgcolor="#EEEEEE" width="280"><label class="Validform_label">险种</label></td>
			<td align="center" bgcolor="#EEEEEE" width="200"><label class="Validform_label">保险金额/责任限额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="200"><label class="Validform_label">保费</label></td>
			<td align="center" bgcolor="#EEEEEE" width="70"></td>
		</tr>
		<tbody>
			<c:forEach items="${insuranceKindList}" var="insuranceKind" varStatus="stuts">
				<tr>
					<td align="center"><div style="width: 30px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center"><c:out value="${insuranceKind.insKindName}"></c:out></td>
					<td align="center"><input name="insuranceKindList[${stuts.index }].insAmount"
						value="${insuranceKind.insAmount}" type="text" class="inputxt" readonly="readonly"></td>
					<td align="center"><input name="insuranceKindList[${stuts.index }].premium" value="${insuranceKind.premium}"
						type="text" class="inputxt" readonly="readonly"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

