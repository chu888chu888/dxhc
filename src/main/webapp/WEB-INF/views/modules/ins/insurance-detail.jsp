<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(document).ready(function() {
		var type = $("#type").val()
		if (type == '1') {
			$("#insKindTab").hide();
		} else if (type == '2') {
			$("#insKindTab").show();
		}
	});
</script>
</head>
<body>
	<t:form id="insuranceDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="vehicle.sn" class="inputxt" readonly="readonly"
					value="${insurancePage.vehicle.sn}" /></td>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input name="vehicle.plateNumber" class="inputxt" readonly="readonly"
					value="${insurancePage.vehicle.plateNumber}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">保险类型:</label></td>
				<td class="value"><select id="type" name="type" disabled="disabled">
						<option value="1" <c:if test="${insurancePage.type eq 1}">selected="selected"</c:if>>交强险</option>
						<option value="2" <c:if test="${insurancePage.type eq 2}">selected="selected"</c:if>>商业险</option>
				</select></td>
				<td align="right"><label class="Validform_label">保险公司:</label></td>
				<td class="value"><t:dict id="insCompany" name="insCompany" type="select" groupCode="insCompany"
						defaultVal="${insurancePage.insCompany}"></t:dict></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">保费:</label></td>
				<td class="value"><input name="premium" type="text" class="inputxt" value="${insurancePage.premium}"></td>
				<td align="right"><label class="Validform_label">车船税:</label></td>
				<td class="value"><input name="vavt" type="text" class="inputxt" value="${insurancePage.vavt}"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">生效日期:</label></td>
				<td class="value"><input name="startDate" type="text" class="Wdate"
					value='<fmt:formatDate value='${insurancePage.startDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
				<td align="right"><label class="Validform_label">到期日期:</label></td>
				<td class="value"><input name="endDate" type="text" class="Wdate"
					value='<fmt:formatDate value='${insurancePage.endDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 575px; height: 30px;">${insurancePage.remarks}</textarea></td>
			</tr>
		</table>
		<div style="width: 800px; height: 355px;" id="insKindTab">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div title="保险种类" href="insuranceController.do?insKindDetailTab&id=${insurancePage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>