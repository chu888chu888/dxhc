<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(document).ready(function() {
		$("#insKindTab").hide();
	});

	function vehicleChange() {
		var vehicleId = $("#vehicleId").val();
		if (vehicleId != "" && vehicleId != null && vehicleId != undefined) {
			var type = $("#type").val();
			var url = "insuranceController.do?vehicleChange&vehicleId="
					+ vehicleId + "&type=" + type;
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						$('#startDate').val(d.attributes.startDate);
						$('#endDate').val(d.attributes.endDate);
					}
				}
			});
		}
	}

	function typeChange() {
		var type = $("#type").val()
		if (type == '1') {
			$("#insKindTab").hide();
		} else if (type == '2') {
			$("#insKindTab").show();
		}
		//生效日期、到期日期
		var vehicleId = $("#vehicleId").val();
		var url = "insuranceController.do?typeChange&vehicleId=" + vehicleId
				+ "&type=" + type;
		$.ajax({
			type : 'POST',
			url : url,
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.success) {
					$('#startDate').val(d.attributes.startDate);
					$('#endDate').val(d.attributes.endDate);
				}
			}
		});
	}

	function startDateChange() {
		var startDate = $("#startDate").val();
		var url = "insuranceController.do?startDateChange&startDate="
				+ startDate;
		$.ajax({
			type : 'POST',
			url : url,
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.success) {
					$('#endDate').val(d.attributes.endDate);
				}
			}
		});
	}

	function addDict(groupCode) {
		createAddUpdateWindow('增加', 'dictList',
				'dictController.do?goAddCustom&groupCode=' + groupCode, 600,
				300);
	}
</script>
</head>
<body>
	<t:form id="insuranceAdd" action="insuranceController.do?doAdd" tiptype="1">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value" colspan="3"><input id="vehicleId" name="vehicle.id" type="hidden" /> <input
					name="vehicle.plateNumber" id="vehiclePlateNumber" class="inputxt" readonly="readonly"
					onclick="choose_vehiclePlateNumber()" datatype="*" nullmsg="请填写车辆" /> <t:gridSelect nameId="vehiclePlateNumber"
						url="vehicleController.do?select" valueId="vehicleId" gridField="plateNumber" gridName="vehicleSelectList"
						callback="vehicleChange();" title="车辆列表" height="460" width="800"></t:gridSelect></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">保险类型:</label></td>
				<td class="value"><select name="type" id="type" datatype="*" nullmsg="请填写保险类型" onchange="typeChange()">
						<option value="1">交强险</option>
						<option value="2">商业险</option>
				</select></td>
				<td align="right"><label class="Validform_label">保险公司:</label></td>
				<td class="value"><t:dict id="insCompany" name="insCompany" type="select" groupCode="insCompany" datatype="*"></t:dict><a
					onclick="addDict('insCompany');"><span style="font-size: 12px; margin-left: 5px; color: #c1b7b7">增加</span></a></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">保费:</label></td>
				<td class="value"><input name="premium" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写保费"
					errormsg="请填写正确金额"></td>
				<td align="right"><label class="Validform_label">车船税:</label></td>
				<td class="value"><input name="vavt" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" ignore="ignore"
					errormsg="请填写正确金额"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">生效日期:</label></td>
				<td class="value"><input id="startDate" name="startDate" type="text" class="Wdate" onClick="WdatePicker()"
					datatype="date" nullmsg="请填写生效日期" onchange="startDateChange()"></td>
				<td align="right"><label class="Validform_label">到期日期:</label></td>
				<td class="value"><input id="endDate" name="endDate" type="text" class="Wdate" onClick="WdatePicker()"
					datatype="date" nullmsg="请填写到期日期"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 545px; height: 30px;"></textarea></td>
			</tr>
		</table>
		<div style="width: 800px; height: 355px;" id="insKindTab">
			<div border="false" class="easyui-tabs" fit="true">
				<div title="保险种类" href="insuranceController.do?insKindTab"></div>
			</div>
		</div>
	</t:form>
</body>
</html>