<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(function() {
		//初始化
		$("#rptVehicleListtb").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
	});
	function loadList(rptType) {
		$("#rptVehicleList").datagrid('reload', {
			rptType : rptType
		});
	}
</script>
<div class="easyui-layout" fit="true">
	<div region="west" style="width: 150px;" title="车辆报表" split="true" collapsed="false">
		<div class="easyui-panel" style="padding: 10px; border: 0px" fit="true" border="false">
			<div style="padding: 10px;">
				<span><a onclick="loadList(1)" style="font-weight: 600; color: #676a6c">车辆状态报表</a></span>
			</div>
			<div style="padding: 10px;">
				<span><a onclick="loadList(2)" style="font-weight: 600; color: #676a6c">车辆动态报表</a></span>
			</div>
		</div>
	</div>
	<div region="center" style="padding: 0px; border: 0px">
		<t:grid name="rptVehicleList" url="rptVehicleController.do?list" checkbox="false" pagination="false">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="年份" field="year" dateFormat="yyyy-MM-dd" hidden="true"></t:gridCol>
			<t:gridCol title="" field="name"></t:gridCol>
			<t:gridCol title="1月" field="m1"></t:gridCol>
			<t:gridCol title="2月" field="m2"></t:gridCol>
			<t:gridCol title="3月" field="m3"></t:gridCol>
			<t:gridCol title="4月" field="m4"></t:gridCol>
			<t:gridCol title="5月" field="m5"></t:gridCol>
			<t:gridCol title="6月" field="m6"></t:gridCol>
			<t:gridCol title="7月" field="m7"></t:gridCol>
			<t:gridCol title="8月" field="m8"></t:gridCol>
			<t:gridCol title="9月" field="m9"></t:gridCol>
			<t:gridCol title="10月" field="m10"></t:gridCol>
			<t:gridCol title="11月" field="m11"></t:gridCol>
			<t:gridCol title="12月" field="m12"></t:gridCol>
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums" id="searchColums">
		<form onkeydown='if(event.keyCode==13){rptVehicleListSearch();return false;}' id='rptVehicleListForm'>
			<link rel="stylesheet" href="plug-in/Validform/css/tablefrom.css" type="text/css">
			<link rel="stylesheet" href="plug-in/bootstrap/bootstrap-btn.css" type="text/css">
			<span style="display: -moz-inline-box; display: inline-block;"><span
				style="vertical-align: middle; display: inline-block; width: 40px; text-align: right; text-overflow: ellipsis;"
				title="年份">年份:</span><input onkeypress="EnterPress(event)" onkeydown="EnterPress()" type="text" name="year"
				style="width: 100px" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy'})" /></span><span
				style="float: right; padding-right: 5px;"><button id="queryBtn" style="font-size: 12px; font-weight: 600;"
					class="btn btn-info" onclick="rptVehicleListSearch()" type="button">查询</button>
				<button id="resetBtn" style="font-size: 12px; font-weight: 600;" class="btn btn-primary"
					onclick="searchReset('rptVehicleList')" type="button">重置</button></span>
		</form>
	</div>
</div>
