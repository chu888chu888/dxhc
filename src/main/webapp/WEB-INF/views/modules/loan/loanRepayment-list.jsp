<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="loanRepaymentList" url="loanRepaymentController.do?list" sortName="createDate">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="loan.loanContract.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="loanContractId" field="loan.loanContract.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="loan.sn" query="true" formatter="loanContractFormat"></t:gridCol>
			<t:gridCol title="责任人" field="loan.loanContract.owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="还本金" field="principal" align="right"></t:gridCol>
			<t:gridCol title="还利息" field="interest" align="right"></t:gridCol>
			<t:gridCol title="还款总额" field="totalAmount" align="right"></t:gridCol>
			<t:gridCol title="收款人" field="createBy" query="true"></t:gridCol>
			<t:gridCol title="收款时间 " field="createDate" dateFormat="yyyy-MM-dd hh:mm:ss" query="true" queryMode="group"></t:gridCol>
			<t:gridCol title="作废人 " field="updateBy"></t:gridCol>
			<t:gridCol title="作废时间 " field="updateDate" dateFormat="yyyy-MM-dd hh:mm:ss"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="正常_0,作废_1" style="background-color:#ed5565;color:#FFF;_1"
				align="center" width="30"></t:gridCol>
			<t:gridBar title="作废" icon="icon-void" url="loanRepaymentController.do?doVoid" funname="doVoid"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="30" align="center"></t:gridCol>
			<t:gridOpt title="打印" funname="print(id)" style="btn_blue" exp="status#eq#0" />
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function doVoid() {
		var rows = $('#loanRepaymentList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('收款已作废');
			return;
		}
		createDialog('作废确认', '确定作废该记录 ?',
				'loanRepaymentController.do?doVoid&id=' + rows[0].id,
				'loanRepaymentList');
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["loan.loanContract.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'loanRepaymentList', 800, 500)
	}

	function loanContractFormat(value, rec, index) {
		var loanContractId = rec["loan.loanContract.id"];
		if (loanContractId != undefined) {
			return '<a href=\'#\' onclick=loanContractDetail(\''
					+ loanContractId + '\')>' + value + '</a>';
		}
	}

	function loanContractDetail(id) {
		openwindow('查看', 'loanContractController.do?goDetail&id=' + id,
				'loanRepaymentList', 800, 600)
	}

	function print(id) {
		openPrintWindow('打印', 'loanRepaymentController.do?goPrint&id=' + id,
				'loanRepaymentList', 260, 600);
	}
</script>

