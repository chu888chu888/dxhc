<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	function genPlan() {
		var principal = $("#principal").val();
		var period = $("#period").val();
		var rate = $("#rate").val();
		var startDate = $("#startDate").val();
		var repaymentType = $("#repaymentType").val();
		var genType = $("#genType").val();
		if (genType == '1'
				&& (isMoney(principal) && isMoney(period) && isMoney(rate) && isNotBlank(startDate))) {
			//自动生成
			var url = "loanContractController.do?planTab&principal="
					+ principal + "&period=" + period + "&rate=" + rate
					+ "&startDate=" + startDate + "&repaymentType="
					+ repaymentType + "&genType=" + genType;
			refreshTab(url);
		} else if (genType == '2' && (isMoney(period) && isNotBlank(startDate))) {
			//手动输入
			var url = "loanContractController.do?planTab&period=" + period
					+ "&startDate=" + startDate + "&genType=" + genType;
			refreshTab(url);
		}

	}

	//刷新指定Tab的内容  
	function refreshTab(url) {
		if ($('#tt').tabs('exists', '还款计划')) {
			var currTab = $('#tt').tabs('getTab', '还款计划');
			$('#tt').tabs('update', {
				tab : currTab,
				options : {
					id : 'planTab',
					title : '还款计划',
					href : url,
				}
			});
		}
	}

	function isNotBlank(v) {
		if (v != "" || v != null || v != undefined) {
			return true;
		}
		return false;
	}
	function isMoney(v) {
		var regExp = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		if (isNotBlank(v) && regExp.test(v)) {
			return true;
		}
		return false;
	}
</script>
</head>
<body>
	<t:form id="loanContractAdd" action="loanContractController.do?doAdd" tiptype="1">
		<input name="status" value="0" type="hidden">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input id="ownerId" name="owner.id" type="hidden" /> <input name="owner.name" id="ownerName"
					class="inputxt" readonly="readonly" onclick="choose_ownerName()" datatype="*" nullmsg="请选择责任人" /> <t:gridSelect
						nameId="ownerName" url="ownerController.do?select" valueId="ownerId" gridField="name" gridName="ownerSelectList"
						title="责任人" height="460" width="800"></t:gridSelect></td>
				<td align="right"><label class="Validform_label">借款金额(元):</label></td>
				<td class="value"><input id="principal" name="principal" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写借款金额"
					errormsg="请填写正确金额" onchange="genPlan()"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">期数:</label></td>
				<td class="value"><input id="period" name="period" type="text" class="inputxt" datatype="n" nullmsg="请填写期数"
					onchange="genPlan()"></td>
				<td align="right"><label class="Validform_label">利率(月):</label></td>
				<td class="value"><input id="rate" name="rate" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写利率"
					errormsg="请填写正确利率" onchange="genPlan()">%</td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">起息日:</label></td>
				<td class="value"><input id="startDate" name="startDate" type="text" class="Wdate" onClick="WdatePicker()"
					datatype="date" nullmsg="请填写起息日" onchange="genPlan()"></td>
				<td align="right"><label class="Validform_label">还款方式:</label></td>
				<td class="value"><select name="repaymentType" id="repaymentType" datatype="*" nullmsg="请填写还款方式"
					onchange="genPlan()">
						<option value="1">等额本金</option>
						<option value="2">等额本息</option>
						<option value="3">等本等息</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">生成方式:</label></td>
				<td class="value"><select id="genType" datatype="*" nullmsg="请填写生成方式" onchange="genPlan()">
						<option value="1">自动生成</option>
						<option value="2">手动输入</option>
				</select></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" type="text" class="inputxt"></td>
			</tr>
		</table>
		<div style="width: 800px; height: 520px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="planTab" title="还款计划" href="loanContractController.do?planTab"></div>
			</div>
		</div>
	</t:form>
</body>
</html>