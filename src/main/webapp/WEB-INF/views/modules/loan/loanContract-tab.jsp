<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<style>
.loanTable {
	margin: 0 auto;
	border-collapse: collapse;
}

.loanThead {
	background: #EEEEEE;
	display: block
}

.loanTbody {
	height: 445px;
	overflow-y: scroll;
	display: block
}

.td50 {
	width: 50px;
	min-width: 50px;
	max-width: 50px;
}

.td160 {
	width: 160px;
	min-width: 160px;
	max-width: 160px;
}

.td180 {
	width: 180px;
	min-width: 180px;
	max-width: 180px;
}

.td200 {
	width: 200px;
	min-width: 200px;
	max-width: 200px;
}
</style>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0" class="loanTable">
		<thead class="loanThead">
			<tr bgcolor="#E6E6E6">
				<td align="center" class="td50"><label class="Validform_label">期数</label></td>
				<td align="center" class="td200"><label class="Validform_label">还款日</label></td>
				<td align="center" class="td160"><label class="Validform_label">本金</label></td>
				<td align="center" class="td160"><label class="Validform_label">利息</label></td>
				<td align="center" class="td180"><label class="Validform_label">应还总额</label></td>
			</tr>
		</thead>
		<tbody id="loanListTable" class="loanTbody">
			<c:forEach items="${loanList}" var="loan" varStatus="stuts">
				<tr>
					<td align="center" class="td50"><input name="loanList[${stuts.index }].period" value="${stuts.index+1 }"
						type="hidden"><input name="loanList[${stuts.index }].status" value="0" type="hidden">
						<div style="width: 30px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center" class="td200"><input name="loanList[${stuts.index }].endDate"
						value='<fmt:formatDate value='${loan.endDate}' type="date" pattern="yyyy-MM-dd"/>' type="text" class="Wdate"
						onClick="WdatePicker()" datatype="date" nullmsg="请填写还款日" style="width: 120px;"></td>
					<td align="center" class="td160"><input name="loanList[${stuts.index }].principal" value="${loan.principal}"
						type="text" class="inputxt" datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/"
						onchange="calAmount()" style="width: 120px; text-align: right;"></td>
					<td align="center" class="td160"><input name="loanList[${stuts.index }].interest" value="${loan.interest}"
						type="text" class="inputxt" datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/"
						onchange="calAmount()" style="width: 120px; text-align: right;"></td>
					<td align="center" class="td180"><input name="loanList[${stuts.index }].totalAmount"
						value="${loan.totalAmount}" type="text" class="inputxt"
						datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" readonly="readonly"
						style="width: 120px; text-align: right;"></td>
				</tr>
			</c:forEach>
			<tr>
				<td></td>
				<td style="font-weight: 700;" align="center" class="td200">总计:</td>
				<td id="totalPrincipal" style="color: red; font-weight: 700; text-align: center;" class="td160">${totalPrincipal}</td>
				<td id="totalInterest" style="color: red; font-weight: 700; text-align: center;" class="td160">${totalInterest}</td>
				<td id="totalAmount" style="color: red; font-weight: 700; text-align: center;" class="td160">${totalAmount}</td>
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	function calAmount() {
		var totalPrincipal = 0;
		var totalInterest = 0;
		var regExp = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#loanListTable").find('>tr').each(function(i) {
			var amount = 0;
			$(':input', this).each(function() {
				var $this = $(this);
				var name = $this.attr('name');
				if (name != null && name.indexOf('principal') >= 0) {
					var val = $this.val();
					if (regExp.test(val)) {
						totalPrincipal = addAmount(totalPrincipal, val);
						amount = addAmount(amount, val);
					}
				}
				if (name != null && name.indexOf('interest') >= 0) {
					var val = $this.val();
					if (regExp.test(val)) {
						totalInterest = addAmount(totalInterest, val);
						amount = addAmount(amount, val);
					}
				}
				if (name != null && name.indexOf('totalAmount') >= 0) {
					$this.val(amount);
				}
			});
		});
		$("#totalPrincipal").html(totalPrincipal);
		$("#totalInterest").html(totalInterest);
		$("#totalAmount").html(addAmount(totalPrincipal, totalInterest));
	}

	function addAmount(arg1, arg2) {
		var tem = arg1 * 10 + arg2 * 10;
		tem = tem / 10;
		return tem;
	}
</script>

