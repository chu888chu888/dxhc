<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="loanContractList" url="loanContractController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true" width="80"></t:gridCol>
			<t:gridCol title="责任人" field="owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="贷款金额 " field="principal" align="right"></t:gridCol>
			<t:gridCol title="利息" field="interest" align="right"></t:gridCol>
			<t:gridCol title="总金额" field="totalAmount" align="right"></t:gridCol>
			<t:gridCol title="期数 " field="period" align="right"></t:gridCol>
			<t:gridCol title="月利率(%) " field="rate" align="right"></t:gridCol>
			<t:gridCol title="还款方式" field="repaymentType" replace="等额本金_1,等额本息_2,等本等息_3" align="center" width="40"></t:gridCol>
			<t:gridCol title="起息日 " field="startDate" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridCol title="尚欠金额" field="oweAmount" align="right"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="未还款_0,还款中_1,已还完_2"
				style="background-color:#f8ac59;color:#FFF;_1,background-color:#ed5565;color:#FFF;_0" align="center" width="30"></t:gridCol>
			<t:gridBar title="增加" icon="icon-add" url="loanContractController.do?goAdd" funname="add" height="600"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="loanContractController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="loanContractController.do?goDetail" funname="detail" height="700"></t:gridBar>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'loanContractController.do?goDetail', 'loanContractList',
				800, 600);
	}

	function del() {
		var rows = $('#loanContractList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('贷款还款中,无法删除');
			return;
		}
		if (rows[0].status == '2') {
			tip('贷款已还完,无法删除');
			return;
		}
		createDialog('删除确认', '确定删除项目 ?', 'loanContractController.do?doDel&id='
				+ rows[0].id, 'loanContractList');
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'loanContractList', 800, 500)
	}
</script>