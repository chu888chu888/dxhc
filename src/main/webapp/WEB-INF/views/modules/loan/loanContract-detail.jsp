<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="loanContractDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="sn" type="text" class="inputxt" value="${loanContractPage.sn}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="ownerName" type="text" class="inputxt" value="${loanContractPage.owner.name}"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">贷款金额:</label></td>
				<td class="value"><input name="principal" type="text" class="inputxt" value="${loanContractPage.principal}"
					readonly="readonly"></td>
				<td align="right"><label class="Validform_label">期数:</label></td>
				<td class="value"><input name="period" type="text" class="inputxt" value="${loanContractPage.period}"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">利率(月):</label></td>
				<td class="value"><input name="rate" type="text" class="inputxt" value="${loanContractPage.rate}"
					readonly="readonly">%</td>
				<td align="right"><label class="Validform_label">还款方式:</label></td>
				<td class="value"><select name="repaymentType" disabled="disabled">
						<option value="1" <c:if test="${loanContractPage.repaymentType eq 1}">selected="selected"</c:if>>等额本金</option>
						<option value="2" <c:if test="${loanContractPage.repaymentType eq 2}">selected="selected"</c:if>>等额本息</option>
						<option value="3" <c:if test="${loanContractPage.repaymentType eq 3}">selected="selected"</c:if>>等本等息</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">起息日:</label></td>
				<td class="value"><input name="startDate" type="text" class="Wdate" readonly="readonly"
					value='<fmt:formatDate value='${loanContractPage.startDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" type="text" class="inputxt" value="${loanContractPage.remarks}"
					readonly="readonly"></td>
			</tr>
		</table>
		<div style="width: 800px; height: 520px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="planTab" title="还款计划" href="loanContractController.do?planTabDetail&id=${loanContractPage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>