<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	function nameChange() {
		var type = $('#type').val();
		if (type == '4') {
			var name = $('#name').val();
			var url = "ownerController.do?nameChange&name=" + name;
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.attributes.exist == 'yes') {
						//相同姓名司机已存在,类型切换为仅责任人
						$('#type').val('2');
						typeChange();
					}
				}
			});
		}
	}

	function idCardChange() {
		var type = $('#type').val();
		if (type == '4') {
			var idCard = $('#idCard').val();
			var url = "ownerController.do?idCardChange&idCard="
					+ encodeURIComponent(idCard);
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.attributes.exist == 'yes') {
						//相同身份证司机已存在,类型切换为仅责任人
						$('#type').val('2');
						typeChange();
					}
				}
			});
		}
	}

	function phoneChange() {
		var type = $('#type').val();
		if (type == '4') {
			var phone = $('#phone').val();
			var url = "ownerController.do?phoneChange&phone="
					+ encodeURIComponent(phone);
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.attributes.exist == 'yes') {
						//相同手机司机已存在,类型切换为仅责任人
						$('#type').val('2');
						typeChange();
					}
				}
			});
		}
	}

	//类型改变 
	function typeChange() {
		var type = $('#type').val();
		if (type == '4') {
			$('#tt').tabs('close', '扩展信息');
			$('#tt').tabs('add', {
				id : 'extTab',
				title : '扩展信息',
				href : 'ownerController.do?ext&type=' + type,
			});
			//姓名，身份证，手机设置为空
			$('#name').val('');
			$('#idCard').val('');
			$('#phone').val('');
		} else if (type = '2') {
			refreshTab('扩展信息', type);
		}
	}
	//刷新指定Tab的内容  
	function refreshTab(title, type) {
		if ($('#tt').tabs('exists', title)) {
			var currTab = $('#tt').tabs('getTab', title);
			$('#tt').tabs('update', {
				tab : currTab,
				options : {
					id : 'extTab',
					title : title,
					href : 'ownerController.do?ext&type=' + type,
				}
			});
		}
	}
</script>
</head>
<body>
	<t:form id="ownerAdd" action="ownerController.do?doAdd">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right" width="100"><label class="Validform_label">兼司机:</label></td>
				<td class="value"><select name="type" id="type" datatype="*" onchange="typeChange();">
						<option value="4">是</option>
						<option value="2">否</option>
				</select></td>
				<td align="right" width="100"><label class="Validform_label">姓名:</label></td>
				<td class="value"><input id="name" name="name" type="text" class="inputxt" datatype="*" nullmsg="请填写姓名"
					ajaxurl="ownerController.do?validName" onchange="nameChange()" /></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">性别:</label></td>
				<td class="value"><select name="sex" datatype="*">
						<option value="1">男</option>
						<option value="2">女</option>
				</select></td>
				<td align="right" width="100"><label class="Validform_label">身份证:</label></td>
				<td class="value"><input id="idCard" name="idCard" type="text" class="inputxt" datatype="idcard"
					ignore="ignore" ajaxurl="ownerController.do?validIdCard" onchange="idCardChange()" /></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">手机:</label></td>
				<td class="value"><input id="phone" class="inputxt" type="text" name="phone" datatype="m"
					ajaxurl="ownerController.do?validPhone" onchange="phoneChange()"></td>
				<td align="right" width="100"><label class="Validform_label">地址:</label></td>
				<td class="value"><input name="address" class="inputxt" type="text"></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><input name="remarks" class="inputxt" type="text"></td>
			</tr>
		</table>
		<div style="width: 800px; height: 200px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="extTab" title="扩展信息" href="ownerController.do?ext"></div>
			</div>
		</div>
	</t:form>
</body>
</html>