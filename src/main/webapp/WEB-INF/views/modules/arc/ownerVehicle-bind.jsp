<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="ownerVehicleBindList" url="ownerController.do?ownerVehicleBindList&ownerId=${ownerId}"
			singleSelect="false" pagination="false">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="plateNumber" query="true"></t:gridCol>
			<t:gridCol title="类型" field="type" width="40" query="true" replace="牵引车_1,挂车_2,整车_3" align="center"></t:gridCol>
			<t:gridCol title="状态" field="status" width="40" dictGroup="vehicleStatus" dictExt="注销_-1" align="center" query="true"
				style="background-color:#ed5565;color:#FFF;_-1"></t:gridCol>
			<t:gridCol title="普货/危货" field="generalDanger" width="40" replace="普货_1,危货_2" align="center"></t:gridCol>
			<t:gridCol title="登记日期" field="registerDate" width="50" dateFormat="yyyy-MM-dd"></t:gridCol>
		</t:grid>
	</div>
</div>
<div style="display: none">
	<t:form id="ownerVehicleBind" action="ownerController.do?doBind&ownerId=${param.ownerId}" beforeSubmit="setVehicleIds">
		<input id="vehicleIds" name="vehicleIds">
	</t:form>
</div>
<script>
	function setVehicleIds() {
		$("#vehicleIds").val(getOwnerVehicleAddListSelections('id'));
		return true;
	}

	function getOwnerVehicleAddListSelections(field) {
		var ids = [];
		var rows = $('#ownerVehicleBindList').datagrid('getSelections');
		for (var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	}
</script>
