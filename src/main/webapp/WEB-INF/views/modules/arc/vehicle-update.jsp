<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(document).ready(function() {
		var type = $("#type").val()
		if (type == '1') {
			$("#relationVehicle").show();
			$("#trailer").show();
			$("#tractor").hide();
		} else if (type == '2') {
			$("#relationVehicle").show();
			$("#trailer").hide();
			$("#tractor").show();
		} else if (type == '3') {
			$("#relationVehicle").hide();
		}
	});

	function ownerChange() {
		var ownerId = $("#ownerId").val();
		var url = "vehicleController.do?ownerChange&ownerId=" + ownerId;
		$.ajax({
			type : 'POST',
			url : url,
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.success) {
					$('#driverId').val(d.attributes.driverId);
					$('#driverName').val(d.attributes.driverName);
				}
			}
		});
	}

	function typeChange() {
		var type = $("#type").val();
		if (type == '1') {
			$("#relationVehicle").show();
			$("#trailer").show();
			$("#tractor").hide();
		} else if (type == '2') {
			$("#relationVehicle").show();
			$("#trailer").hide();
			$("#tractor").show();
		} else if (type == '3') {
			$("#relationVehicle").hide();
		}
	}

	$(function() {
		//车牌栏位有输入时触发的事件
		$("#plateNumber").keydown(function() {
			if (event.keyCode >= 65 && event.keyCode <= 90) {
				var txt = "";
				switch (event.keyCode) {
				case 65:
					txt = "A";
					break;
				case 66:
					txt = "B";
					break;
				case 67:
					txt = "C";
					break;
				case 68:
					txt = "D";
					break;
				case 69:
					txt = "E";
					break;
				case 70:
					txt = "F";
					break;
				case 71:
					txt = "G";
					break;
				case 72:
					txt = "H";
					break;
				case 73:
					txt = "I";
					break;
				case 74:
					txt = "J";
					break;
				case 75:
					txt = "K";
					break;
				case 76:
					txt = "L";
					break;
				case 77:
					txt = "M";
					break;
				case 78:
					txt = "N";
					break;
				case 79:
					txt = "O";
					break;
				case 80:
					txt = "P";
					break;
				case 81:
					txt = "Q";
					break;
				case 82:
					txt = "R";
					break;
				case 83:
					txt = "S";
					break;
				case 84:
					txt = "T";
					break;
				case 85:
					txt = "U";
					break;
				case 86:
					txt = "V";
					break;
				case 87:
					txt = "W";
					break;
				case 88:
					txt = "X";
					break;
				case 89:
					txt = "Y";
					break;
				case 90:
					txt = "Z";
					break;

				}
				$('#plateNumber').val($('#plateNumber').val() + txt);
				window.event.keyCode = 0;
				event.returnValue = false;
			}
		});
	});
</script>
</head>
<body>
	<t:form id="vehicleUpdate" action="vehicleController.do?doUpdate">
		<input name="id" type="hidden" value="${vehiclePage.id }">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right" width="100"><label class="Validform_label">编号:</label></td>
				<td class="value"><input id="sn" name="sn" type="text" class="inputxt" value="${vehiclePage.sn}" /></td>
				<td align="right" width="100"><label class="Validform_label">类型:</label></td>
				<td class="value"><select name="type" id="type" datatype="*" onchange="typeChange()">
						<option value="1" <c:if test="${vehiclePage.type eq 1}">selected="selected"</c:if>>牵引车</option>
						<option value="2" <c:if test="${vehiclePage.type eq 2}">selected="selected"</c:if>>挂车</option>
						<option value="3" <c:if test="${vehiclePage.type eq 3}">selected="selected"</c:if>>整车</option>
				</select></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">车牌:</label></td>
				<td class="value"><input id="plateNumber" name="plateNumber" type="text" class="inputxt"
					value="${vehiclePage.plateNumber}"
					datatype="/^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂]{1}$/"
					ajaxurl="vehicleController.do?validPlateNumber" nullmsg="请填写车牌号码" errormsg="请填写正确车牌号码" /></td>
				<td align="right" width="100"><label class="Validform_label">普货/危货:</label></td>
				<td class="value"><select name="generalDanger" id="generalDanger" datatype="*">
						<option value="1" <c:if test="${vehiclePage.generalDanger eq 1}">selected="selected"</c:if>>普货</option>
						<option value="2" <c:if test="${vehiclePage.generalDanger eq 2}">selected="selected"</c:if>>危货</option>
				</select></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">状态:</label></td>
				<td class="value"><t:dict name="status" id="status" type="select" groupCode="vehicleStatus" datatype="*"
						defaultVal="${vehiclePage.status}"></t:dict><a onclick="addDict('vehicleStatus');"><span
						style="font-size: 12px; margin-left: 5px; color: #c1b7b7">增加</span></a></td>
				<td align="right" width="100"><label class="Validform_label">登记日期:</label></td>
				<td class="value"><input id="registerDate" name="registerDate" type="text" class="Wdate"
					onClick="WdatePicker()"
					value='<fmt:formatDate value='${vehiclePage.registerDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input id="ownerId" name="owner.id" type="hidden" value="${vehiclePage.owner.id}" /> <input
					name="owner.name" id="ownerName" value='${vehiclePage.owner.name}' class="inputxt" readonly="readonly"
					onclick="choose_ownerName()" datatype="*" />
				<t:gridSelect nameId="ownerName" url="ownerController.do?select" valueId="ownerId" gridField="name"
						gridName="ownerSelectList" callback="ownerChange()" title="责任人" height="459" width="800"></t:gridSelect><a
					onclick="addOwner();"><span style="font-size: 12px; margin-left: 5px; color: #c1b7b7">增加</span></a></td>
				<td align="right" width="100"><label class="Validform_label">司机:</label></td>
				<td class="value"><input id="driverId" name="driverId" type="hidden" value="${driverId}" /> <input
					name="driverName" id="driverName" class="inputxt" readonly="readonly" onclick="driverSelect()"
					value="${driverName}" /></td>
			</tr>
			<tr id="relationVehicle">
				<td align="right" width="100"><label class="Validform_label"><span id="trailer">挂车:</span><span
						id="tractor">牵引车:</span></label></td>
				<td class="value" colspan="3"><input id="relationVehicleId" name="relationVehicleId" type="hidden"
					value="${vehiclePage.relationVehicleId}" /> <input name="relationVehiclePlateNumber"
					id="relationVehiclePlateNumber" class="inputxt" readonly="readonly" onclick="choose_relationVehiclePlateNumber()"
					value="${relationVehiclePlateNumber}" /> <t:gridSelect nameId="relationVehiclePlateNumber"
						url="vehicleController.do?select" valueId="relationVehicleId" gridField="plateNumber" gridName="vehicleSelectList"
						title="车辆列表" height="459" width="800"></t:gridSelect></td>
			</tr>
			<tr>
				<td align="right" width="100"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" id="remarks" name="remarks"
						style="width: 500px; height: 30px;">${vehiclePage.remarks }</textarea></td>
			</tr>
		</table>
		<div style="width: 800px; height: 300px;">
			<div id="tt" border="false" class="easyui-tabs" fit="true">
				<div id="extTab" title="扩展信息" href="vehicleController.do?ext&id=${vehiclePage.id}"></div>
			</div>
		</div>
	</t:form>
	<script type="text/javascript">
		function driverSelect() {
			var driverId = $("#driverId").val();
			var driverName = $("#driverName").val();
			var url = 'driverController.do?selectMore&driverId=' + driverId
					+ "&driverName=" + driverName;
			$.dialog.setting.zIndex = getzIndex();
			$.dialog({
				content : 'url:' + encodeURI(url),
				zIndex : getzIndex(),
				title : '司机列表',
				lock : true,
				width : '860px',
				height : '460px',
				opacity : 0.4,
				button : [ {
					name : '确定',
					callback : callbackDriverSelect,
					focus : true
				}, {
					name : '取消',
					callback : function() {
					}
				} ]
			}).zindex();
		}
		function callbackDriverSelect() {
			var iframe = this.iframe.contentWindow;
			var table = iframe.$("#driverTable");
			var driverId = '', driverName = '';
			$(table).find("tbody tr").each(function() {
				driverId += $(this).find("input").val() + ",";
				driverName += $(this).find("span").text() + ",";

			})
			$("#driverName").val(driverName);
			$("#driverName").blur();
			$("#driverId").val(driverId);
		}
	</script>
</body>
</html>