<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="vehicleDisable" action="vehicleController.do?doDisable">
		<input name="id" type="hidden" value="${vehiclePage.id }">
		<input name="status" type="hidden" value="${vehiclePage.status }">
		<input name="remarks" type="hidden" value="${vehiclePage.remarks }">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="sn" type="text" class="inputxt" value="${vehiclePage.sn}" readonly="readonly" /></td>
				<td align="right"><label class="Validform_label">车牌:</label></td>
				<td class="value"><input name="plateNumber" type="text" class="inputxt" value="${vehiclePage.plateNumber}"
					readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">注销原因:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="reason" style="width: 491px; height: 100px;"
						datatype="*"></textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>