<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="vehicleSelectList" url="vehicleController.do?list&owner.id=${param.ownerId}&type=${param.type}"
			onLoadSuccess="selectRecord" pageSize="10">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="plateNumber" query="true"></t:gridCol>
			<t:gridCol title="责任人" field="owner.name"></t:gridCol>
			<t:gridCol title="手机" field="owner.phone"></t:gridCol>
			<t:gridCol title="类型" field="type" replace="牵引车_1,挂车_2,整车_3" width="40" align="center"></t:gridCol>
			<t:gridCol title="状态" field="status" dictGroup="vehicleStatus" dictExt="注销_-1" width="40" align="center"
				style="background-color:#ed5565;color:#FFF;_-1"></t:gridCol>
			<t:gridCol title="普货/危货" field="generalDanger" replace="普货_1,危货_2" width="40" align="center"></t:gridCol>
			<t:gridCol title="登记日期" field="registerDate" dateFormat="yyyy-MM-dd"></t:gridCol>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function selectRecord(data) {
		var ids = "${param.ids}";
		if (ids != "" && ids != null && ids != undefined) {
			var idArray = ids.split(",");
			for (i = 0; i < idArray.length; i++) {
				$("#vehicleSelectList").datagrid("selectRecord", idArray[i]);
			}
		}
	}
</script>
