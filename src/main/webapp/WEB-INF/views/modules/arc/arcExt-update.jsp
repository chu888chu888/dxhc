<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/views/common/tags.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>${fne:getConfig('title')}</title>
    <meta name="author" content="${fne:getConfig('author')}">
    <meta name="keywords" content="${fne:getConfig('keywords')}">
    <meta name="description" content="${fne:getConfig('description')}">
    <t:base></t:base>
</head>
<body>
<t:form id="arcExtUpdate" action="arcExtController.do?doUpdate">
    <input name="id" type="hidden" value="${arcExtPage.id }">
    <input name="type" type="hidden" value="${arcExtPage.type}"/>
    <input name="tenantId" type="hidden" value="${arcExtPage.tenantId}"/>
    <table style="width: 500px;" cellpadding="0" cellspacing="1" class="formtable">
        <tr>
            <td align="right"><label class="Validform_label">名称:</label></td>
            <td class="value"><input id="name" name="name" type="text" class="inputxt" datatype="*"
                                     value="${arcExtPage.name}"/></td>
        </tr>
        <tr>
            <td align="right"><label class="Validform_label">应用范围:</label></td>
            <td class="value"><select name="type" id="type" disabled="disabled">
                <option value="1" <c:if test="${arcExtPage.type eq 1}">selected="selected"</c:if>>车辆档案</option>
                <option value="2" <c:if test="${arcExtPage.type eq 2}">selected="selected"</c:if>>责任人档案</option>
                <option value="3" <c:if test="${arcExtPage.type eq 3}">selected="selected"</c:if>>司机档案</option>
            </select></td>
        </tr>
    </table>
</t:form>
</body>
</html>