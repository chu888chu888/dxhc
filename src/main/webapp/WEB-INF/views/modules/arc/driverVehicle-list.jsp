<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:grid name="driverVehicleList" url="driverController.do?driverVehicleList&driverId=${driverId}">
	<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
	<t:gridCol title="编号" field="sn"></t:gridCol>
	<t:gridCol title="车牌" field="plateNumber"></t:gridCol>
	<t:gridCol title="类型" field="type" width="40" replace="牵引车_1,挂车_2,整车_3" align="center"></t:gridCol>
	<t:gridCol title="状态" field="status" width="40" dictGroup="vehicleStatus" dictExt="注销_-1" align="center"
		style="background-color:#ed5565;color:#FFF;_-1"></t:gridCol>
	<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
	<t:gridOpt title="取消" style="btn_red" funname="cancelBind(id,{'${driverId}'})" />
	<t:gridBar title="添加" icon="icon-add" url="driverController.do?goBind&driverId=${driverId}" funname="add" width="800"
		height="459"></t:gridBar>
</t:grid>
<script type="text/javascript">
	function cancelBind(vehicleId, driverId) {
		var url = 'driverController.do?doCancelBind&vehicleId=' + vehicleId
				+ '&driverId=' + driverId;
		$.dialog.setting.zIndex = getzIndex(true);
		layer.open({
			title : '取消确认',
			content : '确定取消 ?',
			icon : 7,
			yes : function(index) {
				doSubmit(url, 'driverVehicleList');
			},
			btn : [ '确定', '取消' ]
		});
	}
</script>
