<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						//初始化
						var driverId = "${driverId}";
						var idArry = driverId.split(",");
						var driverName = "${driverName}";
						var nameArry = driverName.split(",");
						for (var i = 0; i < idArry.length; i++) {
							if (idArry[i] != "") {
								var newRow = '<tr><td><input type="hidden" value='+idArry[i]+' id="id"><span>'
										+ nameArry[i]
										+ '</span></td><td><a href="#" onclick="del(this)">移除</a></td></tr>';
								$("#driverTable").append(newRow);
							}
						}
						//去除查询、 重置按钮
						$("#driverSelectListtb").find(
								"div[name='searchColums']").find(
								"form#driverSelectListForm").find("#queryBtn")
								.remove();
						$("#driverSelectListtb").find(
								"div[name='searchColums']").find(
								"form#driverSelectListForm").find("#resetBtn")
								.remove();
						//添加全部选择按钮
						$("#driverSelectListtb").find(
								"div[name='searchColums']").find(
								"form#driverSelectListForm").append(
								$("#tempSearchColums div[name='searchColums']")
										.html());
						$("#tempSearchColums").html('');
					});
</script>
<div class="easyui-layout" style="width: 900px; height: 460px;">
	<div data-options="region:'center'">
		<t:grid name="driverSelectList" url="driverController.do?list" onClick="select" checkbox="false" pagination="false">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="姓名" field="name" width="25" query="true"></t:gridCol>
			<t:gridCol title="性别" field="sex" width="15" replace="男_1,女_2" align="center"></t:gridCol>
			<t:gridCol title="手机" field="phone" width="30" query="true"></t:gridCol>
			<t:gridCol title="身份证" field="idCard" width="45"></t:gridCol>
			<t:gridCol title="地址" field="address"></t:gridCol>
		</t:grid>
	</div>
	<div data-options="region:'east',split:true" title="已选择" style="width: 200px;">
		<table class="table-list" cellspacing="0" id="driverTable">
			<thead>
				<tr>
					<th width="100">姓名</th>
					<th><a href="javascript:deleteAll()">移除全部</a></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="float: right; padding-right: 5px;">
			<button id="queryBtn" class="btn btn-info" onclick="driverSelectListSearch()" type="button">查询</button>
			<button id="selectAllBtn" class="btn btn-primary" onclick="selectAll()" type="button">选择全部 >></button>
		</span>
	</div>
</div>
<script type="text/javascript">
	function select() {
		var rows = $("#driverSelectList").datagrid("getChecked");
		if (rows.length >= 1) {
			for (var i = 0; i < rows.length; i++) {
				var rowsName = rows[i]['name'];
				var rowsId = rows[i]['id'];
				if (check(rowsId)) {
					var newRow = '<tr><td><input type="hidden" value='+rowsId+' id="id"><span>'
							+ rowsName
							+ '</span></td><td><a href="#" onclick="del(this)">移除</a></td></tr>';
					$("#driverTable").append(newRow);
				}
			}

		}
	}
	function selectAll() {
		var rows = $("#driverSelectList").datagrid("getRows");
		if (rows.length >= 1) {
			for (var i = 0; i < rows.length; i++) {
				var rowsName = rows[i]['name'];
				var rowsId = rows[i]['id'];
				if (check(rowsId)) {
					var newRow = '<tr><td><input type="hidden" value='+rowsId+' id="id"><span>'
							+ rowsName
							+ '</span></td><td><a href="#" onclick="del(this)">移除</a></td></tr>';
					$("#driverTable").append(newRow);
				}
			}
		}
	}
	function check(rowsId) {
		var flag = true;
		$("#driverTable").find("tbody").find("tr").each(function() {
			if ($(this).find("input").val() == rowsId) {
				flag = false;
				return false;
			}
		});
		return flag;
	}

	function del(row) {
		var tr = row.parentNode.parentNode;
		var tbody = tr.parentNode;
		tbody.removeChild(tr);
	}

	function deleteAll() {
		$("#driverTable tbody").html("");
	}
</script>