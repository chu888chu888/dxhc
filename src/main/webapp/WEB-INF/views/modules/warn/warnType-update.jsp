<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="warnTypeUpdate" action="warnTypeController.do?doUpdate">
		<input name="id" type="hidden" value="${warnTypePage.id }">
		<input name="tenantId" type="hidden" value="${warnTypePage.tenantId}" />
		<table style="width: 500px;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">名称:</label></td>
				<td class="value"><input name="name" type="text" class="inputxt" datatype="*" value="${warnTypePage.name}"
					nullmsg="请填写名称" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">状态:</label></td>
				<td class="value"><select name="status" datatype="*" nullmsg="请填写状态">
						<option value="0" <c:if test="${warnTypePage.status eq 0}">selected="selected"</c:if>>启用</option>
						<option value="1" <c:if test="${warnTypePage.status eq 1}">selected="selected"</c:if>>禁用</option>
				</select>
			</tr>
		</table>
	</t:form>
</body>
</html>