<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeWarnSms" action="incomeWarnController.do?doSendSms">
		<input name="id" type="hidden" value="${incomePage.id }">
		<input name="vehicle.plateNumber" type="hidden" value="${incomePage.vehicle.plateNumber}" />
		<input name="expenses.name" type="hidden" value="${incomePage.expenses.name}" />
		<input name="endDate" type="hidden"
			value='<fmt:formatDate value='${incomePage.endDate}' type="date" pattern="yyyy-MM-dd"/>'>
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="ownerName" type="text" class="inputxt" value="${incomePage.vehicle.owner.name}"
					readonly="readonly" /></td>
				<td align="right"><label class="Validform_label">手机:</label></td>
				<td class="value"><input name="ownerPhone" type="text" class="inputxt"
					value="${incomePage.vehicle.owner.phone}" readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">短信内容:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="content" readonly="readonly"
						style="width: 490px; height: 120px;">您的机动车${incomePage.vehicle.plateNumber},提醒类型-${incomePage.expenses.name},到期日期:${fne:getDate(incomePage.endDate)},请及时处理。</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>