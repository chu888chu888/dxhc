<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script>
	$(function() {
		$("#loanWarnListtb").find("div[name='searchColums']").find(
				"form#loanWarnListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
		//事件绑定
		$("input[name='endDate_begin']").attr("onchange",
				"endDateBeginChange()");
		$("input[name='endDate_end']").attr("onchange", "endDateEndChange()");
		$("input[name='sn']").attr("onchange", "snOwnerChange()");
		$("input[name='loanContract.owner.name']").attr("onchange",
				"snOwnerChange()");
		$("select[name='status']").attr("onchange", "statusChange()");
	});
</script>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="loanWarnList" url="loanWarnController.do?list" sortName="createDate">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="loanContractId" field="loanContract.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="loanContract.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="sn" query="true" formatter="loanContractFormat" width="70"></t:gridCol>
			<t:gridCol title="责任人" field="loanContract.owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="手机" field="loanContract.owner.phone"></t:gridCol>
			<t:gridCol title="应还本金 " field="principal" align="right"></t:gridCol>
			<t:gridCol title="应还利息 " field="interest" align="right"></t:gridCol>
			<t:gridCol title="应还总额 " field="totalAmount" align="right"></t:gridCol>
			<t:gridCol title="实还本金 " field="realPrincipal" align="right"></t:gridCol>
			<t:gridCol title="实还利息 " field="realInterest" align="right"></t:gridCol>
			<t:gridCol title="实还总额 " field="realTotalAmount" align="right"></t:gridCol>
			<t:gridCol title="尚欠金额" field="oweAmount" align="right"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="未还款_0,还款中_1,已还完_2"
				style="background-color:#f8ac59;color:#FFF;_1,background-color:#1ab394;color:#FFF;_2" align="center" width="40"></t:gridCol>
			<t:gridCol title="还款日" field="endDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group" style="color:red"></t:gridCol>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="短信" funname="send(id)" style="btn_green" />
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><select id="warnDateType" name="warnDateType"
			style="width: 104px" onchange="warnDateTypeChange()">
				<option value="1">下月到期</option>
				<option value="2">自定义</option>
		</select></span>
	</div>
</div>
<script type="text/javascript">
	function snOwnerChange() {
		var sn = $("input[name='sn']").val();
		var owner = $("input[name='loanContract.owner.name']").val();
		if (isNotBlank(sn) || isNotBlank(owner)) {
			$("#warnDateType").val('2')
		}
	}

	function statusChange() {
		$("#warnDateType").val('2')
	}

	function endDateBeginChange() {
		var warnDateBegin = $("input[name='endDate_begin']").val();
		if (warnDateBegin.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function endDateEndChange() {
		var warnDateEnd = $("input[name='endDate_end']").val();
		if (warnDateEnd.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function warnDateTypeChange() {
		var warnDateType = $("#warnDateType").val();
		if (warnDateType == '1') {
			$("input[name='endDate_begin']").val('');
			$("input[name='endDate_end']").val('');
			loanWarnListSearch();
		}
	}
</script>
<script type="text/javascript">
	function loanContractFormat(value, rec, index) {
		var loanContractId = rec["loanContract.id"];
		if (loanContractId != undefined) {
			return '<a href=\'#\' onclick=loanContractDetail(\''
					+ loanContractId + '\')>' + value + '</a>';
		}
	}

	function loanContractDetail(id) {
		openwindow('查看', 'loanContractController.do?goDetail&id=' + id,
				'loanList', 800, 700)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["loanContract.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'loanWarnList', 800, 500)
	}

	function send(id) {
		createAddUpdateWindow('发送短信', 'loanWarnList',
				'loanWarnController.do?goSendSms&id=' + id, 800, 500);
	}

	function isNotBlank(value) {
		if (value != '' && value != null && value != undefined) {
			return true;
		}
		return false;
	}
</script>

