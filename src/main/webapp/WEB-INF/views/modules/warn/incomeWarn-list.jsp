<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script>
	$(function() {
		$("#incomeWarnListtb").find("div[name='searchColums']").find(
				"form#incomeWarnListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
		//事件绑定
		$("input[name='endDate_begin']").attr("onchange",
				"endDateBeginChange()");
		$("input[name='endDate_end']").attr("onchange", "endDateEndChange()");
		$("input[name='vehicle.sn']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("input[name='vehicle.plateNumber']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("input[name='vehicle.owner.name']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("select[name='exs']").attr("onchange", "exsChange()");
	});
</script>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="incomeWarnList" url="incomeWarnController.do?list" sortName="createDate">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="incomeSumId" field="incomeSum.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true" width="40"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat" width="50"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" width="40" align="center"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true" formatter="ownerNameFormat" width="40"></t:gridCol>
			<t:gridCol title="手机" field="vehicle.owner.phone" width="50"></t:gridCol>
			<t:gridCol title="费用类型" field="expenses.name" width="50"></t:gridCol>
			<t:gridCol title="金额" field="incomeAmount" width="40" align="right"></t:gridCol>
			<t:gridCol title="状态" field="status" replace="未核销_0,已核销_1" style="background-color:#1ab394;color:#FFF;_1"
				align="center" width="30"></t:gridCol>
			<t:gridCol title="备注" field="remarks"></t:gridCol>
			<t:gridCol title="到期日期" field="endDate" width="40" dateFormat="yyyy-MM-dd" query="true" queryMode="group"
				style="color:red"></t:gridCol>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="短信" funname="send(id)" style="btn_green" />
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><select id="warnDateType" name="warnDateType"
			style="width: 90px" onchange="warnDateTypeChange()">
				<option value="1">下月到期</option>
				<option value="2">自定义</option>
		</select></span> <span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 70px; text-align: right; text-overflow: ellipsis;"
			title="费用类型">费用类型:</span><select name="exs" style="width: 104px">
				<option value="">---请选择---</option>
				<c:forEach items="${expensesList}" var="expenses">
					<option value="${expenses.id}">${expenses.name}</option>
				</c:forEach>
		</select></span>
	</div>
</div>
<script type="text/javascript">
	function snPlateNumberOwnerChange() {
		var sn = $("input[name='vehicle.sn']").val();
		var plateNumber = $("input[name='vehicle.plateNumber']").val();
		var owner = $("input[name='vehicle.owner.name']").val();
		if (isNotBlank(sn) || isNotBlank(plateNumber) || isNotBlank(owner)) {
			$("#warnDateType").val('2')
			$("select[name='ws']").val('-1');
		}
	}

	function exsChange() {
		$("#warnDateType").val('2')
	}

	function endDateBeginChange() {
		var warnDateBegin = $("input[name='endDate_begin']").val();
		if (warnDateBegin.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function endDateEndChange() {
		var warnDateEnd = $("input[name='endDate_end']").val();
		if (warnDateEnd.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function warnDateTypeChange() {
		var warnDateType = $("#warnDateType").val();
		if (warnDateType == '1') {
			$("input[name='endDate_begin']").val('');
			$("input[name='endDate_end']").val('');
			insuranceWarnListSearch();
		}
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		if (vehicleId != undefined) {
			return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId
					+ '\')>' + value + '</a>';
		}
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'incomeList', 800, 580)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id, 'incomeList',
				800, 500)
	}

	function send(id) {
		createAddUpdateWindow('发送短信', 'incomeWarnList',
				'incomeWarnController.do?goSendSms&id=' + id, 800, 500);
	}

	function isNotBlank(value) {
		if (value != '' && value != null && value != undefined) {
			return true;
		}
		return false;
	}
</script>
