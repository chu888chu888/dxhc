<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="vehicleWarnHisList" url="vehicleWarnController.do?hisList" checkbox="false">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" width="40" align="center"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true"></t:gridCol>
			<t:gridCol title="手机" field="vehicle.owner.phone"></t:gridCol>
			<t:gridCol title="提醒类型" field="warnType.name"></t:gridCol>
			<t:gridCol title="办理前" field="preWarnDate" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridCol title="办理后" field="afterWarnDate" dateFormat="yyyy-MM-dd"></t:gridCol>
			<t:gridCol title="办理人" field="createBy" query="true"></t:gridCol>
			<t:gridCol title="办理日期" field="createDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group"></t:gridCol>
		</t:grid>
	</div>
</div>
