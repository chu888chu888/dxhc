<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<script type="text/javascript">
	function addClick() {
		var tr = $("#warnListTable_template tr").clone();
		$("#warnListTable").append(tr);
		resetTrNum('warnListTable');
	}

	function delClick() {
		$("#warnListTable").find("input:checked").parent().parent().remove();
		resetTrNum('warnListTable');
	}

	function resetTrNum(tableId) {
		$tbody = $("#" + tableId + "");
		$tbody.find('>tr').each(function(i) {
			$(':input, select,button,a', this).each(function() {
				var $this = $(this);
				var name = $this.attr('name');
				if (name != null) {
					if (name.indexOf("#index#") >= 0) {
						$this.attr("name", name.replace('#index#', i));
					} else {
						var s = name.indexOf("[");
						var e = name.indexOf("]");
						var new_name = name.substring(s + 1, e);
						$this.attr("name", name.replace(new_name, i));
					}
				}
			});
			$(this).find('div[name=\'xh\']').html(i + 1);
		});
	}
</script>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<div style="height: 30px;">
		<a id="addBtn" onclick="addClick()" style="cursor: pointer; margin-left: 10px;"><img alt="增加"
			src="${ctx}/plug-in/easyui/icons/add.png"></a><a id="delBtn" onclick="delClick()"
			style="cursor: pointer; margin-left: 25px;"><img alt="删除" src="${ctx}/plug-in/easyui/icons/minus.png"></a> <img
			style="margin-left: 650px;" title="该功能用于车辆到期提醒.如合同到期，二维到期等" src="${ctx}/plug-in/easyui/icons/tip.png">
	</div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="center" bgcolor="#EEEEEE" style="width: 35px;"><label class="Validform_label">序号</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 50px;"><label class="Validform_label">操作</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 200px;"><label class="Validform_label">提醒类型</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 200px;"><label class="Validform_label">到期日期</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 300px;"></td>
		</tr>
		<tbody id="warnListTable">
			<tr>
				<td align="center"><div style="width: 35px;" name="xh">1</div></td>
				<td align="center"><input style="width: 50px;" type="checkbox" name="ck" /><input name="warnList[0].id"
					type="hidden" /></td>
				<td align="center"><select name="warnList[0].warnType.id" datatype="*" nullmsg="请填写提醒类型" style="width: 180px;"><c:forEach
							items="${warnTypeList}" var="warnType">
							<option value="${warnType.id}">${warnType.name}</option>
						</c:forEach>
				</select></td>
				<td align="center"><input name="warnList[0].warnDate" type="text" class="Wdate" onClick="WdatePicker()"
					datatype="date" nullmsg="请填写到期日期" style="width: 180px;"></td>
			</tr>
		</tbody>
	</table>
</div>
