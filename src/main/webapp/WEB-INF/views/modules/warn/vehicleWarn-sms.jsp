<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="vehicleWarnSms" action="vehicleWarnController.do?doSendSms">
		<input name="id" type="hidden" value="${vehicleWarnPage.id }">
		<input name="plateNumber" type="hidden" value="${vehicleWarnPage.vehicle.plateNumber}" />
		<input name="warnTypeName" type="hidden" value="${vehicleWarnPage.warnType.name}" />
		<input name="warnDate" type="hidden"
			value='<fmt:formatDate value='${vehicleWarnPage.warnDate}' type="date" pattern="yyyy-MM-dd"/>'>
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="ownerName" type="text" class="inputxt"
					value="${vehicleWarnPage.vehicle.owner.name}" readonly="readonly" /></td>
				<td align="right"><label class="Validform_label">手机:</label></td>
				<td class="value"><input name="ownerPhone" type="text" class="inputxt"
					value="${vehicleWarnPage.vehicle.owner.phone}" readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">短信内容:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="content" readonly="readonly"
						style="width: 550px; height: 120px;">您的机动车${vehicleWarnPage.vehicle.plateNumber},提醒类型-${vehicleWarnPage.warnType.name},到期日期:${fne:getDate(vehicleWarnPage.warnDate)},请及时处理。</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>