<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script>
	$(function() {
		//初始化
		$("#vehicleWarnListtb").find("div[name='searchColums']").find(
				"form#vehicleWarnListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
		//事件绑定
		$("input[name='warnDate_begin']").attr("onchange",
				"warnDateBeginChange()");
		$("input[name='warnDate_end']").attr("onchange", "warnDateEndChange()");
		$("input[name='vehicle.sn']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("input[name='vehicle.plateNumber']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("input[name='vehicle.owner.name']").attr("onchange",
				"snPlateNumberOwnerChange()");
		$("select[name='wt']").attr("onchange", "warnTypeChange()");
	});
</script>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="vehicleWarnList" url="vehicleWarnController.do?list" sortName="warnDate" sortOrder="asc">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" width="40" align="center"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true" formatter="ownerNameFormat" sortable="false"></t:gridCol>
			<t:gridCol title="手机" field="vehicle.owner.phone" sortable="false"></t:gridCol>
			<t:gridCol title="提醒类型" field="warnType.name"></t:gridCol>
			<t:gridCol title="到期日期" field="warnDate" dateFormat="yyyy-MM-dd" query="true" queryMode="group" style="color:red"></t:gridCol>
			<t:gridCol title="提醒状态" field="status" replace="正常_0,暂停_1" width="40" align="center"
				style="background-color:#ed5565;color:#FFF;_1"></t:gridCol>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="办理" funname="handle(id)" style="btn_blue" />
			<t:gridOpt title="短信" funname="send(id)" style="btn_green" />
			<t:gridBar title="增加" icon="icon-add" url="vehicleWarnController.do?goAdd" funname="add"></t:gridBar>
			<t:gridBar title="启动提醒" icon="icon-run" url="vehicleWarnController.do?doStart" funname="start"></t:gridBar>
			<t:gridBar title="暂停提醒" icon="icon-pause" url="vehicleWarnController.do?doPause" funname="pause"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="vehicleWarnController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="导出" icon="icon-xls" funname="xlsExport('vehicleWarnController.do?xlsExport', 'vehicleWarnList')"></t:gridBar>
			<t:gridBar title="办理记录" icon="icon-record" funname="record"></t:gridBar>
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><select id="warnDateType" name="warnDateType"
			style="width: 90px" onchange="warnDateTypeChange()">
				<option value="1">下月到期</option>
				<option value="2">自定义</option>
		</select></span> <span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 70px; text-align: right; text-overflow: ellipsis;"
			title="提醒类型 ">提醒类型:</span><select name="wt" style="width: 104px">
				<option value="">---请选择---</option>
				<c:forEach items="${warnTypeList}" var="warn">
					<option value="${warn.id}">${warn.name}</option>
				</c:forEach>
		</select></span><span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 70px; text-align: right; text-overflow: ellipsis;"
			title="状态">提醒状态:</span><select name="ws" style="width: 104px">
				<option value="0">正常</option>
				<option value="1">暂停</option>
				<option value="-1">全部</option>
		</select></span>
	</div>
</div>

<script type="text/javascript">
	function snPlateNumberOwnerChange() {
		var sn = $("input[name='vehicle.sn']").val();
		var plateNumber = $("input[name='vehicle.plateNumber']").val();
		var owner = $("input[name='vehicle.owner.name']").val();
		if (isNotBlank(sn) || isNotBlank(plateNumber) || isNotBlank(owner)) {
			$("#warnDateType").val('2')
			$("select[name='ws']").val('-1');
		}
	}

	function warnTypeChange() {
		$("#warnDateType").val('2')
	}

	function warnDateBeginChange() {
		var warnDateBegin = $("input[name='warnDate_begin']").val();
		if (warnDateBegin.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function warnDateEndChange() {
		var warnDateEnd = $("input[name='warnDate_end']").val();
		if (warnDateEnd.length != 0) {
			$("#warnDateType").val('2')
		}
	}

	function warnDateTypeChange() {
		var warnDateType = $("#warnDateType").val();
		if (warnDateType == '1') {
			$("input[name='warnDate_begin']").val('');
			$("input[name='warnDate_end']").val('');
			vehicleWarnListSearch();
		}
	}

	function handle(id) {
		createAddUpdateWindow('办理', 'vehicleWarnList',
				'vehicleWarnController.do?goHandle&id=' + id, 800, 500);
	}

	function send(id) {
		createAddUpdateWindow('发送短信', 'vehicleWarnList',
				'vehicleWarnController.do?goSendSms&id=' + id, 800, 500);
	}

	function start() {
		var rows = $('#vehicleWarnList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择启动提醒');
			return;
		}
		createDialog('启动确认', '确定启动提醒 ?', 'vehicleWarnController.do?doStart&id='
				+ rows[0].id, 'vehicleWarnList');
	}

	function pause() {
		var rows = $('#vehicleWarnList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择暂停提醒');
			return;
		}
		createDialog('暂停确认', '确定暂停提醒 ?', 'vehicleWarnController.do?doPause&id='
				+ rows[0].id, 'vehicleWarnList');
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId + '\')>'
				+ value + '</a>';
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'vehicleWarnList', 800, 580)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
				+ value + '</a>';
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'vehicleWarnList', 800, 500)
	}

	function record() {
		addTab('办理记录', 'vehicleWarnController.do?hisIndex');
	}

	function isNotBlank(value) {
		if (value != '' && value != null && value != undefined) {
			return true;
		}
		return false;
	}
</script>
