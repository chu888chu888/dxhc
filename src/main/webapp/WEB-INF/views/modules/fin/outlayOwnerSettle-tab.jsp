<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="left" bgcolor="#EEEEEE" width="30px;"><label class="Validform_label">序号</label></td>
			<td align="left" bgcolor="#EEEEEE" width="40px"><label class="Validform_label">操作</label></td>
			<td align="center" bgcolor="#EEEEEE" width="100px"><label class="Validform_label">车牌</label></td>
			<td align="center" bgcolor="#EEEEEE" width="100px"><label class="Validform_label">费用类型</label></td>
			<td align="center" bgcolor="#EEEEEE" width="100px"><label class="Validform_label">应付金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="100px"><label class="Validform_label">核销金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="250px"><label class="Validform_label">备注</label></td>
		</tr>
		<tbody id="outlayOwnerSettleTable">
			<c:forEach items="${outlayList}" var="outlay" varStatus="stuts">
				<tr>
					<input name="outlayList[${stuts.index }].id" type="hidden" value="${outlay.id}">
					<input name="outlayList[${stuts.index }].tenantId" type="hidden" value="${outlay.tenantId}">
					<input name="outlayList[${stuts.index }].vehicle.id" type="hidden" value="${outlay.vehicle.id}">
					<input name="outlayList[${stuts.index }].expenses.id" type="hidden" value="${outlay.expenses.id}">
					<input name="outlayList[${stuts.index }].outlayAmount" type="hidden" value="${outlay.outlayAmount}">
					<input name="outlayList[${stuts.index }].createBy" type="hidden" value="${outlay.createBy}">
					<input name="outlayList[${stuts.index }].createDate" type="hidden" value="${outlay.createDate}">
					<td align="center"><div style="width: 30px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center"><input style="width: 40px;" type="checkbox" name="ck${stuts.index }" checked="checked"
						onchange="checkboxChange(${stuts.index})" /><input name="outlayList[${stuts.index }].status" type="hidden"
						value="1"></td>
					<td align="center" width="100px"><c:out value="${outlay.vehicle.sn}-${outlay.vehicle.plateNumber}"></c:out></td>
					<td align="center" width="100px"><c:out value="${outlay.expenses.name}"></c:out></td>
					<td align="center" width="100px"><c:out value="${outlay.outlayAmount}"></c:out></td>
					<td align="center"><input name="outlayList[${stuts.index }].settleAmount" type="text" class="inputxt"
						style="width: 100px" datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/"
						ignore="ignore" onchange="settleAmountChange(${stuts.index});" value="${outlay.outlayAmount}"></td>
					<td align="center"><input name="outlayList[${stuts.index }].remarks" type="text" class="inputxt"
						style="width: 250px" value="${outlay.remarks}"></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="3"></td>
				<td style="font-size: 14px; font-weight: 700;" align="center">总计:</td>
				<td id="totalOutlayAmount" style="color: red; font-size: 14px; font-weight: 700;" align="center">${totalOutlayAmount}</td>
				<td id="totalSettleAmount" style="color: red; font-size: 16px; font-weight: 700;">${totalSettleAmount}</td>
			</tr>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	function checkboxChange(num){
		if($("#outlayOwnerSettleTable").find("input[name='ck" + num+ "']").is(':checked')){
			var outlayAmount = $("#outlayOwnerSettleTable").find("input[name='outlayList[" + num+ "].outlayAmount']").val();
			$("#outlayOwnerSettleTable").find("input[name='outlayList[" + num+ "].settleAmount']").val(outlayAmount);
			$("#outlayOwnerSettleTable").find("input[name='outlayList[" + num+ "].status']").val(1);
		}else{
			$("#outlayOwnerSettleTable").find("input[name='outlayList[" + num+ "].settleAmount']").val("");
			$("#outlayOwnerSettleTable").find("input[name='outlayList[" + num+ "].status']").val(0);
		}
		//核销总金额
		var totalSettleAmount=0;
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#outlayOwnerSettleTable").find('>tr').each(function(i){
			if($("#outlayOwnerSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('settleAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalSettleAmount = addAmount(totalSettleAmount,val);
							}
						}	
				});
			}
		});
		$("#totalSettleAmount").html(totalSettleAmount);
	}

	function settleAmountChange(num) {
		//计算优惠总额,核销总金额
		var totaloutlayAmount=0;
		var totalSettleAmount=0;
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#outlayOwnerSettleTable").find('>tr').each(function(i){
			if($("#outlayOwnerSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('outlayAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totaloutlayAmount = addAmount(totaloutlayAmount,val);
							}
						}	
						if(name!=null && name.indexOf('settleAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalSettleAmount = addAmount(totalSettleAmount,val);
							}
						}	
				});
			}
		});
		$("#totalDiscountAmount").html(subAmount(totalSettleAmount,totaloutlayAmount));
		$("#totalSettleAmount").html(totalSettleAmount);
	}
	
	
	function addAmount(arg1, arg2){
		var tem = arg1*10+arg2*10;
		tem = tem/10;
		return tem;
	}
	
	function subAmount(arg1, arg2) {
	    var tem = arg2*10-arg1*10;
	    tem = tem/10;
	    return tem;
	}
</script>

