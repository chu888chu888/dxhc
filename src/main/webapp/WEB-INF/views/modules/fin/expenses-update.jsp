<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="expensesUpdate" action="expensesController.do?doUpdate">
		<input name="id" type="hidden" value="${expensesPage.id }">
		<input name="tenantId" type="hidden" value="${expensesPage.tenantId}">
		<input name="type" type="hidden" value="${expensesPage.type}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">名称:</label></td>
				<td class="value"><input name="name" type="text" class="inputxt" datatype="*" nullmsg="请填写名称"
					value=" ${expensesPage.name}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">费用标准:</label></td>
				<td class="value"><input name="amount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写费用标准"
					errormsg="请填写正确金额" ignore="ignore" value="${expensesPage.amount}"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">应用范围:</label></td>
				<td class="value"><select name="type" datatype="*" nullmsg="请填写应用范围" disabled="disabled">
						<option value="1" <c:if test="${expensesPage.type eq 1}">selected="selected"</c:if>>收车辆款</option>
						<option value="2" <c:if test="${expensesPage.type eq 2}">selected="selected"</c:if>>付车辆款</option>
						<option value="3" <c:if test="${expensesPage.type eq 3}">selected="selected"</c:if>>收单位款</option>
						<option value="4" <c:if test="${expensesPage.type eq 4}">selected="selected"</c:if>>付单位款</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">状态:</label></td>
				<td class="value"><select name="status" datatype="*" nullmsg="请填写应用状态">
						<option value="0" <c:if test="${expensesPage.status eq 0}">selected="selected"</c:if>>启用</option>
						<option value="1" <c:if test="${expensesPage.status eq 1}">selected="selected"</c:if>>禁用</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">排序:</label></td>
				<td class="value"><input name="sort" type="text" class="inputxt" datatype="n1-3" value="${expensesPage.sort}"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>