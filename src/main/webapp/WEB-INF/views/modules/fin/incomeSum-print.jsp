<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<style type="text/css">
* {
	padding: 0;
	margin: 0;
}

.print_container {
	margin: 0;
	padding: 0px;
	width: 240px;
	height: 410px;
}

a {
	text-decoration: none
}

a:hover {
	color: #DDDDDD !important;
}

.btn_blue {
	background-color: #1c84c6;
	border-color: #1c84c6;
	color: #FFFFFF;
	border-radius: 3px;
	padding: 1px 5px;
	font-size: 16px;
	line-height: 1.5;
	display: inline-block;
}
</style>
<script src="${ctx}/plug-in/jquery/jquery-1.8.3.min.js"></script>
<script src="${ctx}/plug-in/jquery/jquery.jqprint.js"></script>
<script type="text/javascript">
	function print() {
		$(".print_container").jqprint();
	}
</script>
</head>
<body>
	<div align="center" style="margin-top: 15px; margin-bottom: 15px;">
		<a class="btn_blue" href="javascript:print();">打印</a>
	</div>
	<div class="print_container">
		<div>
			<h2>收款单</h2>
			<div>
				<span>${incomeSumPage.sn}</span>
			</div>
		</div>
		<span>************************</span>
		<div>
			<c:forEach items="${incomeSumPage.incomeList}" var="income">
				<div>
					<span>${income.vehicle.sn}-${income.vehicle.plateNumber}</span>
				</div>
				<div>
					<span>${income.expenses.name}</span>
				</div>
				<div>
					<span>${income.settleAmount}</span>
				</div>
				<span>--------------------</span>
			</c:forEach>
			<div>
				<span>总计</span>
			</div>
			<div>
				<span>${incomeSumPage.amount}</span>
			</div>
			<span>************************</span>
			<div>
				<span>责任人：${incomeSumPage.owner.name}</span>
			</div>
			<div>
				<span>收款人：${incomeSumPage.createBy}</span>
			</div>
			<span>--------------------</span>
			<div>
				<span>日期：${fne:getDate(incomeSumPage.createDate)}</span>
			</div>
			<div>
				<span>签章：</span>
			</div>
		</div>
	</div>

</body>
</html>