<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<script type="text/javascript">
	function expensesChange(val, num) {
		var url = "incomeController.do?expensesChange&expensesId=" + val;
		$.ajax({
			type : 'POST',
			url : url,
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.success) {
					$("#incomeListTable").find(
							"input[name='incomeList[" + num
									+ "].incomeAmount']").val(
							d.attributes.incomeAmount);
					calTotalIncomeAmount();
				}
			}
		});
	}

	function addClick() {
		var tr = $("#incomeListTable_template tr").clone();
		$("#incomeListTable").append(tr);
		resetTrNum('incomeListTable');
		calTotalIncomeAmount();
	}

	function delClick() {
		$("#incomeListTable").find("input:checked").parent().parent().remove();
		resetTrNum('incomeListTable');
		calTotalIncomeAmount();
	}

	function resetTrNum(tableId) {
		$tbody = $("#" + tableId + "");
		$tbody
				.find('>tr')
				.each(
						function(i) {
							$(':input, select,button,a', this)
									.each(
											function() {
												var $this = $(this);
												var name = $this.attr('name');
												var onchange_str = $this
														.attr('onchange');
												if (name != null) {
													if (name.indexOf("#index#") >= 0) {
														$this
																.attr(
																		"name",
																		name
																				.replace(
																						'#index#',
																						i));
													} else {
														var s = name
																.indexOf("[");
														var e = name
																.indexOf("]");
														var new_name = name
																.substring(
																		s + 1,
																		e);
														$this
																.attr(
																		"name",
																		name
																				.replace(
																						new_name,
																						i));
													}
												}
												if (name.indexOf('expenses') >= 0
														&& onchange_str != null) {
													if (onchange_str
															.indexOf("#index#") >= 0) {
														$this
																.attr(
																		"onchange",
																		onchange_str
																				.replace(
																						/#index#/g,
																						i));
													} else {
														onchange_str = 'expensesChange(this.options[this.options.selectedIndex].value,'
																+ i + ')';
														$this.attr("onchange",
																onchange_str);
													}
												}
											});
							$(this).find('div[name=\'xh\']').html(i + 1);
						});
	}
</script>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<div style="height: 30px;">
		<a id="addBtn" onclick="addClick()" style="cursor: pointer; margin-left: 10px;"><img alt="增加"
			src="${ctx}/plug-in/easyui/icons/add.png"></a><a id="delBtn" onclick="delClick()"
			style="cursor: pointer; margin-left: 25px;"><img alt="删除" src="${ctx}/plug-in/easyui/icons/minus.png"></a> <img
			style="margin-left: 650px;" title="该功能用于收取车辆费用.如管理费，代办费等" src="${ctx}/plug-in/easyui/icons/tip.png">
	</div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="center" bgcolor="#EEEEEE" style="width: 25px;"><label class="Validform_label">序号</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 35px;"><label class="Validform_label">操作</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 150px;"><label class="Validform_label">费用</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 120px;"><label class="Validform_label">应收金额</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 120px;"><label class="Validform_label">到期日期</label></td>
			<td align="center" bgcolor="#EEEEEE" style="width: 240px;"><label class="Validform_label">备注</label></td>
		</tr>
		<tbody id="incomeListTable">
			<tr>
				<td align="center"><div style="width: 25px;" name="xh">1</div></td>
				<td align="center"><input style="width: 40px;" type="checkbox" name="ck" /><input name="incomeList[0].id"
					type="hidden" /></td>
				<td align="center"><select name="incomeList[0].expenses.id"
					onchange="expensesChange(this.options[this.options.selectedIndex].value,0)" style="width: 150px;"><c:forEach
							items="${expensesList}" var="expenses">
							<option value="${expenses.id}">${expenses.name}</option>
						</c:forEach>
				</select></td>
				<td align="center"><input name="incomeList[0].incomeAmount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" errormsg="请填写正确金额"
					value="${incomeAmount}" style="width: 120px;" onchange="calTotalIncomeAmount()"></td>
				<td align="center"><input name="incomeList[0].endDate" type="text" class="Wdate" onClick="WdatePicker()"
					style="width: 120px;"></td>
				<td align="center"><input name="incomeList[0].remarks" type="text" class="inputxt" style="width: 240px;"></td>
			</tr>
		</tbody>
		<tr>
			<td colspan="2"></td>
			<td style="font-size: 14px; font-weight: 700;" align="center">总计:</td>
			<td id="totalIncomeAmount" style="color: red; font-size: 16px; font-weight: 700;" align="center">${incomeAmount}</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	function calTotalIncomeAmount() {
		var totalIncomeAmount = 0;
		var regExp = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#incomeListTable").find('>tr').each(function(i) {
			$(':input', this).each(function() {
				var $this = $(this);
				var name = $this.attr('name');
				if (name != null && name.indexOf('incomeAmount') >= 0) {
					var val = $this.val();
					if (regExp.test(val)) {
						totalIncomeAmount = addAmount(totalIncomeAmount, val);
					}
				}
			});
		});
		$("#totalIncomeAmount").html(totalIncomeAmount);
	}

	function addAmount(arg1, arg2) {
		var tem = arg1 * 10 + arg2 * 10;
		tem = tem / 10;
		return tem;
	}
</script>
