<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	function expensesChange() {
		var expensesId = $("#expenses").val();
		var url = "incomeController.do?expensesChange&expensesId=" + expensesId;
		$.ajax({
			type : 'POST',
			url : url,
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.success) {
					$("#incomeAmount").val(d.attributes.incomeAmount);
				}
			}
		});
	}
</script>
</head>
<body>
	<t:form id="incomeUpdate" action="incomeController.do?doUpdate">
		<input name="id" type="hidden" value="${incomePage.id }">
		<input name="tenantId" type="hidden" value="${incomePage.tenantId }">
		<input name="status" type="hidden" value="${incomePage.status }">
		<input name="discountAmount" type="hidden" value="${incomePage.discountAmount }">
		<input name="settleAmount" type="hidden" value="${incomePage.settleAmount }">
		<input name="createDate" type="hidden" value="${incomePage.createDate}">
		<input name="createBy" type="hidden" value="${incomePage.createBy}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicle.id" type="hidden" value="${incomePage.vehicle.id }" /> <input
					name="vehicle.plateNumber" id="vehiclePlateNumber" class="inputxt" readonly="readonly"
					onclick="choose_vehiclePlateNumber()" datatype="*" nullmsg="请填写车辆" value="${incomePage.vehicle.plateNumber }" /> <t:gridSelect
						nameId="vehiclePlateNumber" url="vehicleController.do?select" valueId="vehicleId" gridField="plateNumber"
						gridName="vehicleSelectList" title="车辆列表" height="460" width="800"></t:gridSelect></td>
				<td align="right"><label class="Validform_label">费用:</label></td>
				<td class="value"><select id="expenses" name="expenses.id" datatype="*" nullmsg="请填写费用"
					onchange="expensesChange()">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}" <c:if test="${incomePage.expenses.id eq expenses.id}">selected="selected"</c:if>>${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">应收金额:</label></td>
				<td class="value"><input id="incomeAmount" name="incomeAmount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写应收金额"
					errormsg="请填写正确金额" value="${incomePage.incomeAmount}"></td>
				<td align="right"><label class="Validform_label">到期日期:</label></td>
				<td class="value"><input name="endDate" type="text" class="Wdate"
					onClick="WdatePicker({minDate:'%y-%M-{%d+1}'})" nullmsg="请填写到期日期"
					value='<fmt:formatDate value='${incomePage.endDate}' type="date" pattern="yyyy-MM-dd"/>'></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><textarea class="inputxt" name="remarks" style="width: 510px; height: 40px;">${incomePage.remarks}</textarea></td>
			</tr>
		</table>
	</t:form>
</body>
</html>