<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="outlayVehicleSettle" action="outlayController.do?doSettle" tiptype="1">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input id="ownerId" name="owner.id" type="hidden" class="inputxt"
					value="${outlayPage.vehicle.owner.id}" /><input name="owner.name" type="hidden"
					value="${outlayPage.vehicle.owner.name}" /><input id="ownerName" name="owner.name" type="text" class="inputxt"
					value="${outlayPage.vehicle.owner.name}" readonly="readonly" /></td>
				<td align="right"><label class="Validform_label">车牌号:</label></td>
				<td class="value"><input id="plateNumber" name="plateNumber" type="text" class="inputxt"
					value="${outlayPage.vehicle.plateNumber}" readonly="readonly" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">付款方式:</label></td>
				<td class="value"><select name="way" id="way" datatype="*" nullmsg="请填写收款方式">
						<option value="1">现金</option>
						<option value="2">微信</option>
						<option value="3">支付宝</option>
						<option value="4">银行转账</option>
				</select></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input id="remarks" name="remarks" type="text" class="inputxt" /></td>
			</tr>
		</table>
		<div style="width: 800px; height: 400px;" id="outlayVehicleListTab">
			<div border="false" class="easyui-tabs" fit="true">
				<div title="付款明细" href="outlayController.do?vehicleSettleTab&vehicleId=${outlayPage.vehicle.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>