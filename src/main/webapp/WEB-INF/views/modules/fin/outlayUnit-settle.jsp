<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	//往来单位,费用类型 
	function settleChange() {
		var unit = $('#unit').val();
		var expensesId = $('#expensesId').val();
		refreshTab(unit, expensesId);
	}
	//刷新指定Tab的内容  
	function refreshTab(unit, expensesId) {
		if ($('#outlayUnitListTab').tabs('exists', '车辆明细')) {
			var currTab = $('#outlayUnitListTab').tabs('getTab', '车辆明细');
			$('#outlayUnitListTab')
					.tabs(
							'update',
							{
								tab : currTab,
								options : {
									id : 'outlayUnitListTab',
									title : '车辆明细',
									href : 'outlayUnitController.do?unitSettleTab&unit='
											+ unit
											+ '&expensesId='
											+ expensesId,
								}
							});
		}
	}
</script>
</head>
<body>
	<t:form id="outlayUnitSettle" action="outlayUnitController.do?doUnitSettle" tiptype="1">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">往来单位:</label></td>
				<td class="value"><t:dict id="unit" name="unit" type="select" groupCode="unit" datatype="*"
						onchange="settleChange();"></t:dict></td>
				<td align="right"><label class="Validform_label">费用类型:</label></td>
				<td class="value"><select name="expenses.id" id="expensesId" datatype="*" onchange="settleChange();">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}">${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">付款方式:</label></td>
				<td class="value"><select name="way" datatype="*" nullmsg="请填写付款方式">
						<option value="1">现金</option>
						<option value="2">银行转账</option>
						<option value="3">微信</option>
						<option value="4">支付宝</option>
						<option value="5">POS</option>
				</select></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" type="text" class="inputxt" /></td>
			</tr>
		</table>
		<div style="width: 795px; height: 450px;">
			<div id="outlayUnitListTab" border="false" class="easyui-tabs" fit="true">
				<div title="车辆明细" href="outlayUnitController.do?unitSettleTab"></div>
			</div>
		</div>
	</t:form>
</body>
</html>