<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="outlayUnitList" url="outlayUnitController.do?list" sortName="createDate" onDblClick="onDblClick">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="vehicleId" field="vehicle.id" hidden="true"></t:gridCol>
			<t:gridCol title="ownerId" field="vehicle.owner.id" hidden="true"></t:gridCol>
			<t:gridCol title="outlayUnitSumId" field="outlayUnitSum.id" hidden="true"></t:gridCol>
			<t:gridCol title="往来单位" field="unit" query="true" dictGroup="unit"></t:gridCol>
			<t:gridCol title="编号" field="vehicle.sn" query="true"></t:gridCol>
			<t:gridCol title="车牌" field="vehicle.plateNumber" query="true" formatter="plateNumberFormat"></t:gridCol>
			<t:gridCol title="车辆状态" field="vehicle.status" dictGroup="vehicleStatus" dictExt="注销_-1" align="center" width="40"></t:gridCol>
			<t:gridCol title="责任人" field="vehicle.owner.name" query="true" formatter="ownerNameFormat"></t:gridCol>
			<t:gridCol title="费用类型" field="expenses.name"></t:gridCol>
			<t:gridCol title="应付金额" field="amount" align="right"></t:gridCol>
			<t:gridCol title="状态" field="status" query="true" replace="未核销_0,已核销_1"
				style="background-color:#f8ac59;color:#FFF;_0" align="center" width="40"></t:gridCol>
			<t:gridCol title="核销编号" field="outlayUnitSum.sn" formatter="outlayUnitSumSnFormat"></t:gridCol>
			<t:gridBar title="登记" icon="icon-add" url="outlayUnitController.do?goAdd" funname="add" height="550"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="outlayUnitController.do?goUpdate" funname="update"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="outlayUnitController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="查看" icon="icon-detail" url="outlayUnitController.do?goDetail" funname="detail"></t:gridBar>
			<t:gridBar title="核销" icon="icon-amount" url="outlayUnitController.do?goUnitSettle" funname="add" height="550"></t:gridBar>
			<t:gridBar title="付款记录" icon="icon-record" funname="record"></t:gridBar>
			<t:gridCol title="操作" field="opt" width="40" align="center"></t:gridCol>
			<t:gridOpt title="核销" funname="settle(id)" style="btn_blue" exp="status#eq#0" />
		</t:grid>
	</div>
</div>
<div id="tempSearchColums" style="display: none;">
	<div name="searchColums">
		<span style="display: -moz-inline-box; display: inline-block;"><span
			style="vertical-align: middle; display: inline-block; width: 70px; text-align: right; text-overflow: ellipsis;"
			title="费用类型">费用类型:</span><select name="exs" style="width: 120px">
				<option value="">---请选择---</option>
				<c:forEach items="${expensesList}" var="expenses">
					<option value="${expenses.id}">${expenses.name}</option>
				</c:forEach>
		</select></span>
	</div>
</div>
<script>
	$(function() {
		$("#outlayUnitListtb").find("div[name='searchColums']").find(
				"form#outlayUnitListForm").append(
				$("#tempSearchColums div[name='searchColums']").html());
		$("#tempSearchColums").html('');
	});
</script>
<script type="text/javascript">
	function onDblClick() {
		detail('查看', 'outlayUnitController.do?goDetail', 'outlayUnitList', 800,
				500);
	}

	function update() {
		var rows = $('#outlayUnitList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('费用已核销,无法修改');
			return;
		}
		createAddUpdateWindow('修改', 'outlayUnitList',
				'outlayUnitController.do?goUpdate&id=' + rows[0].id, 800, 500);
	}

	function del() {
		var rows = $('#outlayUnitList').datagrid('getSelections');
		if (!rows || rows.length == 0) {
			tip('请选择项目');
			return;
		}
		if (rows[0].status == '1') {
			tip('费用已核销,无法删除');
			return;
		}
		createDialog('删除确认', '确定删除项目 ?', 'outlayUnitController.do?doDel&id='
				+ rows[0].id, 'outlayUnitList');
	}

	function settle(id) {
		createAddUpdateWindow('核销', 'outlayUnitList',
				'outlayUnitController.do?goVehicleSettle&id=' + id, 800, 500);
	}

	function record() {
		addTab('付单位款-付款记录', 'outlayUnitSumController.do?index');
	}

	function plateNumberFormat(value, rec, index) {
		var vehicleId = rec["vehicle.id"];
		if (vehicleId != undefined) {
			return '<a href=\'#\' onclick=vehicleDetail(\'' + vehicleId
					+ '\')>' + value + '</a>';
		}
	}

	function vehicleDetail(id) {
		openwindow('查看', 'vehicleController.do?goDetail&id=' + id,
				'outlayList', 800, 580)
	}

	function ownerNameFormat(value, rec, index) {
		var ownerId = rec["vehicle.owner.id"];
		if (ownerId != undefined) {
			return '<a href=\'#\' onclick=ownerDetail(\'' + ownerId + '\')>'
					+ value + '</a>';
		}
	}

	function ownerDetail(id) {
		openwindow('查看', 'ownerController.do?goDetail&id=' + id,
				'outlayUnitList', 800, 450)
	}

	function outlayUnitSumSnFormat(value, rec, index) {
		var outlayUnitSumId = rec["outlayUnitSum.id"];
		if (outlayUnitSumId != undefined) {
			return '<a href=\'#\' onclick=outlayUnitSumDetail(\''
					+ outlayUnitSumId + '\')>' + value + '</a>';
		}
	}

	function outlayUnitSumDetail(id) {
		openwindow('查看', 'outlayUnitSumController.do?goDetail&id=' + id,
				'outlayUnitList', 750, 600)
	}
</script>
