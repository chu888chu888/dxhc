<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="left" bgcolor="#EEEEEE" width="20px;"><label class="Validform_label">序号</label></td>
			<td align="left" bgcolor="#EEEEEE" width="40px"><label class="Validform_label">操作</label></td>
			<td align="center" bgcolor="#EEEEEE" width="90px"><label class="Validform_label">费用类型</label></td>
			<td align="center" bgcolor="#EEEEEE" width="90px"><label class="Validform_label">到期日期</label></td>
			<td align="center" bgcolor="#EEEEEE" width="90px"><label class="Validform_label">应收金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="80px"><label class="Validform_label">优惠金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="80px"><label class="Validform_label">核销金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="150px"><label class="Validform_label">备注</label></td>
		</tr>
		<tbody id="incomeVehicleSettleTable">
			<c:forEach items="${incomeList}" var="income" varStatus="stuts">
				<tr>
					<input name="incomeList[${stuts.index }].id" type="hidden" value="${income.id}">
					<input name="incomeList[${stuts.index }].tenantId" type="hidden" value="${income.tenantId}">
					<input name="incomeList[${stuts.index }].vehicle.id" type="hidden" value="${income.vehicle.id}">
					<input name="incomeList[${stuts.index }].expenses.id" type="hidden" value="${income.expenses.id}">
					<input name="incomeList[${stuts.index }].incomeAmount" type="hidden" value="${income.incomeAmount}">
					<input name="incomeList[${stuts.index }].endDate" type="hidden" value="${income.endDate}">
					<input name="incomeList[${stuts.index }].warn" type="hidden" value="${income.warn}">
					<input name="incomeList[${stuts.index }].createBy" type="hidden" value="${income.createBy}">
					<input name="incomeList[${stuts.index }].createDate" type="hidden" value="${income.createDate}">
					<td align="center"><div style="width: 20px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center"><input style="width: 40px;" type="checkbox" name="ck${stuts.index }" checked="checked"
						onchange="checkboxChange(${stuts.index})" /><input name="incomeList[${stuts.index }].status" type="hidden"
						value="1"></td>
					<td align="center" width="90px"><c:out value="${income.expenses.name}"></c:out></td>
					<td align="center" width="90px"><c:out value="${fne:getDate(income.endDate)}"></c:out></td>
					<td align="center" width="90px"><c:out value="${income.incomeAmount}"></c:out></td>
					<td align="center"><input name="incomeList[${stuts.index }].discountAmount" type="text" class="inputxt"
						style="width: 80px" datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/"
						ignore="ignore" onchange="discountAmountChange(${stuts.index})" value="0"></td>
					<td align="center"><input name="incomeList[${stuts.index }].settleAmount" type="text" class="inputxt"
						style="width: 80px" datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/"
						ignore="ignore" onchange="settleAmountChange(${stuts.index});" value="${income.incomeAmount}"></td>
					<td align="center"><input name="incomeList[${stuts.index }].remarks" type="text" class="inputxt"
						style="width: 150px" value="${income.remarks}"></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="3"></td>
				<td style="font-size: 14px; font-weight: 700;" align="center">总计:</td>
				<td id="totalIncomeAmount" style="color: red; font-size: 14px; font-weight: 700;" align="center">${totalIncomeAmount}</td>
				<td id="totalDiscountAmount" style="color: red; font-size: 14px; font-weight: 700;">${totalDiscountAmount}</td>
				<td id="totalSettleAmount" style="color: red; font-size: 16px; font-weight: 700;">${totalSettleAmount}</td>
			</tr>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	function checkboxChange(num){
		if($("#incomeVehicleSettleTable").find("input[name='ck" + num+ "']").is(':checked')){
			var incomeAmount = $("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].incomeAmount']").val();
			$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].discountAmount']").val(0);
			$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].settleAmount']").val(incomeAmount);
			$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].status']").val(1);
		}else{
			$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].discountAmount']").val("");
			$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].settleAmount']").val("");
			$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].status']").val(0);
		}
		//计算优惠总额,核销总金额
		var totalDiscountAmount=0;
		var totalSettleAmount=0;
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#incomeVehicleSettleTable").find('>tr').each(function(i){
			if($("#incomeVehicleSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('discountAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalDiscountAmount = addAmount(totalDiscountAmount,val);
							}
						}	
						if(name!=null && name.indexOf('settleAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalSettleAmount = addAmount(totalSettleAmount,val);
							}
						}	
				});
			}
		});
		$("#totalIncomeAmount").html(addAmount(totalSettleAmount,totalDiscountAmount));
		$("#totalDiscountAmount").html(totalDiscountAmount);
		$("#totalSettleAmount").html(totalSettleAmount);
	}


	function discountAmountChange(num) {
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		var incomeAmount = $("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].incomeAmount']").val();
		var discountAmount = $("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].discountAmount']").val();
		if(regExp.test(incomeAmount)&&regExp.test(discountAmount)){
			var url = "incomeController.do?discountAmountChange&incomeAmount=" + incomeAmount+"&discountAmount="+discountAmount;
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].settleAmount']").val(d.attributes.settleAmount);
					}
				}
			});
		}
		//计算优惠总额,核销总金额
		var totalIncomeAmount=0;
		var totalDiscountAmount=0;
		$("#incomeVehicleSettleTable").find('>tr').each(function(i){
			if($("#incomeVehicleSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('incomeAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalIncomeAmount = addAmount(totalIncomeAmount,val);
							}
						}	
						if(name!=null && name.indexOf('discountAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalDiscountAmount = addAmount(totalDiscountAmount,val);
							}
						}	
				});
			}
		});
		$("#totalDiscountAmount").html(totalDiscountAmount);
		$("#totalSettleAmount").html(subAmount(totalDiscountAmount,totalIncomeAmount));
	}
	
	function settleAmountChange(num) {
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		var incomeAmount = $("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].incomeAmount']").val();
		var settleAmount = $("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].settleAmount']").val();
		if(regExp.test(incomeAmount)&&regExp.test(settleAmount)){
			var url = "incomeController.do?settleAmountChange&incomeAmount=" + incomeAmount+"&settleAmount="+settleAmount;
			$.ajax({
				type : 'POST',
				url : url,
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						$("#incomeVehicleSettleTable").find("input[name='incomeList[" + num+ "].discountAmount']").val(d.attributes.discountAmount);
					}
				}
			});
		}
		//计算优惠总额,核销总金额
		var totalIncomeAmount=0;
		var totalSettleAmount=0;
		$("#incomeVehicleSettleTable").find('>tr').each(function(i){
			if($("#incomeVehicleSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('incomeAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalIncomeAmount = addAmount(totalIncomeAmount,val);
							}
						}	
						if(name!=null && name.indexOf('settleAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalSettleAmount = addAmount(totalSettleAmount,val);
							}
						}	
				});
			}
		});
		$("#totalDiscountAmount").html(subAmount(totalSettleAmount,totalIncomeAmount));
		$("#totalSettleAmount").html(totalSettleAmount);
	}
	
	
	function addAmount(arg1, arg2){
		var tem = arg1*10+arg2*10;
		tem = tem/10;
		return tem;
	}
	
	function subAmount(arg1, arg2) {
	    var tem = arg2*10-arg1*10;
	    tem = tem/10;
	    return tem;
	}
</script>

