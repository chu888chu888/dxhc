<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="outlayUnitVehicleSettle" action="outlayUnitController.do?doVehicleSettle">
		<input name="id" type="hidden" value="${outlayUnitPage.id }">
		<input name="tenantId" type="hidden" value="${outlayUnitPage.tenantId }">
		<input name="unit" type="hidden" value="${outlayUnitPage.unit }">
		<input name="expenses.id" type="hidden" value="${outlayUnitPage.expenses.id }">
		<input name="createDate" type="hidden" value="${outlayUnitPage.createDate}">
		<input name="createBy" type="hidden" value="${outlayUnitPage.createBy}">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">往来单位:</label></td>
				<td class="value"><t:dict id="unit" name="unit" type="select" groupCode="unit" datatype="*"
						defaultVal="${outlayUnitPage.unit}"></t:dict></td>
				<td align="right"><label class="Validform_label">费用类型:</label></td>
				<td class="value"><select id="expenses" name="expenses.id" datatype="*" nullmsg="请填写费用" disabled="disabled">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}"
								<c:if test="${outlayUnitPage.expenses.id eq expenses.id}">selected="selected"</c:if>>${expenses.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicle.id" type="hidden" value="${outlayUnitPage.vehicle.id }" />
					<input name="vehicle.plateNumber" id="vehiclePlateNumber" class="inputxt" readonly="readonly" datatype="*"
					nullmsg="请填写车辆" value="${outlayUnitPage.vehicle.plateNumber }" /></td>
				<td align="right"><label class="Validform_label">应付金额:</label></td>
				<td class="value"><input name="amount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请填写应付金额"
					errormsg="请填写正确金额" value="${outlayUnitPage.amount}" readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">付款方式:</label></td>
				<td class="value"><select name="way" datatype="*" nullmsg="请填写付款方式">
						<option value="1">现金</option>
						<option value="2">银行转账</option>
						<option value="3">微信</option>
						<option value="4">支付宝</option>
						<option value="5">POS</option>
				</select></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" type="text" class="inputxt" value="${outlayUnitPage.remarks}" /></td>
			</tr>
		</table>
	</t:form>
</body>
</html>