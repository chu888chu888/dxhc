<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<style>
.vehicleTable {
	margin: 0 auto;
	border-collapse: collapse;
}

.vehicleThead {
	background: #EEEEEE;
	display: block
}

.vehicleTbody {
	height: 353px;
	overflow-y: scroll;
	display: block;
	overflow-y: scroll;
}

.td50 {
	width: 50px;
	min-width: 50px;
	max-width: 50px;
}

.td140 {
	width: 140px;
	min-width: 140px;
	max-width: 140px;
}

.td250 {
	width: 250px;
	min-width: 250px;
	max-width: 250px;
}

.td285 {
	width: 285px;
	min-width: 285px;
	max-width: 285px;
}
</style>
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<thead class="vehicleThead">
			<tr bgcolor="#E6E6E6">
				<td align="center" class="td50"><label class="Validform_label">序号</label></td>
				<td align="center" class="td140"><label class="Validform_label">车牌</label></td>
				<td align="center" class="td140"><label class="Validform_label">费用类型</label></td>
				<td align="center" class="td140"><label class="Validform_label">应付金额</label></td>
				<td align="center" class="td285"><label class="Validform_label">备注</label></td>
			</tr>
		</thead>
		<tbody id="outlayUnitListTable" class="vehicleTbody">
			<c:forEach items="${outlayUnitList}" var="outlayUnit" varStatus="stuts">
				<tr>
					<td align="center" class="td50"><div style="width: 20px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center" class="td140"><c:out value="${outlayUnit.vehicle.sn}-${outlayUnit.vehicle.plateNumber}"></c:out></td>
					<td align="center" class="td140"><c:out value="${outlayUnit.expenses.name}"></c:out></td>
					<td align="center" class="td140"><c:out value="${outlayUnit.amount}"></c:out></td>
					<td align="center" class="td285"><c:out value="${outlayUnit.remarks}"></c:out></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<table>
		<tr>
			<td style="font-weight: 700;" align="right" class="td285">总计:</td>
			<td id="totalAmount" style="color: red; font-weight: 700; text-align: center;" class="td250">${totalAmount}</td>
		</tr>
	</table>
</div>
