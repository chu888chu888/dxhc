<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeUnitSumDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="sn" class="inputxt" readonly="readonly" value="${incomeUnitSumPage.sn}" /></td>
				<td align="right"><label class="Validform_label">往来单位:</label></td>
				<td class="value"><t:dict id="unit" name="unit" type="select" groupCode="unit" datatype="*"
						defaultVal="${incomeUnitSumPage.unit}"></t:dict></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">核销金额:</label></td>
				<td class="value"><input name="amount" class="inputxt" readonly="readonly" value="${incomeUnitSumPage.amount}" /></td>
				<td align="right"><label class="Validform_label">收款方式:</label></td>
				<td class="value"><select name="way" disabled="disabled">
						<option value="1" <c:if test="${incomeUnitSumPage.way eq 1}">selected="selected"</c:if>>现金</option>
						<option value="2" <c:if test="${incomeUnitSumPage.way eq 2}">selected="selected"</c:if>>银行转账</option>
						<option value="3" <c:if test="${incomeUnitSumPage.way eq 3}">selected="selected"</c:if>>微信</option>
						<option value="4" <c:if test="${incomeUnitSumPage.way eq 4}">selected="selected"</c:if>>支付宝</option>
						<option value="5" <c:if test="${incomeUnitSumPage.way eq 5}">selected="selected"</c:if>>POS</option>

				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">状态:</label></td>
				<td class="value"><select name="status" disabled="disabled">
						<option value="0" <c:if test="${incomeUnitSumPage.status eq 0}">selected="selected"</c:if>>正常</option>
						<option value="1" <c:if test="${incomeUnitSumPage.status eq 1}">selected="selected"</c:if>>作废</option>
				</select></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" class="inputxt" readonly="readonly"
					value="${incomeUnitSumPage.remarks}" /></td>
			</tr>
		</table>
		<div style="width: 795px; height: 455px;">
			<div id="incomeListTab" border="false" class="easyui-tabs" fit="true">
				<div title="核销明细" href="incomeUnitSumController.do?settleTab&id=${incomeUnitSumPage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>