<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<script type="text/javascript">
	//责任人改变 
	function ownerChange() {
		var ownerId = $('#ownerId').val();
		refreshTab(ownerId);
	}
	//刷新指定Tab的内容  
	function refreshTab(ownerId) {
		if ($('#outlayOwnerListTab').tabs('exists', '付款明细')) {
			var currTab = $('#outlayOwnerListTab').tabs('getTab', '付款明细');
			$('#outlayOwnerListTab')
					.tabs(
							'update',
							{
								tab : currTab,
								options : {
									id : 'outlayOwnerListTab',
									title : '付款明细',
									href : 'outlayController.do?ownerSettleTab&ownerId='
											+ ownerId,
								}
							});
		}
	}
</script>
</head>
<body>
	<t:form id="outlayOwnerSettle" action="outlayController.do?doSettle" tiptype="1">
		<table style="width: 750px;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input id="ownerId" name="owner.id" type="hidden" /> <input id="ownerName" name="owner.name"
					class="inputxt" readonly="readonly" onclick="choose_ownerName()" datatype="*" /> <t:gridSelect valueId="ownerId"
						nameId="ownerName" url="ownerController.do?select" gridField="name" gridName="ownerSelectList" title="责任人列表"
						height="460" width="800" callback="ownerChange();"></t:gridSelect></td>
				<td align="right"><label class="Validform_label">付款方式:</label></td>
				<td class="value"><select name="way" datatype="*" nullmsg="请填写付款方式">
						<option value="1">现金</option>
						<option value="2">微信</option>
						<option value="3">支付宝</option>
						<option value="4">POS</option>
						<option value="5">银行转账</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value" colspan="3"><input name="remarks" type="text" class="inputxt" /></td>
			</tr>
		</table>
		<div style="width: 750px; height: 450px;">
			<div id="outlayOwnerListTab" border="false" class="easyui-tabs" fit="true">
				<div title="付款明细" href="outlayController.do?ownerSettleTab"></div>
			</div>
		</div>
	</t:form>
</body>
</html>