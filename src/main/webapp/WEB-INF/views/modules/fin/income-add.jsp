<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeAdd" action="incomeController.do?doAdd" tiptype="1">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">车辆:</label></td>
				<td class="value"><input id="vehicleId" name="vehicleId" type="hidden" /> <input name="vehiclePlateNumber"
					id="vehiclePlateNumber" class="inputxt" style="width: 240px;" readonly="readonly" onclick="vehicleSelect()"
					datatype="*" /></td>
			</tr>
		</table>
		<div style="width: 800px; height: 450px;" id="expensesTab">
			<div border="false" class="easyui-tabs" fit="true">
				<div title="收款明细" href="incomeController.do?expensesTab"></div>
			</div>
		</div>
	</t:form>
	<!-- 添加 明细 模版 -->
	<table style="display: none" id="incomeListTable_template">
		<tbody>
			<tr>
				<td align="center"><div style="width: 25px;" name="xh"></div></td>
				<td align="center"><input style="width: 40px;" type="checkbox" name="ck" /></td>
				<td align="center"><select name="incomeList[#index#].expenses.id" style="width: 150px;"
					onchange="expensesChange(this.options[this.options.selectedIndex].value,#index#)">
						<c:forEach items="${expensesList}" var="expenses">
							<option value="${expenses.id}">${expenses.name}</option>
						</c:forEach>
				</select></td>
				<td align="center"><input name="incomeList[#index#].incomeAmount" type="text" class="inputxt"
					datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" errormsg="请填写正确金额"
					value="${incomeAmount}" style="width: 120px;" onchange="calTotalIncomeAmount()"></td>
				<td align="center"><input name="incomeList[#index#].endDate" type="text" class="Wdate" onClick="WdatePicker()"
					style="width: 120px;"></td>
				<td align="center"><input name="incomeList[#index#].remarks" type="text" class="inputxt" style="width: 240px;"></td>
			</tr>
		</tbody>
	</table>
</body>
<script type="text/javascript">
	function vehicleSelect() {
		var vehicleId = $("#vehicleId").val();
		var vehiclePlateNumber = $("#vehiclePlateNumber").val();
		var url = 'vehicleController.do?selectMore&vehicleId=' + vehicleId
				+ "&vehiclePlateNumber=" + vehiclePlateNumber;
		$.dialog.setting.zIndex = getzIndex();
		$.dialog({
			content : 'url:' + encodeURI(url),
			zIndex : getzIndex(),
			title : '车辆列表',
			lock : true,
			width : '1240px',
			height : '460px',
			opacity : 0.4,
			button : [ {
				name : '确定',
				callback : callbackVehicleSelect,
				focus : true
			}, {
				name : '取消',
				callback : function() {
				}
			} ]
		}).zindex();
	}
	function callbackVehicleSelect() {
		var iframe = this.iframe.contentWindow;
		var table = iframe.$("#vehicleTable");
		var vehicleId = '', vehiclePlateNumber = '';
		$(table).find("tbody tr").each(function() {
			vehicleId += $(this).find("input").val() + ",";
			vehiclePlateNumber += $(this).find("span").text() + ",";

		})
		$("#vehiclePlateNumber").val(vehiclePlateNumber);
		$("#vehiclePlateNumber").blur();
		$("#vehicleId").val(vehicleId);
	}
</script>
</html>