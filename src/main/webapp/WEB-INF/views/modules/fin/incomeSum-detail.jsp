<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="incomeSumDetail">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">编号:</label></td>
				<td class="value"><input name="sn" class="inputxt" readonly="readonly" value="${incomeSumPage.sn}" /></td>
				<td align="right"><label class="Validform_label">责任人:</label></td>
				<td class="value"><input name="owner.name" class="inputxt" readonly="readonly"
					value="${incomeSumPage.owner.name}" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">核销金额:</label></td>
				<td class="value"><input name="amount" class="inputxt" readonly="readonly" value="${incomeSumPage.amount}" /></td>
				<td align="right"><label class="Validform_label">收款方式:</label></td>
				<td class="value"><select name="way" disabled="disabled">
						<option value="1" <c:if test="${incomeSumPage.way eq 1}">selected="selected"</c:if>>现金</option>
						<option value="2" <c:if test="${incomeSumPage.way eq 2}">selected="selected"</c:if>>微信</option>
						<option value="3" <c:if test="${incomeSumPage.way eq 3}">selected="selected"</c:if>>支付宝</option>
						<option value="4" <c:if test="${incomeSumPage.way eq 4}">selected="selected"</c:if>>POS</option>
						<option value="5" <c:if test="${incomeSumPage.way eq 5}">selected="selected"</c:if>>银行转账</option>
				</select></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">状态:</label></td>
				<td class="value"><select name="status" disabled="disabled">
						<option value="0" <c:if test="${incomeSumPage.status eq 0}">selected="selected"</c:if>>正常</option>
						<option value="1" <c:if test="${incomeSumPage.status eq 1}">selected="selected"</c:if>>作废</option>
				</select></td>
				<td align="right"><label class="Validform_label">备注:</label></td>
				<td class="value"><input name="remarks" class="inputxt" readonly="readonly" value="${incomeSumPage.remarks}" /></td>
			</tr>
		</table>
		<div style="height: 420px;">
			<div id="incomeUnitListTab" border="false" class="easyui-tabs" fit="true">
				<div title="核销明细" href="incomeUnitSumController.do?settleTab&id=${incomeSumPage.id}"></div>
			</div>
		</div>
	</t:form>
</body>
</html>