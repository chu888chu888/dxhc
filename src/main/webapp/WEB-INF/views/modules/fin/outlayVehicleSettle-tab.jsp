<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<div style="padding: 1px; height: 1px;"></div>
<div>
	<table border="0" cellpadding="2" cellspacing="0">
		<tr bgcolor="#E6E6E6">
			<td align="left" bgcolor="#EEEEEE" width="30px;"><label class="Validform_label">序号</label></td>
			<td align="left" bgcolor="#EEEEEE" width="40px"><label class="Validform_label">操作</label></td>
			<td align="center" bgcolor="#EEEEEE" width="115px"><label class="Validform_label">费用类型</label></td>
			<td align="center" bgcolor="#EEEEEE" width="115px"><label class="Validform_label">应付金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="110px"><label class="Validform_label">核销金额</label></td>
			<td align="center" bgcolor="#EEEEEE" width="250px"><label class="Validform_label">备注</label></td>
		</tr>
		<tbody id="outlayVehicleSettleTable">
			<c:forEach items="${outlayList}" var="outlay" varStatus="stuts">
				<tr>
					<input name="outlayList[${stuts.index }].id" type="hidden" value="${outlay.id}">
					<input name="outlayList[${stuts.index }].tenantId" type="hidden" value="${outlay.tenantId}">
					<input name="outlayList[${stuts.index }].vehicle.id" type="hidden" value="${outlay.vehicle.id}">
					<input name="outlayList[${stuts.index }].expenses.id" type="hidden" value="${outlay.expenses.id}">
					<input name="outlayList[${stuts.index }].outlayAmount" type="hidden" value="${outlay.outlayAmount}">
					<input name="outlayList[${stuts.index }].createBy" type="hidden" value="${outlay.createBy}">
					<input name="outlayList[${stuts.index }].createDate" type="hidden" value="${outlay.createDate}">
					<td align="center"><div style="width: 30px;" name="xh">${stuts.index+1 }</div></td>
					<td align="center"><input style="width: 40px;" type="checkbox" name="ck${stuts.index }" checked="checked"
						onchange="checkboxChange(${stuts.index})" /><input name="outlayList[${stuts.index }].status" type="hidden"
						value="1"></td>
					<td align="center" width="115px"><c:out value="${outlay.expenses.name}"></c:out></td>
					<td align="center" width="115px"><c:out value="${outlay.outlayAmount}"></c:out></td>
					<td align="center" width="110px"><input name="outlayList[${stuts.index }].settleAmount" type="text"
						class="inputxt" style="width: 110px"
						datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" ignore="ignore"
						onchange="settleAmountChange(${stuts.index});" value="${outlay.outlayAmount}"></td>
					<td align="center"><input name="outlayList[${stuts.index }].remarks" type="text" class="inputxt"
						style="width: 250px" value="${outlay.remarks}"></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="2"></td>
				<td style="font-size: 14px; font-weight: 700;" align="center">总计:</td>
				<td id="totalOutlayAmount" style="color: red; font-size: 14px; font-weight: 700;" align="center">${totalOutlayAmount}</td>
				<td id="totalSettleAmount" style="color: red; font-size: 16px; font-weight: 700;">${totalSettleAmount}</td>
			</tr>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	function checkboxChange(num){
		if($("#outlayVehicleSettleTable").find("input[name='ck" + num+ "']").is(':checked')){
			var outlayAmount = $("#outlayVehicleSettleTable").find("input[name='outlayList[" + num+ "].outlayAmount']").val();
			$("#outlayVehicleSettleTable").find("input[name='outlayList[" + num+ "].settleAmount']").val(outlayAmount);
			$("#outlayVehicleSettleTable").find("input[name='outlayList[" + num+ "].status']").val(1);
		}else{
			$("#outlayVehicleSettleTable").find("input[name='outlayList[" + num+ "].settleAmount']").val("");
			$("#outlayVehicleSettleTable").find("input[name='outlayList[" + num+ "].status']").val(0);
		}
		//计算优惠总额,核销总金额
		var totalOutlayAmount=0;
		var totalSettleAmount=0;
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#outlayVehicleSettleTable").find('>tr').each(function(i){
			if($("#outlayVehicleSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('outlayAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalOutlayAmount = addAmount(totalOutlayAmount,val);
							}
						}	
						if(name!=null && name.indexOf('settleAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalSettleAmount = addAmount(totalSettleAmount,val);
							}
						}	
				});
			}
		});
		$("#totalOutlayAmount").html(totalOutlayAmount);
		$("#totalSettleAmount").html(totalSettleAmount);
	}

	function settleAmountChange(num) {
		//核销总金额
		var totalOutlayAmount=0;
		var totalSettleAmount=0;
		var regExp= /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		$("#outlayVehicleSettleTable").find('>tr').each(function(i){
			if($("#outlayVehicleSettleTable").find("input[name='ck" + i+ "']").is(':checked')){
				$(':input', this).each(function(){
					var $this = $(this);
						var name = $this.attr('name');
						if(name!=null && name.indexOf('outlayAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalOutlayAmount = addAmount(totalOutlayAmount,val);
							}
						}	
						if(name!=null && name.indexOf('settleAmount')>=0){
							var val = $this.val();
							if(regExp.test(val)){
								totalSettleAmount = addAmount(totalSettleAmount,val);
							}
						}	
				});
			}z
		});
		$("#totalOutlayAmount").html(totalOutlayAmount);
		$("#totalSettleAmount").html(totalSettleAmount);
	}
	
	
	function addAmount(arg1, arg2){
		var tem = arg1*10+arg2*10;
		tem = tem/10;
		return tem;
	}
	
	function subAmount(arg1, arg2) {
	    var tem = arg2*10-arg1*10;
	    tem = tem/10;
	    return tem;
	}
</script>

