<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
</head>
<body>
	<t:form id="dictAdd" action="dictController.do?doAdd">
		<input name="dictGroup.id" type="hidden" value="${dictGroupPage.id}">
		<input name="dictGroup.code" type="hidden" value="${dictGroupPage.code}">
		<table style="width: 400px;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">名称:</label></td>
				<td class="value"><input id="name" name="name" type="text" class="inputxt" datatype="*" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">排序:</label></td>
				<td class="value"><input name="order" type="text" class="inputxt" datatype="n1-3"></td>
			</tr>
		</table>
	</t:form>
</body>
</html>
