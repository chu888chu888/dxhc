<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base></t:base>
<div class="easyui-layout" fit="true">
	<div region="center">
		<t:grid name="userList" url="userController.do?list" sortName="createDate">
			<t:gridCol title="id" field="id" hidden="true"></t:gridCol>
			<t:gridCol title="手机" field="phone" query="true"></t:gridCol>
			<t:gridCol title="姓名" field="name" query="true"></t:gridCol>
			<t:gridCol title="部门" field="department" query="true" dictGroup="department"></t:gridCol>
			<t:gridBar title="增加" icon="icon-add" url="userController.do?goAdd" funname="add"></t:gridBar>
			<t:gridBar title="修改" icon="icon-update" url="userController.do?goUpdate" funname="update"></t:gridBar>
			<t:gridBar title="删除" icon="icon-delete" url="userController.do?doDel" funname="del"></t:gridBar>
			<t:gridBar title="密码重置" icon="icon-password" url="userController.do?goPassword" funname="update"></t:gridBar>
			<t:gridCol title="操作" field="opt"></t:gridCol>
			<t:gridOpt title="权限设置" funname="authSet(id)" style="btn_green"></t:gridOpt>
		</t:grid>
	</div>
</div>
<script type="text/javascript">
	function authSet(id) {
		createAddUpdateWindow('权限设置', 'userList',
				'userController.do?goAuth&id=' + id, 500, 650);
	}
</script>
