<%@ taglib prefix="t" uri="/tags-extend"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="fne" uri="/functions-extend"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
