<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>${fne:getConfig('title')}</title>
<meta name="author" content="${fne:getConfig('author')}">
<meta name="keywords" content="${fne:getConfig('keywords')}">
<meta name="description" content="${fne:getConfig('description')}">
<t:base />
</head>
<body>
	<t:form id="tenantSelect" action="index.do?doTenantSelect">
		<table cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label">用户名:</label></td>
				<td class="value"><input id="userId" name="userId" value="${userId}" type="hidden"><input
					name="userName" type="text" class="inputxt" value="${userName}" readonly="readonly" style="width: 272px;" /></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label">公司:</label></td>
				<td class="value"><select id="tenantId" name="tenantId" datatype="*" style="width: 280px;">
						<c:forEach items="${tenantList}" var="tenant">
							<option value="${tenant.id }">${tenant.name}</option>
						</c:forEach>
				</select></td>
			</tr>
		</table>
	</t:form>
</body>
</html>